from django.conf.urls import include
from django.conf.urls import patterns
from django.conf.urls import url
from django.contrib import admin

urlpatterns = patterns('',
                       # Examples:
                       # url(r'^$', 'libreriaDEF.views.home', name='home'),
                       # url(r'^blog/', include('blog.urls')),

                       url(r'^admin/', include(admin.site.urls)),
                       url(r'^$', 'libreriaDEF.views.home', name='home'),
                        url(r'^api/', include('api.urls')),
                       url(r'^libreria/', include ('libreria.urls')),
                      
                       )
