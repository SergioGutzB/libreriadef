# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.
from .views import *
from django.conf.urls import include
from django.conf.urls import patterns
from django.conf.urls import url

urlpatterns = patterns('libreria.views', 
                       url(r'^$', 'libreria_view', name='libreria'), 
                       url(r'^protocolos/', 'portocolos_view', name='view_protocolos'), 
                       url(r'^creditos/', 'creditos_view', name='view_creditos'), 
                       url(r'^informacion/', 'informacion_view', name='view_informacion'), 
                       url(r'^ajax/$', ajax_cCient.as_view(), name='ajax_cCient'),
                       url(r'^coord_ajax/$', ajax_coordenadas.as_view(), name='ajax_coord'),
                       url(r'^firmasEspectrales_ajax/$', ajax_firmasEspectrales.as_view(), name='ajax_firmas'),
                       url(r'^indices_ajax/$', ajax_indices.as_view(), name='ajax_indices'),
                       url(r'^parametros_ajax/$', ajax_arametros.as_view(), name='ajax_indices'),
                       
                       )