# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('libreria', '0004_auto_20150315_2143'),
    ]

    operations = [
        migrations.AddField(
            model_name='clasificacion_cientifica',
            name='seccion',
            field=models.CharField(default='', max_length=200),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='clasificacion_cientifica',
            name='subgenero',
            field=models.CharField(default='', max_length=200),
            preserve_default=False,
        ),
    ]
