# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('libreria', '0003_auto_20150315_2131'),
    ]

    operations = [
        migrations.AlterField(
            model_name='clasificacion_cientifica',
            name='subclase',
            field=models.CharField(max_length=200, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='clasificacion_cientifica',
            name='subfamilia',
            field=models.CharField(max_length=200, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='clasificacion_cientifica',
            name='subreino',
            field=models.CharField(max_length=200, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='clasificacion_cientifica',
            name='tribu',
            field=models.CharField(max_length=200, blank=True),
            preserve_default=True,
        ),
    ]
