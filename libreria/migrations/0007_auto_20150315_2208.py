# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('libreria', '0006_auto_20150315_2149'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cordenada',
            name='este',
            field=models.FloatField(max_length=20, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='cordenada',
            name='norte',
            field=models.FloatField(max_length=20, blank=True),
            preserve_default=True,
        ),
    ]
