# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Clasificacion_cientifica',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('reino', models.CharField(max_length=200)),
                ('division', models.CharField(max_length=200)),
                ('calse', models.CharField(max_length=200)),
                ('orden', models.CharField(max_length=200)),
                ('familia', models.CharField(max_length=200)),
                ('genero', models.CharField(max_length=200)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Cordenada',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('norte', models.FloatField(max_length=20)),
                ('este', models.FloatField(max_length=20)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Especie',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('codigo', models.CharField(max_length=10)),
                ('nombre_comun', models.CharField(max_length=100)),
                ('nombre_cientifico', models.CharField(max_length=200)),
                ('fecha_captura', models.DateTimeField(verbose_name=b'Fecha captura')),
                ('sensor', models.CharField(max_length=30)),
                ('IRGA', models.CharField(max_length=30)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Firma_espectral',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('longitud_onda', models.IntegerField(max_length=4)),
                ('firma', models.FloatField(max_length=20)),
                ('Especie', models.ForeignKey(to='libreria.Especie')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Indice_vegetacion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('NDVI', models.FloatField(max_length=20)),
                ('RDVi', models.FloatField(max_length=20)),
                ('MSR', models.FloatField(max_length=20)),
                ('MSAVI', models.FloatField(max_length=20)),
                ('MCARI', models.FloatField(max_length=20)),
                ('TVI', models.FloatField(max_length=20)),
                ('MCARI1', models.FloatField(max_length=20)),
                ('MTVI1', models.FloatField(max_length=20)),
                ('MCARI2', models.FloatField(max_length=20)),
                ('MTVI2', models.FloatField(max_length=20)),
                ('Especie', models.ForeignKey(to='libreria.Especie')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Parametros_curva_luz',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Amax', models.FloatField(max_length=6)),
                ('K', models.FloatField(max_length=6)),
                ('Rd', models.FloatField(max_length=6)),
                ('PCL', models.FloatField(max_length=6)),
                ('Especie', models.ForeignKey(to='libreria.Especie')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='cordenada',
            name='Especie',
            field=models.ForeignKey(to='libreria.Especie'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='clasificacion_cientifica',
            name='Especie',
            field=models.ForeignKey(to='libreria.Especie'),
            preserve_default=True,
        ),
    ]
