# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('libreria', '0002_auto_20150315_2126'),
    ]

    operations = [
        migrations.AddField(
            model_name='clasificacion_cientifica',
            name='subclase',
            field=models.CharField(default='o', max_length=200),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='clasificacion_cientifica',
            name='subfamilia',
            field=models.CharField(default='0', max_length=200),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='clasificacion_cientifica',
            name='subreino',
            field=models.CharField(default='0', max_length=200),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='clasificacion_cientifica',
            name='tribu',
            field=models.CharField(default='', max_length=200),
            preserve_default=False,
        ),
    ]
