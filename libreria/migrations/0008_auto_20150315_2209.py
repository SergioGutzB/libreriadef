# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('libreria', '0007_auto_20150315_2208'),
    ]

    operations = [
        migrations.AddField(
            model_name='cordenada',
            name='latitud',
            field=models.FloatField(default=0.0, max_length=20, blank=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='cordenada',
            name='longitud',
            field=models.FloatField(default=0.0, max_length=20, blank=True),
            preserve_default=False,
        ),
    ]
