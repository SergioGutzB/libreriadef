# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('libreria', '0005_auto_20150315_2147'),
    ]

    operations = [
        migrations.AlterField(
            model_name='clasificacion_cientifica',
            name='seccion',
            field=models.CharField(max_length=200, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='clasificacion_cientifica',
            name='subgenero',
            field=models.CharField(max_length=200, blank=True),
            preserve_default=True,
        ),
    ]
