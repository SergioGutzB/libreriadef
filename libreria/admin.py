from django.contrib import admin
from libreria.models import *

class Firma_espectralAdmin(admin.ModelAdmin):
    fields = ['Especie', 'longitud_onda', 'firma']
    list_display = ('Especie', 'longitud_onda', 'firma')

class Indice_vegetacionAdmin(admin.ModelAdmin):
    list_display = ('Especie', 'NDVI', 'RDVi', 'MSR', 'MSAVI', 'MCARI', 'TVI', 'MCARI1', 'MTVI1', 'MCARI2', 'MTVI2')

class cClasificacion_Admin(admin.ModelAdmin):
    list_display = ('Especie', 'reino', 'subreino', 'division', 'clase', 'subclase', 'orden', 'familia', 'subfamilia', 'tribu', 'genero', 'subgenero', 'seccion')

class coodenadas_Admin(admin.ModelAdmin):
    list_display = ('Especie', 'norte', 'este', 'latitud', 'longitud')

class Parametros_Admin(admin.ModelAdmin):
    list_display = ('Especie', 'Amax', 'K', 'Rd', 'PCL')
    
class Especie_Admin(admin.ModelAdmin):
    list_display = ('codigo', 'nombre_comun', 'nombre_cientifico', 'fecha_captura', 'sensor', 'IRGA')





admin.site.register(Especie, Especie_Admin)
admin.site.register(Parametros_curva_luz, Parametros_Admin)
admin.site.register(Indice_vegetacion, Indice_vegetacionAdmin)
admin.site.register(Cordenada, coodenadas_Admin)
admin.site.register(Firma_espectral, Firma_espectralAdmin)
admin.site.register(Clasificacion_cientifica, cClasificacion_Admin)