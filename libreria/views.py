from django.http import HttpResponse, Http404
from django.shortcuts import render, render_to_response
from django.template import RequestContext
import libreriaDEF.settings
import django
from django.core.paginator import Paginator,EmptyPage,InvalidPage
import simplejson
from libreria.models import *
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.http import HttpResponseRedirect
from django.views.generic import TemplateView, ListView
from django.core import serializers

# Create your views here.
def libreria_view(request):    
    lista_especie = Especie.objects.all() 
    mensaje = "estamos retornando las especies"
    ctx = {'lista':lista_especie, 'msj':mensaje}
    return render_to_response('libreria/libreria.html', ctx, context_instance=RequestContext(request))

def portocolos_view(request):
    return render (request, 'libreria/protocolos.html')

def creditos_view(request):
    return render (request, 'libreria/creditos.html')

def informacion_view(request):
    return render (request, 'libreria/informacion.html')

class ajax_cCient(TemplateView):
    def get(self, request, * args, ** kwargs):
        id_tipo = request.GET['id']
        cC = Clasificacion_cientifica.objects.filter(Especie=id_tipo)
        data = serializers.serialize('json', cC, fields=('Especie', 'reino', 'subreino', 'division', 'clase', 'subclase', 'orden', 'familia', 'subfamilia', 'tribu', 'genero', 'subgenero', 'seccion'))
        return HttpResponse(data, content_type='application/json')
    
class ajax_coordenadas(TemplateView):
    def get(self, request, * args, ** kwargs):
        id_tipo = request.GET['id']
        cC = Cordenada.objects.filter(Especie=id_tipo)
        data = serializers.serialize('json', cC, fields=('latitud', 'longitud'))
        return HttpResponse(data, content_type='application/json')
    
class ajax_firmasEspectrales(TemplateView):
    def get(self, request, * args, ** kwargs):
        id_tipo = request.GET['id']
        cC = Firma_espectral.objects.filter(Especie=id_tipo)
        data = serializers.serialize('json', cC, fields=('Especie', 'longitud_onda', 'firma'))
        return HttpResponse(data, content_type='application/json')
    
class ajax_indices(TemplateView):
    def get(self, request, * args, ** kwargs):
        id_tipo = request.GET['id']
        cC = Indice_vegetacion.objects.filter(Especie=id_tipo)
        data = serializers.serialize('json', cC, fields=("Especie", "NDVI", "RDVi", "MSR", "MSAVI", "MCARI", "TVI", "MCARI1", "MTVI1", "MCARI2", "MTVI2"))
        return HttpResponse(data, content_type='application/json')
  
class ajax_arametros(TemplateView):
    def get(self, request, * args, ** kwargs):
        id_tipo = request.GET['id']
        cC = Parametros_curva_luz.objects.filter(Especie=id_tipo)
        data = serializers.serialize('json', cC, fields=("Especie", "Amax", "K", "Rd", "PCL"))
        return HttpResponse(data, content_type='application/json')
    

	