from django.db import models

# Create your models here.
class Especie(models.Model):
	codigo = models.CharField(max_length=10)
	nombre_comun = models.CharField(max_length=100)
	nombre_cientifico = models.CharField(max_length=200)
	fecha_captura = models.DateTimeField('Fecha captura')
	sensor = models.CharField(max_length=30)
	IRGA = models.CharField(max_length=30)

	def __str__(self):
		return self.nombre_cientifico

class Parametros_curva_luz (models.Model):
	Especie = models.ForeignKey(Especie)
	Amax = models.FloatField(max_length=6)
	K = models.FloatField(max_length=6)
	Rd = models.FloatField(max_length=6)
	PCL = models.FloatField(max_length=6)

class Indice_vegetacion (models.Model):
	Especie = models.ForeignKey(Especie)
	NDVI = models.FloatField(max_length=20)
	RDVi = models.FloatField(max_length=20)
	MSR = models.FloatField(max_length=20)
	MSAVI = models.FloatField(max_length=20)
	MCARI = models.FloatField(max_length=20)
	TVI = models.FloatField(max_length=20)
	MCARI1 = models.FloatField(max_length=20)
	MTVI1 = models.FloatField(max_length=20)
	MCARI2 = models.FloatField(max_length=20)
	MTVI2 = models.FloatField(max_length=20)

class Cordenada (models.Model):
	Especie = models.ForeignKey(Especie)
	norte = models.FloatField(max_length=20, blank=True)
	este = models.FloatField(max_length=20, blank=True)
        latitud = models.FloatField(max_length=20, blank=True)
	longitud = models.FloatField(max_length=20, blank=True)


class Firma_espectral (models.Model):
	Especie = models.ForeignKey(Especie)
	longitud_onda = models.IntegerField (max_length=4)
	firma = models.FloatField(max_length=20)

class Clasificacion_cientifica (models.Model):
	Especie = models.ForeignKey(Especie)
	reino =  models.CharField(max_length=200)
        subreino =  models.CharField(max_length=200, blank=True)
	division = models.CharField(max_length=200)
	clase = models.CharField(max_length=200)
        subclase = models.CharField(max_length=200, blank=True)
	orden = models.CharField(max_length=200)
	familia = models.CharField(max_length=200)
        subfamilia = models.CharField(max_length=200, blank=True)
        tribu = models.CharField(max_length=200, blank=True)
	genero = models.CharField(max_length=200)
        subgenero = models.CharField(max_length=200, blank=True)
        seccion = models.CharField(max_length=200, blank=True)

	def __str__(self):
		return self.reino