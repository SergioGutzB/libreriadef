from rest_framework import serializers
from libreria.models import *

class cClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Clasificacion_cientifica
        fields = ('Especie', 'reino', 'division', 'clase', 'orden', 'familia', 'genero')