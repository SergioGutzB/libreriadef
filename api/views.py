from django.shortcuts import render

# Create your views here.
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import permissions

from rest_framework.generics import (ListCreateAPIView, RetrieveUpdateDestroyAPIView )

from libreria.models import *
from api.serializers import *


@api_view(['GET', 'POST'])
def cClient_lista(request):
    # GET Request
    if request.method == 'GET':
        org = Clasificacion_cientifica.objects.all()
        serializer = cClientSerializer(org)
        return Response(serializer.data)

    # POST Request
    if request.method == 'POST':
        serializer = cClientSerializer(data=request.DATA, many=True)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        else:
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST
            )



