from django.conf.urls import patterns, include, url

#Function based API views
from api.views import *

# Class based API views
#from api.views import TaskList, TaskDetail

urlpatterns = patterns('',

    #Regular URLs
	url(r'^cClient/$', cClient_lista, name='cClient_list'),
)