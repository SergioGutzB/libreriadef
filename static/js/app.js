$(document).foundation({abide: {live_onblur: false}});

$(function () {
    if (document.getElementById("libreria"))
    {
        libreria();
        mapa(1, "Magnolia Grandiflora");
    }
    else {
    }
    $('#GridView1').filterTable({minRows: 3, placeholder: "Filtrar Especie", label: ""});
    gridviewScroll();
});

function gridviewScroll() {
    gridView1 = $('#GridView1').gridviewScroll({
        width: 500,
        height: 80,
        // railcolor: "#F0F0F0",
        //  barcolor: "#CDCDCD",
        // barhovercolor: "#606060",
        // bgcolor: "#F0F0F0",
        freezesize: 1,
        arrowsize: 5,
        varrowtopimg: "../img/arrowvt.png",
        varrowbottomimg: "../img/arrowvb.png",
        harrowleftimg: "../img/arrowhl.png",
        harrowrightimg: "../img/arrowhr.png",
        headerrowcount: 1,
        railsize: 17,
        barsize: 8
    });
}

function libreria(title) {

    $('body').on('click', '#cvs1', function (e) {
        var id = $("#t-id").val();
        var datos = [];
        var titulo = $("#nom-cient-" + id).text();
        $.ajax({
            data: {'id': id},
            url: '/libreria/firmasEspectrales_ajax',
            type: 'get',
            success: function (data) {
                for (i = 0; i < data.length; i++) {
                    datos.push({"Especie": titulo, "Longitud de Onda": data[i].fields.longitud_onda, "Firma": data[i].fields.firma});
                }
                if (datos == '')
                    return;
                JSONToCSVConvertor(datos, titulo, true);
            }
        });
    });

    $('body').on('click', '#cvs2', function (e) {
        var id = $("#t-id").val();
        var datos = [];
        var titulo = $("#nom-cient-" + id).text();
        $.ajax({
            data: {'id': id},
            url: '/libreria/parametros_ajax',
            type: 'get',
            success: function (data) {
                console.log(data)
                for (i = 0; i < data.length; i++) {
                    datos.push({"Especie": titulo, "Amax": data[i].fields.Amax, "K": data[i].fields.firma, "Rd": data[i].fields.Rd, "PCL": data[i].fields.PCL});
                }
                if (datos == '')
                    return;
                JSONToCSVConvertor(datos, titulo, true);
            }
        });
    });

    $('body').on('click', 'tr', function (e) {
        e.preventDefault();
        var id = this.id;
        $("#t-id").val(id);
        var title = $("#nom-cient-" + id).text();
        var imgs = ["../static/img/Magnolia_Grandiflora.jpg",
            "../static/img/Podocarpus_Oleifolius.jpg",
            "../static/img/Quercus_Humboldtii.jpg",
            "../static/img/Clusia_Multiflora.jpg",
            "../static/img/Tibouchina_Lepidota.jpg"];

        $("#titulo-especie").attr("value", "titulo");
        $.ajax({
            data: {'id': id},
            url: '/libreria/ajax',
            type: 'get',
            success: function (data) {
                var valorTitulo = $("#titulo-especie").text();
                $("#titulo-especie").text($("#nom-cient-" + id).text());
                $(".img-especie").html("<img src='" + imgs[id - 1] + "' widht='100%'>");
                $("#t-reino").text(data[0].fields.reino);
                $("#t-subreino").text(data[0].fields.subreino);
                $("#t-division").text(data[0].fields.division);
                $("#t-clase").text(data[0].fields.clase);
                $("#t-subclase").text(data[0].fields.subclase);
                $("#t-orden").text(data[0].fields.orden);
                $("#t-familia").text(data[0].fields.familia);
                $("#t-subfamilia").text(data[0].fields.subfamilia);
                $("#t-tribu").text(data[0].fields.tribu);
                $("#t-genero").text(data[0].fields.genero);
                $("#t-subgenero").text(data[0].fields.subgenero);
                $("#t-seccion").text(data[0].fields.seccion);
                $("#t-especie").text(title);
                mapa(id, title);
            }
        });

        $.ajax({
            data: {'id': id},
            url: '/libreria/firmasEspectrales_ajax',
            type: 'get',
            success: function (data) {
                var datos = new Array();
                for (i = 0; i < data.length; i++) {
                    var temp = new Array();

                    datos.push([data[i].fields.longitud_onda, data[i].fields.firma]);
                }
                var options = {
                    series: {
                        lines: {show: true},
                        points: {show: false},
                        color: "#00AC00",
                    }
                };
                $.plot($("#g-firmas"), [datos], options);
            }

        });

        $.ajax({
            data: {'id': id},
            url: '/libreria/parametros_ajax',
            type: 'get',
            success: function (data) {
                var datos = new Array();
                $("#t-individuo").text(title);
                $("#t-amax").text(data[0].fields.Amax);
                $("#t-k").text(data[0].fields.K);
                $("#t-rd").text(data[0].fields.Rd);
                $("#t-pcl").text(data[0].fields.PCL);

                var A, Rd, K, Amax;
                Rd = data[0].fields.Rd;
                K = data[0].fields.K;
                Amax = data[0].fields.Amax;

                for (i = 0; i < 2000; i++) {
                    A = ((Amax * i) / (K + i)) - Rd;
                    datos.push([i, A]);
                }
                var options = {
                    series: {
                        lines: {show: true},
                        points: {show: false},
                        color: "#00AC00",
                        clickable: true

                    }
                };
                $.plot($("#g-parametros"), [datos], options);

            }

        });

        $.ajax({
            data: {'id': id},
            url: '/libreria/indices_ajax',
            type: 'get',
            success: function (data) {
                $("#t-indice").text(title);
                $("#t-ndvi").text(data[0].fields.NDVI);
                $("#t-rdvi").text(data[0].fields.RDVi);
                $("#t-msr").text(data[0].fields.MSR);
                $("#t-msavi").text(data[0].fields.MSAVI);
                $("#t-mcari").text(data[0].fields.MCARI);
                $("#t-tvi").text(data[0].fields.TVI);
                $("#t-mcari1").text(data[0].fields.MCARI1);
                $("#t-mtvi1").text(data[0].fields.MTVI1);
                $("#t-mcari2").text(data[0].fields.MCARI2);
                $("#t-mtvi2").text(data[0].fields.MTVI2);
            }
        });
    });
}
function mapa(id, title) {
    $.ajax({
        data: {'id': id},
        url: '/libreria/coord_ajax',
        type: 'get',
        success: function (data) {
            var map;
            map = new GMaps({
                div: '#map',
                lat: data[0].fields.latitud,
                lng: data[0].fields.longitud,
                height: '100px',
                zoom: 17
            });
            map.addMarker({
                lat: data[0].fields.latitud,
                lng: data[0].fields.longitud,
                title: title
            });
        }
    });

}

function JSONToCSVConvertor(JSONData, ReportTitle, ShowLabel) {
    //If JSONData is not an object then JSON.parse will parse the JSON string in an Object
    var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;

    var CSV = '';
    //Set Report title in first row or line

    CSV += ReportTitle + '\r\n\n';

    //This condition will generate the Label/Header
    if (ShowLabel) {
        var row = "";

        //This loop will extract the label from 1st index of on array
        for (var index in arrData[0]) {

            //Now convert each value to string and comma-seprated
            row += index + ',';
        }

        row = row.slice(0, -1);

        //append Label row with line break
        CSV += row + '\r\n';
    }

    //1st loop is to extract each row
    for (var i = 0; i < arrData.length; i++) {
        var row = "";

        //2nd loop will extract each column and convert it in string comma-seprated
        for (var index in arrData[i]) {
            row += '"' + arrData[i][index] + '",';
        }

        row.slice(0, row.length - 1);

        //add a line break after each row
        CSV += row + '\r\n';
    }

    if (CSV == '') {
        alert("Invalid data");
        return;
    }

    //Generate a file name
    var fileName = "MyReport_";
    //this will remove the blank-spaces from the title and replace it with an underscore
    fileName += ReportTitle.replace(/ /g, "_");

    //Initialize file format you want csv or xls
    var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);

    // Now the little tricky part.
    // you can use either>> window.open(uri);
    // but this will not work in some browsers
    // or you will not get the correct file extension    

    //this trick will generate a temp <a /> tag
    var link = document.createElement("a");
    link.href = uri;

    //set the visibility hidden so it will not effect on your web-layout
    link.style = "visibility:hidden";
    link.download = fileName + ".csv";

    //this part will append the anchor tag and remove it after automatic click
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
}
