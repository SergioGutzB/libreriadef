--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.6
-- Dumped by pg_dump version 9.3.6
-- Started on 2015-04-14 18:34:22 COT

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 201 (class 3079 OID 11829)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2232 (class 0 OID 0)
-- Dependencies: 201
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 177 (class 1259 OID 16758)
-- Name: auth_group; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO postgres;

--
-- TOC entry 176 (class 1259 OID 16756)
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_id_seq OWNER TO postgres;

--
-- TOC entry 2233 (class 0 OID 0)
-- Dependencies: 176
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_group_id_seq OWNED BY auth_group.id;


--
-- TOC entry 179 (class 1259 OID 16768)
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO postgres;

--
-- TOC entry 178 (class 1259 OID 16766)
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_group_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_permissions_id_seq OWNER TO postgres;

--
-- TOC entry 2234 (class 0 OID 0)
-- Dependencies: 178
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_group_permissions_id_seq OWNED BY auth_group_permissions.id;


--
-- TOC entry 175 (class 1259 OID 16748)
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE auth_permission (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO postgres;

--
-- TOC entry 174 (class 1259 OID 16746)
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_permission_id_seq OWNER TO postgres;

--
-- TOC entry 2235 (class 0 OID 0)
-- Dependencies: 174
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_permission_id_seq OWNED BY auth_permission.id;


--
-- TOC entry 181 (class 1259 OID 16778)
-- Name: auth_user; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone NOT NULL,
    is_superuser boolean NOT NULL,
    username character varying(30) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(30) NOT NULL,
    email character varying(75) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


ALTER TABLE public.auth_user OWNER TO postgres;

--
-- TOC entry 183 (class 1259 OID 16788)
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.auth_user_groups OWNER TO postgres;

--
-- TOC entry 182 (class 1259 OID 16786)
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_user_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_groups_id_seq OWNER TO postgres;

--
-- TOC entry 2236 (class 0 OID 0)
-- Dependencies: 182
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_user_groups_id_seq OWNED BY auth_user_groups.id;


--
-- TOC entry 180 (class 1259 OID 16776)
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_id_seq OWNER TO postgres;

--
-- TOC entry 2237 (class 0 OID 0)
-- Dependencies: 180
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_user_id_seq OWNED BY auth_user.id;


--
-- TOC entry 185 (class 1259 OID 16798)
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_user_user_permissions OWNER TO postgres;

--
-- TOC entry 184 (class 1259 OID 16796)
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_user_user_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_user_permissions_id_seq OWNER TO postgres;

--
-- TOC entry 2238 (class 0 OID 0)
-- Dependencies: 184
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_user_user_permissions_id_seq OWNED BY auth_user_user_permissions.id;


--
-- TOC entry 187 (class 1259 OID 16852)
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE public.django_admin_log OWNER TO postgres;

--
-- TOC entry 186 (class 1259 OID 16850)
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE django_admin_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_admin_log_id_seq OWNER TO postgres;

--
-- TOC entry 2239 (class 0 OID 0)
-- Dependencies: 186
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE django_admin_log_id_seq OWNED BY django_admin_log.id;


--
-- TOC entry 173 (class 1259 OID 16738)
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE django_content_type (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO postgres;

--
-- TOC entry 172 (class 1259 OID 16736)
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE django_content_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_content_type_id_seq OWNER TO postgres;

--
-- TOC entry 2240 (class 0 OID 0)
-- Dependencies: 172
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE django_content_type_id_seq OWNED BY django_content_type.id;


--
-- TOC entry 171 (class 1259 OID 16727)
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.django_migrations OWNER TO postgres;

--
-- TOC entry 170 (class 1259 OID 16725)
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE django_migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_migrations_id_seq OWNER TO postgres;

--
-- TOC entry 2241 (class 0 OID 0)
-- Dependencies: 170
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE django_migrations_id_seq OWNED BY django_migrations.id;


--
-- TOC entry 188 (class 1259 OID 16874)
-- Name: django_session; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO postgres;

--
-- TOC entry 190 (class 1259 OID 16887)
-- Name: libreria_clasificacion_cientifica; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE libreria_clasificacion_cientifica (
    id integer NOT NULL,
    reino character varying(200) NOT NULL,
    division character varying(200) NOT NULL,
    clase character varying(200) NOT NULL,
    orden character varying(200) NOT NULL,
    familia character varying(200) NOT NULL,
    genero character varying(200) NOT NULL,
    "Especie_id" integer NOT NULL,
    subclase character varying(200) NOT NULL,
    subfamilia character varying(200) NOT NULL,
    subreino character varying(200) NOT NULL,
    tribu character varying(200) NOT NULL,
    seccion character varying(200) NOT NULL,
    subgenero character varying(200) NOT NULL
);


ALTER TABLE public.libreria_clasificacion_cientifica OWNER TO postgres;

--
-- TOC entry 189 (class 1259 OID 16885)
-- Name: libreria_clasificacion_cientifica_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE libreria_clasificacion_cientifica_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.libreria_clasificacion_cientifica_id_seq OWNER TO postgres;

--
-- TOC entry 2242 (class 0 OID 0)
-- Dependencies: 189
-- Name: libreria_clasificacion_cientifica_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE libreria_clasificacion_cientifica_id_seq OWNED BY libreria_clasificacion_cientifica.id;


--
-- TOC entry 192 (class 1259 OID 16898)
-- Name: libreria_cordenada; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE libreria_cordenada (
    id integer NOT NULL,
    norte double precision NOT NULL,
    este double precision NOT NULL,
    "Especie_id" integer NOT NULL,
    latitud double precision NOT NULL,
    longitud double precision NOT NULL
);


ALTER TABLE public.libreria_cordenada OWNER TO postgres;

--
-- TOC entry 191 (class 1259 OID 16896)
-- Name: libreria_cordenada_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE libreria_cordenada_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.libreria_cordenada_id_seq OWNER TO postgres;

--
-- TOC entry 2243 (class 0 OID 0)
-- Dependencies: 191
-- Name: libreria_cordenada_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE libreria_cordenada_id_seq OWNED BY libreria_cordenada.id;


--
-- TOC entry 194 (class 1259 OID 16906)
-- Name: libreria_especie; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE libreria_especie (
    id integer NOT NULL,
    codigo character varying(10) NOT NULL,
    nombre_comun character varying(100) NOT NULL,
    nombre_cientifico character varying(200) NOT NULL,
    fecha_captura timestamp with time zone NOT NULL,
    sensor character varying(30) NOT NULL,
    "IRGA" character varying(30) NOT NULL
);


ALTER TABLE public.libreria_especie OWNER TO postgres;

--
-- TOC entry 193 (class 1259 OID 16904)
-- Name: libreria_especie_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE libreria_especie_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.libreria_especie_id_seq OWNER TO postgres;

--
-- TOC entry 2244 (class 0 OID 0)
-- Dependencies: 193
-- Name: libreria_especie_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE libreria_especie_id_seq OWNED BY libreria_especie.id;


--
-- TOC entry 196 (class 1259 OID 16914)
-- Name: libreria_firma_espectral; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE libreria_firma_espectral (
    id integer NOT NULL,
    longitud_onda integer NOT NULL,
    firma double precision NOT NULL,
    "Especie_id" integer NOT NULL
);


ALTER TABLE public.libreria_firma_espectral OWNER TO postgres;

--
-- TOC entry 195 (class 1259 OID 16912)
-- Name: libreria_firma_espectral_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE libreria_firma_espectral_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.libreria_firma_espectral_id_seq OWNER TO postgres;

--
-- TOC entry 2245 (class 0 OID 0)
-- Dependencies: 195
-- Name: libreria_firma_espectral_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE libreria_firma_espectral_id_seq OWNED BY libreria_firma_espectral.id;


--
-- TOC entry 198 (class 1259 OID 16922)
-- Name: libreria_indice_vegetacion; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE libreria_indice_vegetacion (
    id integer NOT NULL,
    "NDVI" double precision NOT NULL,
    "RDVi" double precision NOT NULL,
    "MSR" double precision NOT NULL,
    "MSAVI" double precision NOT NULL,
    "MCARI" double precision NOT NULL,
    "TVI" double precision NOT NULL,
    "MCARI1" double precision NOT NULL,
    "MTVI1" double precision NOT NULL,
    "MCARI2" double precision NOT NULL,
    "MTVI2" double precision NOT NULL,
    "Especie_id" integer NOT NULL
);


ALTER TABLE public.libreria_indice_vegetacion OWNER TO postgres;

--
-- TOC entry 197 (class 1259 OID 16920)
-- Name: libreria_indice_vegetacion_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE libreria_indice_vegetacion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.libreria_indice_vegetacion_id_seq OWNER TO postgres;

--
-- TOC entry 2246 (class 0 OID 0)
-- Dependencies: 197
-- Name: libreria_indice_vegetacion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE libreria_indice_vegetacion_id_seq OWNED BY libreria_indice_vegetacion.id;


--
-- TOC entry 200 (class 1259 OID 16930)
-- Name: libreria_parametros_curva_luz; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE libreria_parametros_curva_luz (
    id integer NOT NULL,
    "Amax" double precision NOT NULL,
    "K" double precision NOT NULL,
    "Rd" double precision NOT NULL,
    "PCL" double precision NOT NULL,
    "Especie_id" integer NOT NULL
);


ALTER TABLE public.libreria_parametros_curva_luz OWNER TO postgres;

--
-- TOC entry 199 (class 1259 OID 16928)
-- Name: libreria_parametros_curva_luz_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE libreria_parametros_curva_luz_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.libreria_parametros_curva_luz_id_seq OWNER TO postgres;

--
-- TOC entry 2247 (class 0 OID 0)
-- Dependencies: 199
-- Name: libreria_parametros_curva_luz_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE libreria_parametros_curva_luz_id_seq OWNED BY libreria_parametros_curva_luz.id;


--
-- TOC entry 1996 (class 2604 OID 16761)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group ALTER COLUMN id SET DEFAULT nextval('auth_group_id_seq'::regclass);


--
-- TOC entry 1997 (class 2604 OID 16771)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('auth_group_permissions_id_seq'::regclass);


--
-- TOC entry 1995 (class 2604 OID 16751)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_permission ALTER COLUMN id SET DEFAULT nextval('auth_permission_id_seq'::regclass);


--
-- TOC entry 1998 (class 2604 OID 16781)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user ALTER COLUMN id SET DEFAULT nextval('auth_user_id_seq'::regclass);


--
-- TOC entry 1999 (class 2604 OID 16791)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_groups ALTER COLUMN id SET DEFAULT nextval('auth_user_groups_id_seq'::regclass);


--
-- TOC entry 2000 (class 2604 OID 16801)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('auth_user_user_permissions_id_seq'::regclass);


--
-- TOC entry 2001 (class 2604 OID 16855)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_admin_log ALTER COLUMN id SET DEFAULT nextval('django_admin_log_id_seq'::regclass);


--
-- TOC entry 1994 (class 2604 OID 16741)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_content_type ALTER COLUMN id SET DEFAULT nextval('django_content_type_id_seq'::regclass);


--
-- TOC entry 1993 (class 2604 OID 16730)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_migrations ALTER COLUMN id SET DEFAULT nextval('django_migrations_id_seq'::regclass);


--
-- TOC entry 2003 (class 2604 OID 16890)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY libreria_clasificacion_cientifica ALTER COLUMN id SET DEFAULT nextval('libreria_clasificacion_cientifica_id_seq'::regclass);


--
-- TOC entry 2004 (class 2604 OID 16901)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY libreria_cordenada ALTER COLUMN id SET DEFAULT nextval('libreria_cordenada_id_seq'::regclass);


--
-- TOC entry 2005 (class 2604 OID 16909)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY libreria_especie ALTER COLUMN id SET DEFAULT nextval('libreria_especie_id_seq'::regclass);


--
-- TOC entry 2006 (class 2604 OID 16917)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY libreria_firma_espectral ALTER COLUMN id SET DEFAULT nextval('libreria_firma_espectral_id_seq'::regclass);


--
-- TOC entry 2007 (class 2604 OID 16925)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY libreria_indice_vegetacion ALTER COLUMN id SET DEFAULT nextval('libreria_indice_vegetacion_id_seq'::regclass);


--
-- TOC entry 2008 (class 2604 OID 16933)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY libreria_parametros_curva_luz ALTER COLUMN id SET DEFAULT nextval('libreria_parametros_curva_luz_id_seq'::regclass);


--
-- TOC entry 2201 (class 0 OID 16758)
-- Dependencies: 177
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY auth_group (id, name) FROM stdin;
\.


--
-- TOC entry 2248 (class 0 OID 0)
-- Dependencies: 176
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auth_group_id_seq', 1, false);


--
-- TOC entry 2203 (class 0 OID 16768)
-- Dependencies: 179
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY auth_group_permissions (id, group_id, permission_id) FROM stdin;
\.


--
-- TOC entry 2249 (class 0 OID 0)
-- Dependencies: 178
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auth_group_permissions_id_seq', 1, false);


--
-- TOC entry 2199 (class 0 OID 16748)
-- Dependencies: 175
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add log entry	1	add_logentry
2	Can change log entry	1	change_logentry
3	Can delete log entry	1	delete_logentry
4	Can add permission	2	add_permission
5	Can change permission	2	change_permission
6	Can delete permission	2	delete_permission
7	Can add group	3	add_group
8	Can change group	3	change_group
9	Can delete group	3	delete_group
10	Can add user	4	add_user
11	Can change user	4	change_user
12	Can delete user	4	delete_user
13	Can add content type	5	add_contenttype
14	Can change content type	5	change_contenttype
15	Can delete content type	5	delete_contenttype
16	Can add session	6	add_session
17	Can change session	6	change_session
18	Can delete session	6	delete_session
19	Can add especie	7	add_especie
20	Can change especie	7	change_especie
21	Can delete especie	7	delete_especie
22	Can add parametros_curva_luz	8	add_parametros_curva_luz
23	Can change parametros_curva_luz	8	change_parametros_curva_luz
24	Can delete parametros_curva_luz	8	delete_parametros_curva_luz
25	Can add indice_vegetacion	9	add_indice_vegetacion
26	Can change indice_vegetacion	9	change_indice_vegetacion
27	Can delete indice_vegetacion	9	delete_indice_vegetacion
28	Can add cordenada	10	add_cordenada
29	Can change cordenada	10	change_cordenada
30	Can delete cordenada	10	delete_cordenada
31	Can add firma_espectral	11	add_firma_espectral
32	Can change firma_espectral	11	change_firma_espectral
33	Can delete firma_espectral	11	delete_firma_espectral
34	Can add clasificacion_cientifica	12	add_clasificacion_cientifica
35	Can change clasificacion_cientifica	12	change_clasificacion_cientifica
36	Can delete clasificacion_cientifica	12	delete_clasificacion_cientifica
\.


--
-- TOC entry 2250 (class 0 OID 0)
-- Dependencies: 174
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auth_permission_id_seq', 36, true);


--
-- TOC entry 2205 (class 0 OID 16778)
-- Dependencies: 181
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
1	pbkdf2_sha256$15000$ry7b7YQSFA8a$9qW66twlI1Em0vRJ33a+TYRo+OZ0hJK6YcuuTtWfAKo=	2015-04-12 18:51:04.096661-05	t	sergio			sergut18@gmail.com	t	t	2015-02-15 18:13:14.32187-05
2	pbkdf2_sha256$15000$NzQFTUzxo1aL$GMKeROc5Z1GehHcNb6p21xseNTTUToYBRHaRTlPZ5A4=	2015-04-12 18:51:27-05	t	admin				t	t	2015-04-12 18:51:27-05
\.


--
-- TOC entry 2207 (class 0 OID 16788)
-- Dependencies: 183
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY auth_user_groups (id, user_id, group_id) FROM stdin;
\.


--
-- TOC entry 2251 (class 0 OID 0)
-- Dependencies: 182
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auth_user_groups_id_seq', 1, false);


--
-- TOC entry 2252 (class 0 OID 0)
-- Dependencies: 180
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auth_user_id_seq', 2, true);


--
-- TOC entry 2209 (class 0 OID 16798)
-- Dependencies: 185
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
\.


--
-- TOC entry 2253 (class 0 OID 0)
-- Dependencies: 184
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auth_user_user_permissions_id_seq', 1, false);


--
-- TOC entry 2211 (class 0 OID 16852)
-- Dependencies: 187
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
1	2015-02-15 18:20:23.485507-05	1	Magnolio	1		7	1
2	2015-02-15 18:22:15.552696-05	2	Pino Colombiano	1		7	1
3	2015-02-15 18:23:52.099576-05	3	Roble	1		7	1
4	2015-02-15 18:24:32.043451-05	4	Gaque	1		7	1
5	2015-02-15 18:25:20.964456-05	5	Sietecueros	1		7	1
6	2015-02-15 18:27:44.847931-05	1	Plantae	1		12	1
7	2015-02-15 18:29:36.927841-05	1	Indice_vegetacion object	1		9	1
8	2015-02-15 18:29:43.653678-05	1	Indice_vegetacion object	2	Modificado/a NDVI.	9	1
9	2015-02-15 18:30:26.450123-05	2	Indice_vegetacion object	1		9	1
10	2015-02-15 18:31:51.23063-05	3	Indice_vegetacion object	1		9	1
11	2015-02-15 18:33:06.346444-05	4	Indice_vegetacion object	1		9	1
12	2015-02-15 18:34:13.448776-05	5	Indice_vegetacion object	1		9	1
13	2015-02-15 18:36:55.229524-05	1	Parametros_curva_luz object	1		8	1
14	2015-02-15 18:37:26.283007-05	2	Parametros_curva_luz object	1		8	1
15	2015-02-15 18:38:10.431093-05	3	Parametros_curva_luz object	1		8	1
16	2015-02-15 18:38:43.192826-05	4	Parametros_curva_luz object	1		8	1
17	2015-02-15 18:39:34.433315-05	5	Parametros_curva_luz object	1		8	1
18	2015-02-15 18:44:04.705473-05	2	Firma_espectral object	1		11	1
19	2015-02-15 18:49:13.19557-05	2	Firma_espectral object	2	Modificado/a firma.	11	1
20	2015-02-15 18:49:30.120995-05	3	Firma_espectral object	1		11	1
21	2015-02-15 18:49:45.62067-05	4	Firma_espectral object	1		11	1
22	2015-02-15 18:49:57.800392-05	5	Firma_espectral object	1		11	1
23	2015-02-15 18:50:12.316972-05	6	Firma_espectral object	1		11	1
24	2015-03-15 16:44:08.211469-05	1	Plantae	2	Modificado/a subreino, subclase y subfamilia.	12	1
25	2015-03-15 16:44:58.012571-05	2	Plantae	1		12	1
26	2015-03-15 16:45:40.708899-05	3	Plantae	1		12	1
27	2015-03-15 16:46:21.602058-05	4	Plantae	1		12	1
28	2015-03-15 16:48:04.146919-05	4	Plantae	2	Modificado/a subgenero y seccion.	12	1
29	2015-03-15 16:49:15.401433-05	5	Plantae	1		12	1
30	2015-03-15 17:11:07.443748-05	1	Cordenada object	1		10	1
31	2015-03-15 17:11:37.484562-05	2	Cordenada object	1		10	1
32	2015-03-15 17:12:02.648378-05	3	Cordenada object	1		10	1
33	2015-03-15 17:12:25.073887-05	4	Cordenada object	1		10	1
34	2015-03-15 17:12:52.980365-05	5	Cordenada object	1		10	1
35	2015-03-15 21:39:10.661972-05	5	Cordenada object	2	No ha cambiado ningún campo.	10	1
36	2015-03-15 21:39:31.685787-05	4	Cordenada object	2	Modificado/a longitud.	10	1
37	2015-03-15 21:39:44.45207-05	3	Cordenada object	2	No ha cambiado ningún campo.	10	1
38	2015-03-15 21:39:58.362772-05	2	Cordenada object	2	No ha cambiado ningún campo.	10	1
39	2015-03-15 21:40:10.886134-05	1	Cordenada object	2	No ha cambiado ningún campo.	10	1
40	2015-03-15 21:52:05.989606-05	4	Cordenada object	2	No ha cambiado ningún campo.	10	1
41	2015-03-15 21:53:13.628549-05	4	Cordenada object	2	No ha cambiado ningún campo.	10	1
42	2015-03-15 21:53:45.323329-05	4	Cordenada object	2	Modificado/a longitud.	10	1
43	2015-04-12 18:51:27.766183-05	2	admin	1		4	1
44	2015-04-12 18:51:43.545294-05	2	admin	2	Modificado/a is_superuser.	4	1
45	2015-04-12 18:51:55.518263-05	2	admin	2	Modificado/a is_staff.	4	1
46	2015-04-12 18:53:10.017134-05	5	Tibouchina Lepidota	2	Modificado/a fecha_captura.	7	1
47	2015-04-12 18:53:44.061288-05	4	Clusia Multiflora	2	Modificado/a fecha_captura.	7	1
48	2015-04-12 18:54:19.311145-05	3	Querus Humboldtii	2	Modificado/a fecha_captura.	7	1
49	2015-04-12 18:55:02.744179-05	2	Podocarpus Oleifolius	2	Modificado/a fecha_captura.	7	1
50	2015-04-12 18:55:38.575251-05	1	Magnolia Grandiflora	2	Modificado/a fecha_captura.	7	1
\.


--
-- TOC entry 2254 (class 0 OID 0)
-- Dependencies: 186
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('django_admin_log_id_seq', 50, true);


--
-- TOC entry 2197 (class 0 OID 16738)
-- Dependencies: 173
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY django_content_type (id, name, app_label, model) FROM stdin;
1	log entry	admin	logentry
2	permission	auth	permission
3	group	auth	group
4	user	auth	user
5	content type	contenttypes	contenttype
6	session	sessions	session
7	especie	libreria	especie
8	parametros_curva_luz	libreria	parametros_curva_luz
9	indice_vegetacion	libreria	indice_vegetacion
10	cordenada	libreria	cordenada
11	firma_espectral	libreria	firma_espectral
12	clasificacion_cientifica	libreria	clasificacion_cientifica
\.


--
-- TOC entry 2255 (class 0 OID 0)
-- Dependencies: 172
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('django_content_type_id_seq', 12, true);


--
-- TOC entry 2195 (class 0 OID 16727)
-- Dependencies: 171
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2015-02-15 17:28:00.862673-05
2	auth	0001_initial	2015-02-15 17:28:01.837892-05
3	admin	0001_initial	2015-02-15 17:28:02.095056-05
4	sessions	0001_initial	2015-02-15 17:28:02.276521-05
5	libreria	0001_initial	2015-02-15 18:07:12.715466-05
6	libreria	0002_auto_20150315_2126	2015-03-15 16:26:41.431437-05
7	libreria	0003_auto_20150315_2131	2015-03-15 16:31:43.098517-05
8	libreria	0004_auto_20150315_2143	2015-03-15 16:43:32.123645-05
9	libreria	0005_auto_20150315_2147	2015-03-15 16:47:11.845608-05
10	libreria	0006_auto_20150315_2149	2015-03-15 16:49:09.038667-05
11	libreria	0007_auto_20150315_2208	2015-03-15 17:08:48.167993-05
12	libreria	0008_auto_20150315_2209	2015-03-15 17:10:01.945082-05
\.


--
-- TOC entry 2256 (class 0 OID 0)
-- Dependencies: 170
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('django_migrations_id_seq', 12, true);


--
-- TOC entry 2212 (class 0 OID 16874)
-- Dependencies: 188
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY django_session (session_key, session_data, expire_date) FROM stdin;
292v41swn48z0u9d34u2dwbljyqq7u38	MjdmZGUxNGMyYmFmMGFmYzg3ZTg4ZjU3ZTJjNWIwNDVkZTQ5ZTFkNDp7Il9hdXRoX3VzZXJfaGFzaCI6IjgwZmQyM2E3MjVjN2Y3NDUzMTQ5MTY3ZTE2N2ZhMzU4OWM5Y2MxNGMiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjF9	2015-03-01 18:13:42.132906-05
lbml2mx2o5jd6nt839bvnblv1pvosra3	MjdmZGUxNGMyYmFmMGFmYzg3ZTg4ZjU3ZTJjNWIwNDVkZTQ5ZTFkNDp7Il9hdXRoX3VzZXJfaGFzaCI6IjgwZmQyM2E3MjVjN2Y3NDUzMTQ5MTY3ZTE2N2ZhMzU4OWM5Y2MxNGMiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjF9	2015-03-29 16:24:56.823228-05
c5rkpthmq9otj7r4fgfvum8cj0wbisej	MjdmZGUxNGMyYmFmMGFmYzg3ZTg4ZjU3ZTJjNWIwNDVkZTQ5ZTFkNDp7Il9hdXRoX3VzZXJfaGFzaCI6IjgwZmQyM2E3MjVjN2Y3NDUzMTQ5MTY3ZTE2N2ZhMzU4OWM5Y2MxNGMiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjF9	2015-03-29 19:11:17.037561-05
e6ffew69zo8el3kioqyeo0jaohdcstt8	MjdmZGUxNGMyYmFmMGFmYzg3ZTg4ZjU3ZTJjNWIwNDVkZTQ5ZTFkNDp7Il9hdXRoX3VzZXJfaGFzaCI6IjgwZmQyM2E3MjVjN2Y3NDUzMTQ5MTY3ZTE2N2ZhMzU4OWM5Y2MxNGMiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjF9	2015-04-26 18:51:04.146548-05
\.


--
-- TOC entry 2214 (class 0 OID 16887)
-- Dependencies: 190
-- Data for Name: libreria_clasificacion_cientifica; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY libreria_clasificacion_cientifica (id, reino, division, clase, orden, familia, genero, "Especie_id", subclase, subfamilia, subreino, tribu, seccion, subgenero) FROM stdin;
1	Plantae	Pinophyta	Pinopsida	Pinales	Podocarpaceae	Podocarpus	2						
2	Plantae	Magnoliophyta	Magnoliopsida	Magnoliales	Magnoliaceae	Magnolia	1	Magnoliidae	Magnolioideae	Tracheobionta	Magnolieae		
3	Plantae	Magnoliophyta	Magnoliopsida	Myrtales	Melastomataceae	Tibouchina	5						
4	Plantae	Angiospermae	Eudicotyledoneae	Fagales	Fagaceae	Quercus	3	Rosidae				Lobatae	Quercus
5	Plantae	Magnoliophyta	Magnoliopsida	Theales	Clusiaceae	Clusia	4		Clusioideae		Clusieae		
\.


--
-- TOC entry 2257 (class 0 OID 0)
-- Dependencies: 189
-- Name: libreria_clasificacion_cientifica_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('libreria_clasificacion_cientifica_id_seq', 5, true);


--
-- TOC entry 2216 (class 0 OID 16898)
-- Dependencies: 192
-- Data for Name: libreria_cordenada; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY libreria_cordenada (id, norte, este, "Especie_id", latitud, longitud) FROM stdin;
5	1007929.02489999996	997504.442099999986	1	4.66790304099999975	-74.0999999999999943
3	1007954.93500000006	997575.79449999996	5	4.66813736900000009	-74.0993569190000017
2	1007984.54720000003	997369.035499999998	4	4.6684050939999997	-74.1012204160000039
1	1008077.89069999999	997542.762100000051	3	4.66924925899999987	-74.0996546700000067
4	1008040.34439999994	997615.398300000001	2	4.66890974499999967	-74.0990000000000038
\.


--
-- TOC entry 2258 (class 0 OID 0)
-- Dependencies: 191
-- Name: libreria_cordenada_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('libreria_cordenada_id_seq', 5, true);


--
-- TOC entry 2218 (class 0 OID 16906)
-- Dependencies: 194
-- Data for Name: libreria_especie; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY libreria_especie (id, codigo, nombre_comun, nombre_cientifico, fecha_captura, sensor, "IRGA") FROM stdin;
5	A0005	Sietecueros	Tibouchina Lepidota	2014-06-10 03:56:10-05	HandHelad-2	TPS-2
4	A0004	Gaque	Clusia Multiflora	2014-05-26 04:03:00-05	HandHelad-2	TPS-2
3	A0003	Roble	Querus Humboldtii	2014-05-23 04:13:00-05	HandHelad-2	TPS-2
2	A0002	Pino Colombiano	Podocarpus Oleifolius	2014-06-05 03:35:00-05	HandHelad-2	TPS-2
1	A0001	Magnolio	Magnolia Grandiflora	2014-05-23 04:39:00-05	HandHelad-2	TPS-2
\.


--
-- TOC entry 2259 (class 0 OID 0)
-- Dependencies: 193
-- Name: libreria_especie_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('libreria_especie_id_seq', 5, true);


--
-- TOC entry 2220 (class 0 OID 16914)
-- Dependencies: 196
-- Data for Name: libreria_firma_espectral; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY libreria_firma_espectral (id, longitud_onda, firma, "Especie_id") FROM stdin;
2	325	0.0398206569254000026	1
3	325	0.0463778206280300032	2
4	325	0.0434470087289810028	5
5	325	0.0690333843231201033	4
6	325	0.0911612436175347068	3
7	326	0.0406751083210111011	1
8	327	0.0417164240032434006	1
9	328	0.0398984178900718967	1
10	329	0.0384396376088262035	1
11	330	0.0392537236213683985	1
12	331	0.0394112514331936975	1
13	332	0.0385603232309221985	1
14	333	0.038353188894689097	1
15	334	0.0386470211669803013	1
16	335	0.0390483228489756987	1
17	336	0.0384545881301164988	1
18	337	0.0378103801980615023	1
19	338	0.0379901057109236981	1
20	339	0.0381975909695028981	1
21	340	0.0378618471324443998	1
22	341	0.0370134929195046966	1
23	342	0.0367735428735614031	1
24	343	0.0375805038958788029	1
25	344	0.0376870080828667006	1
26	345	0.0372482094913721001	1
27	346	0.0370507694780827027	1
28	347	0.0367152184247971011	1
29	348	0.0366729253903030974	1
30	349	0.0362115526571870003	1
31	350	0.036157622747123197	1
32	351	0.0366813346743583013	1
33	352	0.0367296479642391024	1
34	353	0.0363723713904618995	1
35	354	0.0363552825525403009	1
36	355	0.0362959694117307996	1
37	356	0.0361321758478879984	1
38	357	0.0359059823676943987	1
39	358	0.0356532318517566033	1
40	359	0.0355601811781526	1
41	360	0.0354465231299399969	1
42	361	0.0352690508589149007	1
43	362	0.0354038000106811981	1
44	363	0.0355483084917068981	1
45	364	0.0351493278518318977	1
46	365	0.0349437480792402985	1
47	366	0.0349166529253124996	1
48	367	0.0349630171433090997	1
49	368	0.0348861142992972981	1
50	369	0.0346183363348245982	1
51	370	0.0346905827522277985	1
52	371	0.0346433183178306014	1
53	372	0.0339602995663881024	1
54	373	0.0338213467970489984	1
55	374	0.0341539928689598982	1
56	375	0.0337788911536335973	1
57	376	0.0333356725051998995	1
58	377	0.0335954457521438973	1
59	378	0.0338059278205036992	1
60	379	0.0338530596345663001	1
61	380	0.033888167142867999	1
62	381	0.0338305700570344994	1
63	382	0.0333951424807309993	1
64	383	0.0332842497155070027	1
65	384	0.0333096124231814991	1
66	385	0.0331812545657158009	1
67	386	0.0331753317266703027	1
68	387	0.0332784870639442967	1
69	388	0.0331617716699837972	1
70	389	0.0330861441791057975	1
71	390	0.0328432600945234021	1
72	391	0.0326195752248168019	1
73	392	0.0325265532359481011	1
74	393	0.0323421645909547986	1
75	394	0.0322553666308521975	1
76	395	0.0322909135371447012	1
77	396	0.0321820557117461978	1
78	397	0.0319493342190980967	1
79	398	0.0319564931094645996	1
80	399	0.0320820443332194977	1
81	400	0.032200326770543998	1
82	401	0.0321843732148408973	1
83	402	0.0322371559217572018	1
84	403	0.0323637681081890977	1
85	404	0.032312270812690301	1
86	405	0.0321287987753749033	1
87	406	0.0321223042905329978	1
88	407	0.032297316938638701	1
89	408	0.0325197210535407011	1
90	409	0.0325318893417715985	1
91	410	0.0325019041076302986	1
92	411	0.0325378779321909006	1
93	412	0.0326953487470746026	1
94	413	0.0328952003270387983	1
95	414	0.0329812882468104029	1
96	415	0.0330716444179416022	1
97	416	0.0330964652821420999	1
98	417	0.0332384273409844014	1
99	418	0.0333988118916749982	1
100	419	0.0334962107241153981	1
101	420	0.0336070781573652999	1
102	421	0.0337597314268351031	1
103	422	0.0339188443496823006	1
104	423	0.0340284049510956033	1
105	424	0.0340485682711004975	1
106	425	0.034198683872818901	1
107	426	0.0344311172142625008	1
108	427	0.0344113450497389034	1
109	428	0.034428824856877302	1
110	429	0.0345921870321034969	1
111	430	0.034663018584251401	1
112	431	0.0347005823627114032	1
113	432	0.034795327670872199	1
114	433	0.0348414979875088029	1
115	434	0.0348516115918754976	1
116	435	0.0348929513245821027	1
117	436	0.0349640579894184983	1
118	437	0.0350094746798276998	1
119	438	0.0350413616746663992	1
120	439	0.0350952468812466001	1
121	440	0.0350726544857025008	1
122	441	0.0351112768054007984	1
123	442	0.0351918015629053005	1
124	443	0.0352441286668181003	1
125	444	0.0352999739348887967	1
126	445	0.0354268139228225004	1
127	446	0.0354566447436808971	1
128	447	0.0354169350117444978	1
129	448	0.0355525901541113992	1
130	449	0.0357114840298890998	1
131	450	0.0357890063896774968	1
132	451	0.0357996450737119026	1
133	452	0.0358169773593544974	1
134	453	0.0359410537406801983	1
135	454	0.0360578835010528995	1
136	455	0.0361257605254649977	1
137	456	0.0360840694978833004	1
138	457	0.0360128283500672011	1
139	458	0.0360939266160130029	1
140	459	0.0361469158902764029	1
141	460	0.0361137323081493974	1
142	461	0.0360504630953073987	1
143	462	0.036003169044852297	1
144	463	0.035996765829622801	1
145	464	0.0360976690426468988	1
146	465	0.0361618742346763999	1
147	466	0.0360682494938373982	1
148	467	0.0360023066401482031	1
149	468	0.0360076898708939983	1
150	469	0.0359605832025408981	1
151	470	0.0359361682087182971	1
152	471	0.0359900524839759026	1
153	472	0.0360220741480588996	1
154	473	0.0360314520075916966	1
155	474	0.0359933318570255997	1
156	475	0.0359445009380578995	1
157	476	0.0359370285645127033	1
158	477	0.0359662460163236025	1
159	478	0.0359783967956900999	1
160	479	0.0359594877809285979	1
161	480	0.0360418701544404016	1
162	481	0.0362017761915921985	1
163	482	0.0362182594835758001	1
164	483	0.0361825181171298027	1
165	484	0.0362278264015912996	1
166	485	0.0362945193424821025	1
167	486	0.0363593315705657005	1
168	487	0.0363619768992066023	1
169	488	0.0363498572260141026	1
170	489	0.0363550728186965016	1
171	490	0.0364959573373198967	1
172	491	0.0367086628451942995	1
173	492	0.0368438990786671972	1
174	493	0.0369501071050762978	1
175	494	0.0370926512405276021	1
176	495	0.0373012637719511986	1
177	496	0.0375770194455981033	1
178	497	0.0379031296819447999	1
179	498	0.0382139610126615015	1
180	499	0.0385179053992033005	1
181	500	0.039038565009832403	1
182	501	0.0395676057785750018	1
183	502	0.0400000018998981008	1
184	503	0.040516031347215202	1
185	504	0.0411553829908370972	1
186	505	0.041848882660269697	1
187	506	0.0426896240562201004	1
188	507	0.0436596415936946966	1
189	508	0.0445955015718937017	1
190	509	0.0455948639661074018	1
191	510	0.0468096155673266012	1
192	511	0.0481013767421245991	1
193	512	0.049462601169943797	1
194	513	0.0509953498840332031	1
195	514	0.0526682954281569027	1
196	515	0.0544381041079759972	1
197	516	0.0562861010432242972	1
198	517	0.0582761604338883979	1
199	518	0.0604851868003606977	1
200	519	0.0628523174673319002	1
201	520	0.0652420993894338996	1
202	521	0.0674990694969893057	1
203	522	0.0697301898151636013	1
204	523	0.0721046466380357992	1
205	524	0.0744411610066890023	1
206	525	0.0767166249454021981	1
207	526	0.0790211237967014951	1
208	527	0.0812443546950817053	1
209	528	0.0833128385245800046	1
210	529	0.0851815998554228959	1
211	530	0.086887724697589902	1
212	531	0.0885749958455562952	1
213	532	0.0901512332260608062	1
214	533	0.0915971376001834953	1
215	534	0.0928848974406718958	1
216	535	0.0940873324871064065	1
217	536	0.0953165113925933949	1
218	537	0.0964029558002948983	1
219	538	0.0973263017833232991	1
220	539	0.0981431312859059046	1
221	540	0.0989144347608088975	1
222	541	0.0996921092271804976	1
223	542	0.100486750155687005	1
224	543	0.101272013783455003	1
225	544	0.101994246244431	1
226	545	0.102703784406184995	1
227	546	0.103406113386154005	1
228	547	0.104092468321323003	1
229	548	0.104751579463482	1
230	549	0.105379138141869999	1
231	550	0.105925039201975005	1
232	551	0.106369382888078998	1
233	552	0.106715438514947997	1
234	553	0.106893701106309996	1
235	554	0.106904529035090998	1
236	555	0.106815517693757994	1
237	556	0.106529840826988006	1
238	557	0.105942843109368995	1
239	558	0.105240236222744005	1
240	559	0.104475784301757996	1
241	560	0.103603575378655999	1
242	561	0.102665596455336003	1
243	562	0.101690261811018007	1
244	563	0.100628174841404003	1
245	564	0.099455668032169306	1
246	565	0.0982634782791138028	1
247	566	0.0969307854771614963	1
248	567	0.0954275846481322937	1
249	568	0.0938640877604484947	1
250	569	0.092225450277328494	1
251	570	0.0905014552175999049	1
252	571	0.0887523248791694946	1
253	572	0.0870121233165263019	1
254	573	0.0852431520819663946	1
255	574	0.0835588842630387019	1
256	575	0.0819911301136017068	1
257	576	0.0804729849100112027	1
258	577	0.0790580064058303972	1
259	578	0.0778402499854564944	1
260	579	0.0767298527061939933	1
261	580	0.0756848216056824064	1
262	581	0.0747118085622787975	1
263	582	0.0738177638500928934	1
264	583	0.073010113462805698	1
265	584	0.0723173357546330053	1
266	585	0.0716769240796566037	1
267	586	0.0710218533873557961	1
268	587	0.0704036552459001985	1
269	588	0.0698448766022921053	1
270	589	0.0692966900765895066	1
271	590	0.0687189232558011037	1
272	591	0.0681612838059663939	1
273	592	0.0676487077027558975	1
274	593	0.0672015141695737062	1
275	594	0.066822005808353499	1
276	595	0.0665168341249228051	1
277	596	0.0662626732140779023	1
278	597	0.0659795790910721047	1
279	598	0.0656715907156466966	1
280	599	0.0653787694871425934	1
281	600	0.0650553841143847039	1
282	601	0.0647051416337490054	1
283	602	0.0642674356698989951	1
284	603	0.0637483097612858013	1
285	604	0.0632281295955181011	1
286	605	0.0627274062484503014	1
287	606	0.0622598681598902012	1
288	607	0.0617348607629537027	1
289	608	0.0611351229250430978	1
290	609	0.0604474287480116015	1
291	610	0.0597487598657608018	1
292	611	0.059030382707714997	1
293	612	0.0582731403410435	1
294	613	0.0575243853032589014	1
295	614	0.0567920368164777992	1
296	615	0.0560065891593694992	1
297	616	0.0552902188152075008	1
298	617	0.0546466570347547975	1
299	618	0.0539337322115897994	1
300	619	0.0532371103763581016	1
301	620	0.0528160884976386982	1
302	621	0.0524384103715420033	1
303	622	0.0520165283232928016	1
304	623	0.0516082528978586003	1
305	624	0.0512597672641278007	1
306	625	0.0510002788156270967	1
307	626	0.0507592644542455979	1
308	627	0.0505625300109387027	1
309	628	0.0504939194768668012	1
310	629	0.0504539817571640001	1
311	630	0.0503958843648434004	1
312	631	0.0502520292997359966	1
313	632	0.0500601120293139995	1
314	633	0.0498675495386124018	1
315	634	0.0496634654700756004	1
316	635	0.0494012992829084979	1
317	636	0.0490779399871826005	1
318	637	0.048685117810964601	1
319	638	0.0482079844921826997	1
320	639	0.0476903837174176976	1
321	640	0.0471303865313529996	1
322	641	0.0464578710496426003	1
323	642	0.0458076462149620001	1
324	643	0.0452086191624403028	1
325	644	0.0445572059601545001	1
326	645	0.0438817802816630007	1
327	646	0.0431893065571785015	1
328	647	0.042571375146508203	1
329	648	0.0420661579817534007	1
330	649	0.0415270701050758015	1
331	650	0.0409796312451363026	1
332	651	0.0405896384268999016	1
333	652	0.0401652306318283012	1
334	653	0.0397237623110413968	1
335	654	0.0394309490919112979	1
336	655	0.0390859656035899977	1
337	656	0.0386311361566186034	1
338	657	0.038112348318100002	1
339	658	0.0375787742435932035	1
340	659	0.0369756840169429973	1
341	660	0.0363969532772899021	1
342	661	0.0358728514984249999	1
343	662	0.0354172132909297971	1
344	663	0.0349439103156327993	1
345	664	0.034408725425601	1
346	665	0.0339593561366200031	1
347	666	0.0336232345551251977	1
348	667	0.0333339180797338971	1
349	668	0.0331036083400250009	1
350	669	0.0329409550875425991	1
351	670	0.032764238305389902	1
352	671	0.032666668854653802	1
353	672	0.0327037448063492997	1
354	673	0.0327416582033037973	1
355	674	0.0327960489317774967	1
356	675	0.0328940238803624996	1
357	676	0.0330102838575839982	1
358	677	0.0331489445641637015	1
359	678	0.0333956889808178017	1
360	679	0.033680559135973398	1
361	680	0.0340049544349313032	1
362	681	0.0343830814585089004	1
363	682	0.0348446745425462986	1
364	683	0.0354489903897046987	1
365	684	0.0362174727022648024	1
366	685	0.0371048307046294018	1
367	686	0.0382779903709888	1
368	687	0.0397608801722526967	1
369	688	0.0413702543824911034	1
370	689	0.0432453956454992988	1
371	690	0.0454328183084726014	1
372	691	0.0481501739472151025	1
373	692	0.0514580737799405982	1
374	693	0.0554570756852627023	1
375	694	0.0602325603365898021	1
376	695	0.0657927867025137059	1
377	696	0.0720739129930735023	1
378	697	0.0789198428392410001	1
379	698	0.0864772431552409959	1
380	699	0.0948167346417903983	1
381	700	0.103880517929791999	1
382	701	0.113337228447198998	1
383	702	0.123228267580271	1
384	703	0.13327083289623301	1
385	704	0.143307886272668994	1
386	705	0.153365960717200989	1
387	706	0.163552139699459004	1
388	707	0.173773218691349013	1
389	708	0.183980397880077001	1
390	709	0.194267846643924991	1
391	710	0.204522493481636009	1
392	711	0.214720565080642994	1
393	712	0.225049860775471011	1
394	713	0.235409261286258997	1
395	714	0.245681914687156999	1
396	715	0.255629093945025976	1
397	716	0.265563273429871016	1
398	717	0.275745670497416995	1
399	718	0.286700567603111023	1
400	719	0.297814595699310014	1
401	720	0.308824211359023992	1
402	721	0.318443423509597978	1
403	722	0.327219590544701011	1
404	723	0.335912978649138994	1
405	724	0.345485147833824002	1
406	725	0.354921841621399003	1
407	726	0.363990837335587014	1
408	727	0.372546228766441001	1
409	728	0.380679288506508007	1
410	729	0.388553145527839983	1
411	730	0.396074572205543984	1
412	731	0.403293943405151012	1
413	732	0.410043144226074019	1
414	733	0.416385379433632019	1
415	734	0.422233369946479975	1
416	735	0.427814504504203996	1
417	736	0.433289620280266019	1
418	737	0.438588362932204989	1
419	738	0.443591049313544983	1
420	739	0.44826595485210402	1
421	740	0.452585342526436007	1
422	741	0.456542548537254012	1
423	742	0.460182237625122004	1
424	743	0.46352669298648802	1
425	744	0.466696944832801996	1
426	745	0.469714254140853993	1
427	746	0.472474437952042026	1
428	747	0.474910843372345015	1
429	748	0.477030429244040988	1
430	749	0.479122629761696006	1
431	750	0.481038147211075018	1
432	751	0.482665139436722013	1
433	752	0.484086185693741011	1
434	753	0.485344079136848017	1
435	754	0.486531141400336986	1
436	755	0.487689638137818016	1
437	756	0.488892760872841003	1
438	757	0.490130773186684021	1
439	758	0.49127895236015301	1
440	759	0.492466723918914995	1
441	760	0.494125121831893999	1
442	761	0.496021020412445002	1
443	762	0.496485489606857011	1
444	763	0.496181690692902022	1
445	764	0.49583751261234299	1
446	765	0.495672360062599016	1
447	766	0.495634755492209977	1
448	767	0.495690634846688016	1
449	768	0.495818176865577998	1
450	769	0.495992586016655024	1
451	770	0.496092984080314992	1
452	771	0.496171477437019004	1
453	772	0.496249657869339	1
454	773	0.496352598071097995	1
455	774	0.496497315168381026	1
456	775	0.496690741181374018	1
457	776	0.496825388073921015	1
458	777	0.496895977854728998	1
459	778	0.497044807672500977	1
460	779	0.497215971350670027	1
461	780	0.497380727529526001	1
462	781	0.497645276784897006	1
463	782	0.497944378852843983	1
464	783	0.498109066486358998	1
465	784	0.498178613185882024	1
466	785	0.498208639025688016	1
467	786	0.498328158259391973	1
468	787	0.498399254679679993	1
469	788	0.498361346125603011	1
470	789	0.498338162899017001	1
471	790	0.498364979028702026	1
472	791	0.498508617281913979	1
473	792	0.49865765869617501	1
474	793	0.498766162991523987	1
475	794	0.498901322484016974	1
476	795	0.499016743898392012	1
477	796	0.499035227298737005	1
478	797	0.499122098088264021	1
479	798	0.499261391162871992	1
480	799	0.499327567219733981	1
481	800	0.499374952912331027	1
482	801	0.499409225583076988	1
483	802	0.499397930502891996	1
484	803	0.499395284056664002	1
485	804	0.499506795406342019	1
486	805	0.499636822938918979	1
487	806	0.49973146915435801	1
488	807	0.499839252233505005	1
489	808	0.499898639321326999	1
490	809	0.499898710846901018	1
491	810	0.500029632449149997	1
492	811	0.500207489728928034	1
493	812	0.500215718150138988	1
494	813	0.500258424878121	1
495	814	0.500364938378333957	1
496	815	0.500430786609650013	1
497	816	0.500431337952613964	1
498	817	0.500387021899223017	1
499	818	0.500527471303940041	1
500	819	0.500732010602951028	1
501	820	0.500965946912765969	1
502	821	0.500989457964897023	1
503	822	0.500814318656921054	1
504	823	0.500790536403656006	1
505	824	0.500949722528458041	1
506	825	0.501214244961739053	1
507	826	0.501388898491859969	1
508	827	0.501390182971954035	1
509	828	0.501298931241035972	1
510	829	0.501318749785423012	1
511	830	0.501483026146888999	1
512	831	0.501616039872169961	1
513	832	0.501844060420989946	1
514	833	0.502144205570220992	1
515	834	0.50229108035564396	1
516	835	0.502381575107573974	1
517	836	0.502390879392623946	1
518	837	0.502468907833099054	1
519	838	0.502550712227820995	1
520	839	0.502642035484312966	1
521	840	0.502795735001564048	1
522	841	0.502977168560028054	1
523	842	0.503108325600624018	1
524	843	0.503193989396095054	1
525	844	0.503305152058602046	1
526	845	0.50344426929950703	1
527	846	0.503637969493864968	1
528	847	0.503811618685721996	1
529	848	0.503955990076064952	1
530	849	0.504047247767449025	1
531	850	0.504109373688698015	1
532	851	0.504230609536171048	1
533	852	0.504373583197593955	1
534	853	0.504327729344366982	1
535	854	0.504283034801482954	1
536	855	0.50451714396476699	1
537	856	0.504797875881194957	1
538	857	0.505054855346680043	1
539	858	0.505100053548813044	1
540	859	0.505095058679581044	1
541	860	0.505114564299584012	1
542	861	0.505083116888999983	1
543	862	0.505102264881133967	1
544	863	0.505340218544006015	1
545	864	0.505470207333564958	1
546	865	0.505484637618064947	1
547	866	0.505502197146415977	1
548	867	0.505532386898994046	1
549	868	0.505618885159492049	1
550	869	0.505809617042541015	1
551	870	0.505952280759811024	1
552	871	0.505958011746406977	1
553	872	0.505888941884040988	1
554	873	0.505820456147193953	1
555	874	0.505925983190537054	1
556	875	0.506010288000106945	1
557	876	0.505976882576941955	1
558	877	0.50591377913951896	1
559	878	0.505903166532515991	1
560	879	0.50589684545993796	1
561	880	0.506004396080971031	1
562	881	0.506146535277366971	1
563	882	0.506164157390594949	1
564	883	0.506119921803473982	1
565	884	0.506070867180823947	1
566	885	0.506076022982597018	1
567	886	0.506021705269814048	1
568	887	0.505909311771392978	1
569	888	0.505962961912154952	1
570	889	0.506104022264481035	1
571	890	0.506014594435691967	1
572	891	0.506047776341438049	1
573	892	0.506219151616095986	1
574	893	0.506116539239882979	1
575	894	0.505923125147820008	1
576	895	0.505701735615729953	1
577	896	0.505590429902077032	1
578	897	0.505413657426833973	1
579	898	0.505075052380562051	1
580	899	0.504718723893165966	1
581	900	0.504596593976020968	1
582	901	0.504687383770943021	1
583	902	0.504952538013457963	1
584	903	0.505193775892258001	1
585	904	0.505156046152114957	1
586	905	0.505174711346625949	1
587	906	0.50521686673164401	1
588	907	0.50516785085201299	1
589	908	0.505089405179024009	1
590	909	0.504687502980232017	1
591	910	0.504311916232109025	1
592	911	0.504093372821807995	1
593	912	0.503874808549879982	1
594	913	0.503944236040114957	1
595	914	0.504069492220879045	1
596	915	0.504196321964264005	1
597	916	0.504266178607940985	1
598	917	0.504004570841788957	1
599	918	0.503951355814933999	1
600	919	0.504099574685097029	1
601	920	0.504053986072540039	1
602	921	0.504247879981995051	1
603	922	0.50453034043312095	1
604	923	0.504527503252028997	1
605	924	0.504527258872985995	1
606	925	0.504465463757514998	1
607	926	0.504256632924079962	1
608	927	0.504085519909858948	1
609	928	0.503713116049766985	1
610	929	0.502892944216728033	1
611	930	0.501721832156181047	1
612	931	0.500562322139739946	1
613	932	0.499999946355820013	1
614	933	0.499837645888329019	1
615	934	0.498240679502487016	1
616	935	0.496741977334021978	1
617	936	0.496807160973548989	1
618	937	0.496991205215453979	1
619	938	0.497217872738837996	1
620	939	0.497767633199691972	1
621	940	0.498166391253472007	1
622	941	0.49790475368499798	1
623	942	0.497379162907599981	1
624	943	0.496567812561989008	1
625	944	0.495481583476067022	1
626	945	0.495014595985412975	1
627	946	0.494523411989212014	1
628	947	0.493440631031989985	1
629	948	0.49380422532558399	1
630	949	0.494218605756760021	1
631	950	0.492481812834740018	1
632	951	0.491239520907402027	1
633	952	0.490647777915001027	1
634	953	0.490273317694663979	1
635	954	0.49023316204547901	1
636	955	0.489928281307219993	1
637	956	0.489141950011252991	1
638	957	0.488724592328072027	1
639	958	0.489281013607978987	1
640	959	0.488618332147597978	1
641	960	0.487608119845389987	1
642	961	0.488065683841705011	1
643	962	0.487962386012076976	1
644	963	0.486880668997764998	1
645	964	0.486690568923950018	1
646	965	0.486553442478179998	1
647	966	0.48618728816509299	1
648	967	0.486754518747330001	1
649	968	0.487073609232902993	1
650	969	0.48617736995220201	1
651	970	0.486541095376014976	1
652	971	0.487298327684402011	1
653	972	0.486272931098937988	1
654	973	0.485982942581177024	1
655	974	0.486565524339675981	1
656	975	0.486140191555023027	1
657	976	0.485949799418449013	1
658	977	0.486285328865050992	1
659	978	0.486336848139763023	1
660	979	0.486627623438834978	1
661	980	0.487067395448684992	1
662	981	0.486883676052092984	1
663	982	0.486283728480339017	1
664	983	0.486239856481551991	1
665	984	0.486427000164986023	1
666	985	0.486836567521094998	1
667	986	0.487168681621552024	1
668	987	0.486798352003098012	1
669	988	0.486592817306517988	1
670	989	0.487187924981117015	1
671	990	0.487502413988114003	1
672	991	0.487173393368721008	1
673	992	0.487597566843033015	1
674	993	0.488551878929137984	1
675	994	0.488734742999076988	1
676	995	0.488404181599617015	1
677	996	0.487777137756348012	1
678	997	0.488005384802818021	1
679	998	0.489402160048485024	1
680	999	0.490975919365882973	1
681	1000	0.490126678347587996	1
682	1001	0.488926798105239979	1
683	1002	0.488975784182548001	1
684	1003	0.488874986767769026	1
685	1004	0.489220196008681996	1
686	1005	0.489542415738105974	1
687	1006	0.490331357717514016	1
688	1007	0.491127723455428988	1
689	1008	0.491106075048446999	1
690	1009	0.490659415721892977	1
691	1010	0.489865699410439004	1
692	1011	0.490720829367638001	1
693	1012	0.492083773016930015	1
694	1013	0.492444598674774026	1
695	1014	0.491809210181236023	1
696	1015	0.491656017303466975	1
697	1016	0.491797828674316984	1
698	1017	0.491562205553055021	1
699	1018	0.491140025854110984	1
700	1019	0.491946452856063976	1
701	1020	0.492586284875869973	1
702	1021	0.492571198940277011	1
703	1022	0.492863187193870012	1
704	1023	0.493328461050986988	1
705	1024	0.493957358598708984	1
706	1025	0.493565258383750982	1
707	1026	0.492858839035034024	1
708	1027	0.492620500922202997	1
709	1028	0.493265134096145974	1
710	1029	0.493987566232680975	1
711	1030	0.493347084522247015	1
712	1031	0.494322973489761008	1
713	1032	0.49571938216686301	1
714	1033	0.494831010699271989	1
715	1034	0.493661919236183022	1
716	1035	0.493303623795509016	1
717	1036	0.494668534398079007	1
718	1037	0.495254072546959023	1
719	1038	0.494582447409629977	1
720	1039	0.494184395670891008	1
721	1040	0.493083459138869973	1
722	1041	0.491442805528641002	1
723	1042	0.491000986099242975	1
724	1043	0.491796046495438011	1
725	1044	0.492110323905945002	1
726	1045	0.492437687516212974	1
727	1046	0.492745938897132973	1
728	1047	0.492562705278397017	1
729	1048	0.492944023013115018	1
730	1049	0.49333252906799302	1
731	1050	0.488730394840240001	1
732	1051	0.487068498134613004	1
733	1052	0.490404057502747015	1
734	1053	0.490487909317017023	1
735	1054	0.488892868161201977	1
736	1055	0.486592486500739996	1
737	1056	0.486231610178947005	1
738	1057	0.487184399366378973	1
739	1058	0.488651064038277017	1
740	1059	0.484499502182006991	1
741	1060	0.480019518733025008	1
742	1061	0.484669551253319009	1
743	1062	0.48874841928482099	1
744	1063	0.486663326621055992	1
745	1064	0.483162003755569003	1
746	1065	0.481931737065315002	1
747	1066	0.483152610063552979	1
748	1067	0.481069436669349981	1
749	1068	0.478331914544106007	1
750	1069	0.476481896638869973	1
751	1070	0.478265053033828991	1
752	1071	0.479825481772423013	1
753	1072	0.474156874418258989	1
754	1073	0.477522310614585999	1
755	1074	0.482968035340309021	1
756	1075	0.478122559189797014	1
757	326	0.0451537360038075999	2
758	327	0.0476506182125637015	2
759	328	0.0473204989518438007	2
760	329	0.0471686220594815034	2
761	330	0.0479635761252470982	2
762	331	0.0466909584190164026	2
763	332	0.0445220379957131027	2
764	333	0.0457889208836214992	2
765	334	0.0462916802082743023	2
766	335	0.0452804219509874012	2
767	336	0.0456355315233979986	2
768	337	0.0459028195057595984	2
769	338	0.0454881255115782007	2
770	339	0.0453306574906622017	2
771	340	0.0454635024070740024	2
772	341	0.0455399607973440007	2
773	342	0.045240239373275197	2
774	343	0.0448498097913605995	2
775	344	0.0449438366506781026	2
776	345	0.045098549553326199	2
777	346	0.0455320126244000006	2
778	347	0.0455792189708777995	2
779	348	0.0448931684451443977	2
780	349	0.0449185488479477979	2
781	350	0.045000843171562499	2
782	351	0.0446768243397984988	2
783	352	0.0444646669285638022	2
784	353	0.0444868148437568001	2
785	354	0.0445250187601361966	2
786	355	0.0443449408880301973	2
787	356	0.0440599370215620026	2
788	357	0.0439085795411041993	2
789	358	0.0437108666769095988	2
790	359	0.0440664887428283969	2
791	360	0.0441159834819181002	2
792	361	0.0437338581042630015	2
793	362	0.0440712257155349979	2
794	363	0.0443115947502000021	2
795	364	0.0434870448495661005	2
796	365	0.0431728261922086978	2
797	366	0.0436203868261405003	2
798	367	0.0438970912780079978	2
799	368	0.0438282383339746034	2
800	369	0.0435624383389950007	2
801	370	0.0435548541801316011	2
802	371	0.0437364839017390997	2
803	372	0.0441313987331731009	2
804	373	0.0439777214612279976	2
805	374	0.0433277769812515992	2
806	375	0.0432613678276538988	2
807	376	0.043290695973804999	2
808	377	0.0431409547371523969	2
809	378	0.0427492287542138999	2
810	379	0.0425260721572807976	2
811	380	0.0429760984012058991	2
812	381	0.0432605221867560993	2
813	382	0.0432636567524500998	2
814	383	0.0428520882768290007	2
815	384	0.0426464847155980029	2
816	385	0.0431740778897490018	2
817	386	0.0432788601943425028	2
818	387	0.0429065546819141982	2
819	388	0.0429579546408994026	2
820	389	0.0430425187306745019	2
821	390	0.0425032914749213994	2
822	391	0.0424083077481815021	2
823	392	0.0424854127424103972	2
824	393	0.0424437618681363024	2
825	394	0.0425521268376281986	2
826	395	0.0426240552748953019	2
827	396	0.0424976375486168997	2
828	397	0.0424712566392762003	2
829	398	0.0427509732544422011	2
830	399	0.0426365892801965976	2
831	400	0.0422875556562627986	2
832	401	0.0423343623323099993	2
833	402	0.0424067404653344984	2
834	403	0.0423632407827036969	2
835	404	0.0424139190997395998	2
836	405	0.0424942682896341031	2
837	406	0.0425332263112067968	2
838	407	0.0425403187317507994	2
839	408	0.0426043841455664013	2
840	409	0.0426469392010144013	2
841	410	0.042695811816624199	2
842	411	0.0428412168153695025	2
843	412	0.0428894976420061969	2
844	413	0.0428244620561600009	2
845	414	0.0428724592285497016	2
846	415	0.0429942331143787979	2
847	416	0.0431214384734631001	2
848	417	0.0432649504925523981	2
849	418	0.0433529806988579985	2
850	419	0.0433445498347281993	2
851	420	0.0433969252875872971	2
852	421	0.0434941244976861027	2
853	422	0.0434563378138202033	2
854	423	0.0433793099863188994	2
855	424	0.043450509863240401	2
856	425	0.0435517765581608027	2
857	426	0.0436304930065359031	2
858	427	0.0437752599162715	2
859	428	0.0437866694160869985	2
860	429	0.0436676724680832018	2
861	430	0.0437818808215005018	2
862	431	0.0439222715795039992	2
863	432	0.0439097338489123981	2
864	433	0.0439467765390873025	2
865	434	0.0440186964614050977	2
866	435	0.0439579162214482969	2
867	436	0.0438838452100753992	2
868	437	0.0438661953168255972	2
869	438	0.0438530482351780007	2
870	439	0.043878088572195599	2
871	440	0.0440198023404394026	2
872	441	0.0440527935113226021	2
873	442	0.0439766405948570985	2
874	443	0.0440009901566165013	2
875	444	0.0440489170806748001	2
876	445	0.043998913041182898	2
877	446	0.044080701257501298	2
878	447	0.0442828429596765008	2
879	448	0.0442216486803123018	2
880	449	0.0441697873175143987	2
881	450	0.044265595397778898	2
882	451	0.0443209968507290025	2
883	452	0.0443828095282826976	2
884	453	0.0444984356207506968	2
885	454	0.0445250906050205023	2
886	455	0.0444683996694429026	2
887	456	0.0444748268595763982	2
888	457	0.0445417128503323018	2
889	458	0.0445241071283817985	2
890	459	0.0445673306073460992	2
891	460	0.0446838587522506991	2
892	461	0.0446717276104858993	2
893	462	0.0446151665278844006	2
894	463	0.0445986528481756017	2
895	464	0.0445775964430399976	2
896	465	0.044624145541872301	2
897	466	0.0447271753634725031	2
898	467	0.044712854815380898	2
899	468	0.0445889724152429001	2
900	469	0.044589966003383899	2
901	470	0.0446287186018057974	2
902	471	0.0446588732302189012	2
903	472	0.0446884759834833972	2
904	473	0.0447006113827227991	2
905	474	0.0446435644158294995	2
906	475	0.0446022871349538996	2
907	476	0.044583937951496698	2
908	477	0.0445320792496204029	2
909	478	0.0445765533617564977	2
910	479	0.0447683919753346996	2
911	480	0.0448165099535669034	2
912	481	0.0446863392634051018	2
913	482	0.0446261742285320034	2
914	483	0.0447039987359727983	2
915	484	0.0448306394474847006	2
916	485	0.0448763221502304008	2
917	486	0.044873002916574499	2
918	487	0.0449547607983862008	2
919	488	0.0450000624571528021	2
920	489	0.0450443008116312993	2
921	490	0.0451166667044163028	2
922	491	0.0452245117298194013	2
923	492	0.0453187530594212976	2
924	493	0.0454249935490744008	2
925	494	0.0455818846821784973	2
926	495	0.0458409115672111997	2
927	496	0.0460808798670769015	2
928	497	0.0462860736463751032	2
929	498	0.0465490562575203992	2
930	499	0.0468763395079545009	2
931	500	0.0471412278711796015	2
932	501	0.0475319820855344966	2
933	502	0.0480497106909752031	2
934	503	0.0485116333833763014	2
935	504	0.0489874647131988	2
936	505	0.0496778935194015017	2
937	506	0.0504234385277542996	2
938	507	0.0512037756187574966	2
939	508	0.0521502984421593971	2
940	509	0.0532217004469463031	2
941	510	0.0542537117643015973	2
942	511	0.0554873490972178987	2
943	512	0.0569135754236152966	2
944	513	0.0583601375775677986	2
945	514	0.0599666636969361996	2
946	515	0.0618559393499579016	2
947	516	0.063776036990540394	2
948	517	0.0658250254179749972	2
949	518	0.068269219781671206	2
950	519	0.0707010435206549964	2
951	520	0.0729666501283646046	2
952	521	0.0752269157341547978	2
953	522	0.0774725164685931	2
954	523	0.0796506064278739018	2
955	524	0.0818836838006972989	2
956	525	0.0841202789119312039	2
957	526	0.0861687638929912042	2
958	527	0.0881835296750068942	2
959	528	0.0901312551328113942	2
960	529	0.0917533540299960981	2
961	530	0.0932044812611171059	2
962	531	0.0946554222277233015	2
963	532	0.0958847786699023041	2
964	533	0.0969097752656255945	2
965	534	0.0980145473565375014	2
966	535	0.0990139873964446943	2
967	536	0.0997904432671412045	2
968	537	0.100493704634052994	2
969	538	0.101153081016881002	2
970	539	0.101689529206071994	2
971	540	0.102191365190914998	2
972	541	0.102731003292968998	2
973	542	0.103253886103629997	2
974	543	0.103722923568316999	2
975	544	0.104140054966722004	2
976	545	0.104561714189393001	2
977	546	0.105025511767183	2
978	547	0.105514027178287007	2
979	548	0.105970038899353997	2
980	549	0.106384911707468999	2
981	550	0.106798128357956004	2
982	551	0.107110315135547002	2
983	552	0.107286355325154001	2
984	553	0.107356679226670998	2
985	554	0.107319773307868002	2
986	555	0.107090408248561003	2
987	556	0.106758103838988996	2
988	557	0.106356538832188	2
989	558	0.105808443256786996	2
990	559	0.105107278696128001	2
991	560	0.104318553847926002	2
992	561	0.103468727852617001	2
993	562	0.102511303765433007	2
994	563	0.101463783000196997	2
995	564	0.100348150091512003	2
996	565	0.0991056178297315959	2
997	566	0.0977971990193639007	2
998	567	0.0964298035417285038	2
999	568	0.0949603265949657965	2
1000	569	0.0934210430298532957	2
1001	570	0.0918590075203350037	2
1002	571	0.0901926266295569046	2
1003	572	0.0885049594300133941	2
1004	573	0.0870088370782989007	2
1005	574	0.0855311485273497069	2
1006	575	0.0839943279113087998	2
1007	576	0.0826471788542610941	2
1008	577	0.0814359603183610059	2
1009	578	0.080333210527896895	2
1010	579	0.0793668191347803031	2
1011	580	0.0785035895449775062	2
1012	581	0.0776106087224824048	2
1013	582	0.0767486702118601039	2
1014	583	0.0760090883289065017	2
1015	584	0.0753610442791665947	2
1016	585	0.0747614981872695011	2
1017	586	0.0742131184254374021	2
1018	587	0.0737153451357568934	2
1019	588	0.0732655503920146001	2
1020	589	0.0728354805282183937	2
1021	590	0.0724103333694594009	2
1022	591	0.0719334472502980993	2
1023	592	0.0714618686054434016	2
1024	593	0.0710285061172076931	2
1025	594	0.0707058752221721015	2
1026	595	0.0704041570425033986	2
1027	596	0.0701331208859170968	2
1028	597	0.0698252219174590044	2
1029	598	0.0694708834801402025	2
1030	599	0.0691792869142124067	2
1031	600	0.0689182494367872045	2
1032	601	0.0686706831412656066	2
1033	602	0.0684632845222949982	2
1034	603	0.068262015602418305	2
1035	604	0.0679275159324917999	2
1036	605	0.0675121221159186019	2
1037	606	0.0670334707413400982	2
1038	607	0.066531557589769405	2
1039	608	0.0660311542451381961	2
1040	609	0.0655179438846451995	2
1041	610	0.0650158535156930978	2
1042	611	0.0644490612404687024	2
1043	612	0.0637828744947910031	2
1044	613	0.0630970980439866935	2
1045	614	0.0624410845339297971	2
1046	615	0.0618282604430403018	2
1047	616	0.0612259326236588991	2
1048	617	0.0605872369238307992	2
1049	618	0.0601156114467552988	2
1050	619	0.0597570655601364978	2
1051	620	0.0592797787061759979	2
1052	621	0.0589506998658180029	2
1053	622	0.0588509419134684991	2
1054	623	0.0586689300835133015	2
1055	624	0.0584391424698489023	2
1056	625	0.0583132904555115975	2
1057	626	0.0581691073519843005	2
1058	627	0.0579996822135789014	2
1059	628	0.0578750430473259983	2
1060	629	0.0577448696962424984	2
1061	630	0.0576676178191388999	2
1062	631	0.0576070009597709973	2
1063	632	0.0575257555714676003	2
1064	633	0.0573721565306187023	2
1065	634	0.057199085929564103	2
1066	635	0.0569396795971053027	2
1067	636	0.0565388101552214001	2
1068	637	0.0561256350151130012	2
1069	638	0.0557694834257874972	2
1070	639	0.0552755354770591983	2
1071	640	0.0546440726944378008	2
1072	641	0.0541553699544497988	2
1073	642	0.0536407322755882016	2
1074	643	0.0530047597629682982	2
1075	644	0.0523948515100138013	2
1076	645	0.0518295892647335013	2
1077	646	0.0512911528348923007	2
1078	647	0.0507240460387297989	2
1079	648	0.0501435122319629986	2
1080	649	0.0496422041739736988	2
1081	650	0.0492201559245585979	2
1082	651	0.0489221694214002981	2
1083	652	0.0486479790083000005	2
1084	653	0.048379486692803203	2
1085	654	0.048137525894812197	2
1086	655	0.0479362750692026995	2
1087	656	0.0477942073983805005	2
1088	657	0.0476053830768380995	2
1089	658	0.047347983079297197	2
1090	659	0.0468963714582580002	2
1091	660	0.0465553786073411996	2
1092	661	0.0463298309062208022	2
1093	662	0.0460703873208591005	2
1094	663	0.0458288118243218023	2
1095	664	0.0457006666277135973	2
1096	665	0.0454675512654440991	2
1097	666	0.0451660874698842987	2
1098	667	0.0451262130268981973	2
1099	668	0.0451091048972947009	2
1100	669	0.0450219666319233991	2
1101	670	0.0450584973607743994	2
1102	671	0.0450969173439911969	2
1103	672	0.045086811163595697	2
1104	673	0.0451731910662991967	2
1105	674	0.0453494638204575001	2
1106	675	0.0455824897757598005	2
1107	676	0.0458710555519376006	2
1108	677	0.0460936629346438975	2
1109	678	0.0463736898132732986	2
1110	679	0.0466994270682334969	2
1111	680	0.047060159700257502	2
1112	681	0.0474365007664475993	2
1113	682	0.0478532064173902982	2
1114	683	0.0483523591288498034	2
1115	684	0.0489179364272525966	2
1116	685	0.0496035750423158978	2
1117	686	0.0506687084478991967	2
1118	687	0.0520995967090129991	2
1119	688	0.0535572229751518983	2
1120	689	0.0551206107650484001	2
1121	690	0.056883136076586599	2
1122	691	0.0591362544468471002	2
1123	692	0.0620535755796092006	2
1124	693	0.0659001059830189029	2
1125	694	0.0704674204545362032	2
1126	695	0.0757126414350100946	2
1127	696	0.0816997683473996011	2
1128	697	0.0882612913846970021	2
1129	698	0.0955140995127815029	2
1130	699	0.103503714714730993	2
1131	700	0.112221872167928005	2
1132	701	0.121387390153748997	2
1133	702	0.130840971001557005	2
1134	703	0.140265230621611003	2
1135	704	0.149554967880248996	2
1136	705	0.158905410340854009	2
1137	706	0.168538191488810996	2
1138	707	0.178038507699965987	2
1139	708	0.187449416943959002	2
1140	709	0.196979741965020994	2
1141	710	0.206409262759345014	2
1142	711	0.215717796768461006	2
1143	712	0.225063968982015	2
1144	713	0.234473251870700006	2
1145	714	0.243874253971235994	2
1146	715	0.253018564411572	2
1147	716	0.262260890432767013	2
1148	717	0.272056886128016984	2
1149	718	0.282534488609859025	2
1150	719	0.292867673294885	2
1151	720	0.302067820514952012	2
1152	721	0.309613598244530996	2
1153	722	0.31658405065536499	2
1154	723	0.323959525142397009	2
1155	724	0.332339188882282977	2
1156	725	0.340287911040441982	2
1157	726	0.347572539533887004	2
1158	727	0.354186811617442976	2
1159	728	0.360607828412736975	2
1160	729	0.366825022867747985	2
1161	730	0.37260391030992801	2
1162	731	0.377879389694758983	2
1163	732	0.382585291351590973	2
1164	733	0.386769703456333991	2
1165	734	0.390733999865395976	2
1166	735	0.394795145307268003	2
1167	736	0.398827356951577006	2
1168	737	0.402632164103643997	2
1169	738	0.406011074781418013	2
1170	739	0.409110179969242982	2
1171	740	0.412005641630718011	2
1172	741	0.414708946432385983	2
1173	742	0.417164270366940981	2
1174	743	0.419358202389307988	2
1175	744	0.421370944806507997	2
1176	745	0.423198972429548015	2
1177	746	0.424810418060848016	2
1178	747	0.426299738032477005	2
1179	748	0.42770137105669298	2
1180	749	0.428984424897603023	2
1181	750	0.430118922676358983	2
1182	751	0.431070519345147007	2
1183	752	0.431931572301047007	2
1184	753	0.432716774088996003	2
1185	754	0.433428091662270976	2
1186	755	0.434139311313628984	2
1187	756	0.434882462024688998	2
1188	757	0.435926565102167984	2
1189	758	0.437373548746109009	2
1190	759	0.439816117286682018	2
1191	760	0.443636953830718994	2
1192	761	0.447891925062452012	2
1193	762	0.448029718228748985	2
1194	763	0.446537017822266014	2
1195	764	0.444927283695766007	2
1196	765	0.442877071244375975	2
1197	766	0.441097991807120005	2
1198	767	0.440012961626053023	2
1199	768	0.439341889960425003	2
1200	769	0.438955941370554992	2
1201	770	0.438888890402657994	2
1202	771	0.438912842954908011	2
1203	772	0.438962715012686022	2
1204	773	0.439038710934774978	2
1205	774	0.439093219382423017	2
1206	775	0.439147987536022022	2
1207	776	0.439226248434611999	2
1208	777	0.439336236034120975	2
1209	778	0.439442902803421021	2
1210	779	0.439527149711336995	2
1211	780	0.439583688974380993	2
1212	781	0.439712154013770007	2
1213	782	0.439891185079302027	2
1214	783	0.439976700714656999	2
1215	784	0.440091465200696974	2
1216	785	0.440324630056109001	2
1217	786	0.440626008169991978	2
1218	787	0.440907589026860003	2
1219	788	0.441140426056725987	2
1220	789	0.441307165793009992	2
1221	790	0.441424914768763998	2
1222	791	0.441583373716898986	2
1223	792	0.441656185047966998	2
1224	793	0.441601565905979987	2
1225	794	0.44160902500152599	2
1226	795	0.441772456680025016	2
1227	796	0.442026023353848996	2
1228	797	0.442269240106854977	2
1229	798	0.442435383796692006	2
1230	799	0.442442612988607986	2
1231	800	0.442448892763682988	2
1232	801	0.442570120096206998	2
1233	802	0.442839367049081001	2
1234	803	0.442974669592721026	2
1235	804	0.44280480061258598	2
1236	805	0.442783568586621989	2
1237	806	0.442927892718996019	2
1238	807	0.443101261343274988	2
1239	808	0.443362453154155012	2
1240	809	0.44369571975299299	2
1241	810	0.444181025028228982	2
1242	811	0.444771289825439009	2
1243	812	0.445458637816564973	2
1244	813	0.446099238736288983	2
1245	814	0.446598333971841011	2
1246	815	0.446840780121940007	2
1247	816	0.446891175849096989	2
1248	817	0.446801811456681019	2
1249	818	0.44660305976867698	2
1250	819	0.446713609354837005	2
1251	820	0.447325118950434975	2
1252	821	0.447363683155604985	2
1253	822	0.446929501635688009	2
1254	823	0.446350702217647	2
1255	824	0.446469132389340995	2
1256	825	0.44719598548752898	2
1257	826	0.447283204112733979	2
1258	827	0.447002593960081018	2
1259	828	0.446626854794365979	2
1260	829	0.446597278118133989	2
1261	830	0.446777241570608996	2
1262	831	0.446830102375575022	2
1263	832	0.446659756558281995	2
1264	833	0.44632555331502699	2
1265	834	0.446270768131528994	2
1266	835	0.446322104760578986	2
1267	836	0.446363772664751024	2
1268	837	0.446494264262063012	2
1269	838	0.44657495192119101	2
1270	839	0.446378661053520975	2
1271	840	0.446295508316584988	2
1272	841	0.446369392531259013	2
1273	842	0.446194393294198022	2
1274	843	0.446167724473136018	2
1275	844	0.446447053125925974	2
1276	845	0.446623972484044007	2
1277	846	0.446812599897384977	2
1278	847	0.447101648364748028	2
1279	848	0.447169039930616019	2
1280	849	0.447068512439728005	2
1281	850	0.447427558047431007	2
1282	851	0.447851117168154012	2
1283	852	0.448026035513196974	2
1284	853	0.447626969644001005	2
1285	854	0.447092605488640993	2
1286	855	0.447084516286849976	2
1287	856	0.447723759072166994	2
1288	857	0.44863662549427602	2
1289	858	0.448691721473422012	2
1290	859	0.448551250355584019	2
1291	860	0.448528894356318986	2
1292	861	0.448660748345511007	2
1293	862	0.448850346463066985	2
1294	863	0.449211427143641984	2
1295	864	0.449220491307121994	2
1296	865	0.448782912322452987	2
1297	866	0.448125834975924009	2
1298	867	0.447859857763563018	2
1299	868	0.448178197656359023	2
1300	869	0.448580141578401981	2
1301	870	0.44880225402968299	2
1302	871	0.448788774865014006	2
1303	872	0.448906485523495991	2
1304	873	0.449103781155178017	2
1305	874	0.449121019669941002	2
1306	875	0.449162342718669005	2
1307	876	0.449275672435760998	2
1308	877	0.449504890612194008	2
1309	878	0.449732823031288975	2
1310	879	0.449971522603715979	2
1311	880	0.449969781296593985	2
1312	881	0.449983311550957998	2
1313	882	0.450050724404199021	2
1314	883	0.45014493380274101	2
1315	884	0.450303980282375005	2
1316	885	0.45024673002106802	2
1317	886	0.450241655111312977	2
1318	887	0.450579306909015986	2
1319	888	0.451087900570460998	2
1320	889	0.45160474096025699	2
1321	890	0.451997825077602	2
1322	891	0.452594667673111017	2
1323	892	0.453354873827525984	2
1324	893	0.453832047326224008	2
1325	894	0.454154257263455985	2
1326	895	0.454405465296337019	2
1327	896	0.454371226685387986	2
1328	897	0.454167366027831976	2
1329	898	0.453954168728419982	2
1330	899	0.453116008213587995	2
1331	900	0.452278609786715025	2
1332	901	0.452536855425153983	2
1333	902	0.453369941030229984	2
1334	903	0.454493701457977017	2
1335	904	0.455608091184070974	2
1336	905	0.456188538244791997	2
1337	906	0.455786977495465984	2
1338	907	0.455158701964787027	2
1339	908	0.454872948782785003	2
1340	909	0.455119350126811983	2
1341	910	0.455664481435502977	2
1342	911	0.456121895994459015	2
1343	912	0.456068098545074019	2
1344	913	0.455731600522994995	2
1345	914	0.455019231353486986	2
1346	915	0.454465559550694009	2
1347	916	0.454147313322339985	2
1348	917	0.453943086521966022	2
1349	918	0.45402218188558302	2
1350	919	0.454354145697185019	2
1351	920	0.454788225037710991	2
1352	921	0.455261030367442976	2
1353	922	0.455744632652827997	2
1354	923	0.456195720604487986	2
1355	924	0.456838722739901004	2
1356	925	0.457977294921875	2
1357	926	0.459335152591977991	2
1358	927	0.460718963827406014	2
1359	928	0.461535926376069994	2
1360	929	0.461351603269577026	2
1361	930	0.460260604109082982	2
1362	931	0.458750516176223977	2
1363	932	0.457265351499829997	2
1364	933	0.455594846180506996	2
1365	934	0.453592581408364004	2
1366	935	0.452887552125113002	2
1367	936	0.454284965991973988	2
1368	937	0.455560309546333975	2
1369	938	0.456718351159776992	2
1370	939	0.458396106958389005	2
1371	940	0.459214857646397023	2
1372	941	0.459019282034465015	2
1373	942	0.458973156554357997	2
1374	943	0.45800137519836398	2
1375	944	0.455984532833098977	2
1376	945	0.454744449683598007	2
1377	946	0.453908967120306983	2
1378	947	0.452699175902774975	2
1379	948	0.452297985553741011	2
1380	949	0.45236319303512601	2
1381	950	0.451627561024256996	2
1382	951	0.451187091214315994	2
1383	952	0.451105982065201028	2
1384	953	0.449218643563134012	2
1385	954	0.44807979038783502	2
1386	955	0.449602718864168027	2
1387	956	0.44837392653737701	2
1388	957	0.446840929133551001	2
1389	958	0.447313768523080024	2
1390	959	0.446199417114257979	2
1391	960	0.443981438875197976	2
1392	961	0.442763430731636987	2
1393	962	0.442104007516589015	2
1394	963	0.441614317042486992	2
1395	964	0.44221284559794799	2
1396	965	0.442135431936808987	2
1397	966	0.440849397863660009	2
1398	967	0.441866040229797974	2
1399	968	0.443047919443675997	2
1400	969	0.442727842501231983	2
1401	970	0.442801513842173977	2
1402	971	0.442817147289004009	2
1403	972	0.441893918173653977	2
1404	973	0.440974205732346025	2
1405	974	0.440402865409851019	2
1406	975	0.440258571079798988	2
1407	976	0.44051817911011798	2
1408	977	0.440777978726796027	2
1409	978	0.439958112580435989	2
1410	979	0.439723627907888992	2
1411	980	0.441380092075893016	2
1412	981	0.441019207239151001	2
1413	982	0.43986045888492098	2
1414	983	0.440789158855166008	2
1415	984	0.440662699086325993	2
1416	985	0.439312526157923988	2
1417	986	0.439256216798510013	2
1418	987	0.439979978970118979	2
1419	988	0.440612976040158977	2
1420	989	0.441086688211985989	2
1421	990	0.441198523555483002	2
1422	991	0.440417413200650976	2
1423	992	0.441566228866576982	2
1424	993	0.443025895527430991	2
1425	994	0.441975491387504027	2
1426	995	0.441722916705267976	2
1427	996	0.442544949906213014	2
1428	997	0.441638967820575989	2
1429	998	0.441344976425171009	2
1430	999	0.442003756761551014	2
1431	1000	0.441967168024608004	2
1432	1001	0.442095973661968011	2
1433	1002	0.442288041114807018	2
1434	1003	0.442760561193739022	2
1435	1004	0.443361972059521992	2
1436	1005	0.443818019969123012	2
1437	1006	0.443125920636313009	2
1438	1007	0.441940924951008007	2
1439	1008	0.443807542324065996	2
1440	1009	0.444991690771920023	2
1441	1010	0.44422269293240102	2
1442	1011	0.445135423115322004	2
1443	1012	0.445604962962015017	2
1444	1013	0.444839247635432977	2
1445	1014	0.44555143373353101	2
1446	1015	0.446466177701950018	2
1447	1016	0.444963719163621985	2
1448	1017	0.446201788527624987	2
1449	1018	0.448824086359568997	2
1450	1019	0.446995850120271987	2
1451	1020	0.446074685880116006	2
1452	1021	0.446983358689717003	2
1453	1022	0.446866427149092027	2
1454	1023	0.446223782641546995	2
1455	1024	0.446909027440206996	2
1456	1025	0.446662634611129983	2
1457	1026	0.44671719840594698	2
1458	1027	0.448019576924187979	2
1459	1028	0.447338602372577998	2
1460	1029	0.446543140070778999	2
1461	1030	0.447700321674346979	2
1462	1031	0.447355772767748017	2
1463	1032	0.446368536778859015	2
1464	1033	0.449316642114094	2
1465	1034	0.449787242071969018	2
1466	1035	0.447168133088520992	2
1467	1036	0.44770858969007199	2
1468	1037	0.447907009295055003	2
1469	1038	0.445876858064105985	2
1470	1039	0.449018291064671005	2
1471	1040	0.452494595732008009	2
1472	1041	0.450129147086824999	2
1473	1042	0.44961482712200701	2
1474	1043	0.450244899306977975	2
1475	1044	0.448880842753819009	2
1476	1045	0.449336890663419974	2
1477	1046	0.450794756412505992	2
1478	1047	0.450725781066077025	2
1479	1048	0.448940055710928987	2
1480	1049	0.446948064225061004	2
1481	1050	0.444968926055090974	2
1482	1051	0.445914038590021988	2
1483	1052	0.448852258069175003	2
1484	1053	0.446648831878389996	2
1485	1054	0.444575403417860004	2
1486	1055	0.447476795741490019	2
1487	1056	0.44650396278926302	2
1488	1057	0.444154377494540009	2
1489	1058	0.446214973926544023	2
1490	1059	0.446422670568739022	2
1491	1060	0.442512989044189009	2
1492	1061	0.444933529411043027	2
1493	1062	0.444069789988653985	2
1494	1063	0.437608650752476014	2
1495	1064	0.438234273876463021	2
1496	1065	0.439658692904881021	2
1497	1066	0.439220058066503993	2
1498	1067	0.435268346752438973	2
1499	1068	0.436347254684993024	2
1500	1069	0.450221355472291973	2
1501	1070	0.449134992701667013	2
1502	1071	0.440317898988723977	2
1503	1072	0.436718689543860006	2
1504	1073	0.436988894428526009	2
1505	1074	0.439092751060213005	2
1506	1075	0.435035961014884009	2
1507	326	0.0432671766728163001	5
1508	327	0.0442366804927587995	5
1509	328	0.0436284136027097966	5
1510	329	0.0431137036532164009	5
1511	330	0.044684192910790399	5
1512	331	0.0449744578450917976	5
1513	332	0.0442418675869703029	5
1514	333	0.0436034254729747994	5
1515	334	0.0440718617290258019	5
1516	335	0.0449900221079587992	5
1517	336	0.0437610011547803976	5
1518	337	0.0427840653806925014	5
1519	338	0.0431517347693443007	5
1520	339	0.0434155445545911997	5
1521	340	0.0430765327066182993	5
1522	341	0.0427603591233491967	5
1523	342	0.0424480583518742974	5
1524	343	0.0423788674175738997	5
1525	344	0.0421473689377308003	5
1526	345	0.0419881220906972996	5
1527	346	0.0423213131725787978	5
1528	347	0.0422862265259026995	5
1529	348	0.0421140115708113008	5
1530	349	0.0423316370695829003	5
1531	350	0.0426456373184918996	5
1532	351	0.0427082233130932007	5
1533	352	0.0425670366734266017	5
1534	353	0.0424605432897805987	5
1535	354	0.0422970186918973992	5
1536	355	0.0423647414892912022	5
1537	356	0.042805519327521302	5
1538	357	0.0425415892153978015	5
1539	358	0.0419103570282459009	5
1540	359	0.0424176760017871968	5
1541	360	0.0426786884665490002	5
1542	361	0.0423951849341393031	5
1543	362	0.0419385526329279015	5
1544	363	0.0421378489583729976	5
1545	364	0.0426380936056375975	5
1546	365	0.0426702946424483975	5
1547	366	0.0423742204904555969	5
1548	367	0.0414346102625132023	5
1549	368	0.0411917757242918001	5
1550	369	0.0422334544360637998	5
1551	370	0.0420201014727353966	5
1552	371	0.0413455337285996011	5
1553	372	0.0416231874376535999	5
1554	373	0.0417874798178672971	5
1555	374	0.0418352421373128988	5
1556	375	0.0415404643863439005	5
1557	376	0.0412577964365481997	5
1558	377	0.0413307353854178994	5
1559	378	0.0415051233023404978	5
1560	379	0.0415867049247026013	5
1561	380	0.0406528361141682004	5
1562	381	0.040333577990531902	5
1563	382	0.0408761739730834975	5
1564	383	0.0412610944360495002	5
1565	384	0.0414682425558567033	5
1566	385	0.0412380907684564979	5
1567	386	0.0411211740225553013	5
1568	387	0.0412917055189609	5
1569	388	0.0415305808186531011	5
1570	389	0.0413032386451959999	5
1571	390	0.0408484440296887977	5
1572	391	0.0407483607530593969	5
1573	392	0.0410610899329185985	5
1574	393	0.040889494493603698	5
1575	394	0.0409911990165710977	5
1576	395	0.0414690833538770981	5
1577	396	0.0413415666669606982	5
1578	397	0.0411056756973266976	5
1579	398	0.0410603068768978022	5
1580	399	0.0409269332885741979	5
1581	400	0.0408039573580026987	5
1582	401	0.0406932372599839998	5
1583	402	0.0406518809497355971	5
1584	403	0.0406993038952351005	5
1585	404	0.0407613255083561013	5
1586	405	0.0408107016235590009	5
1587	406	0.040734729915857297	5
1588	407	0.0406603332608937995	5
1589	408	0.0406699813902378013	5
1590	409	0.0409215927124024006	5
1591	410	0.0410905987024307029	5
1592	411	0.0411206107586621974	5
1593	412	0.0410421367734671028	5
1594	413	0.0409372877329588006	5
1595	414	0.0411610011011361993	5
1596	415	0.0413283985108136992	5
1597	416	0.0412750057876110008	5
1598	417	0.0414729945361613991	5
1599	418	0.0417188696563244033	5
1600	419	0.0416815102100372009	5
1601	420	0.0417605414986610968	5
1602	421	0.0420419786125421982	5
1603	422	0.0421599451452494001	5
1604	423	0.0423201829195022985	5
1605	424	0.0425768584012984966	5
1606	425	0.0427419643849134001	5
1607	426	0.0428714793175459033	5
1608	427	0.043081451207399403	5
1609	428	0.043261265382170698	5
1610	429	0.0434033621102572001	5
1611	430	0.043531876429915399	5
1612	431	0.0436584249138831995	5
1613	432	0.0439963418990374014	5
1614	433	0.0443220868706703033	5
1615	434	0.044522275403141999	5
1616	435	0.0445326019078493007	5
1617	436	0.0445197906345128971	5
1618	437	0.0446936611086130031	5
1619	438	0.0449101921170950019	5
1620	439	0.0450797423720359983	5
1621	440	0.0451403301209210989	5
1622	441	0.0451085057109594026	5
1623	442	0.0451154090464115018	5
1624	443	0.0454123392701148987	5
1625	444	0.0457094982266426017	5
1626	445	0.0457237813621758984	5
1627	446	0.0457496847957373012	5
1628	447	0.0458522561937571016	5
1629	448	0.0458063121885061014	5
1630	449	0.0458382815122604023	5
1631	450	0.0460724789649247998	5
1632	451	0.0461838666349650012	5
1633	452	0.0462015084922313968	5
1634	453	0.0463044803589583026	5
1635	454	0.046428665518760702	5
1636	455	0.046491582691669503	5
1637	456	0.0465402282774447992	5
1638	457	0.0465403936803341009	5
1639	458	0.046721939370036103	5
1640	459	0.0468591965734959023	5
1641	460	0.0468854315578937988	5
1642	461	0.0469375129789113971	5
1643	462	0.0469418648630380991	5
1644	463	0.0469543397426605003	5
1645	464	0.0470475412905215981	5
1646	465	0.0471775673329829989	5
1647	466	0.0472485270351171993	5
1648	467	0.047269394248723999	5
1649	468	0.0472957905381918009	5
1650	469	0.0473431844264268972	5
1651	470	0.0473565854132175015	5
1652	471	0.047207838296890299	5
1653	472	0.0472711328417063023	5
1654	473	0.0475088559091091031	5
1655	474	0.0474804725497961003	5
1656	475	0.0473784655332566002	5
1657	476	0.0473556168377399014	5
1658	477	0.0474778261035680993	5
1659	478	0.0476101044565439002	5
1660	479	0.0475734807550906996	5
1661	480	0.0475571062415838033	5
1662	481	0.0475793398916721025	5
1663	482	0.0476676851511002031	5
1664	483	0.0477011725306510995	5
1665	484	0.0476849671453236992	5
1666	485	0.0477551035583018979	5
1667	486	0.047883576899766897	5
1668	487	0.0478107411414384967	5
1669	488	0.0477489762008189988	5
1670	489	0.0478515621274709993	5
1671	490	0.047848702967166902	5
1672	491	0.047848929092288002	5
1673	492	0.0481012798845767975	5
1674	493	0.0482198104262352018	5
1675	494	0.0481717698276043008	5
1676	495	0.0483250092715025031	5
1677	496	0.0484488580375909972	5
1678	497	0.0483906224370002983	5
1679	498	0.0484799049794674003	5
1680	499	0.0486687313765286969	5
1681	500	0.0487965427339076968	5
1682	501	0.0489213343709707024	5
1683	502	0.0491084992885588989	5
1684	503	0.04916359372437	5
1685	504	0.0492329794913530031	5
1686	505	0.0495261494070291033	5
1687	506	0.0497764855623245017	5
1688	507	0.0499453030526638003	5
1689	508	0.0502533555030822976	5
1690	509	0.050586820393800698	5
1691	510	0.050952557101845701	5
1692	511	0.051386205106973698	5
1693	512	0.0518879760056734016	5
1694	513	0.0524075031280517994	5
1695	514	0.0530509904026984988	5
1696	515	0.0538030158728360977	5
1697	516	0.0545437600463629033	5
1698	517	0.0553774964064360012	5
1699	518	0.0564331572502852	5
1700	519	0.0575436316430568973	5
1701	520	0.058700557053089103	5
1702	521	0.059985799714922898	5
1703	522	0.0612672641873360027	5
1704	523	0.0624933149665594032	5
1705	524	0.0638901647180318971	5
1706	525	0.0653854969888925941	5
1707	526	0.0667503885924815993	5
1708	527	0.0681425068527460043	5
1709	528	0.0696215271949768011	5
1710	529	0.0709177814424037933	5
1711	530	0.0720779664814471949	5
1712	531	0.0732895635068416956	5
1713	532	0.0743517663329840012	5
1714	533	0.0753082659095525991	5
1715	534	0.0762549169361591006	5
1716	535	0.0771245226264000022	5
1717	536	0.0778466783463954981	5
1718	537	0.0784907035529613939	5
1719	538	0.0790918260812760038	5
1720	539	0.0796357743442059063	5
1721	540	0.0801344051957129933	5
1722	541	0.0805621884763240981	5
1723	542	0.080952993780374502	5
1724	543	0.0813154309988022017	5
1725	544	0.0816939145326615046	5
1726	545	0.0820237241685391055	5
1727	546	0.0823164470493794043	5
1728	547	0.082709811627864796	5
1729	548	0.0830765254795551022	5
1730	549	0.083303476125001899	5
1731	550	0.0835643917322159008	5
1732	551	0.0838622614741325045	5
1733	552	0.0839417472481727989	5
1734	553	0.0839325234293937933	5
1735	554	0.0839149326086044006	5
1736	555	0.0837443381547928051	5
1737	556	0.0834387384355067957	5
1738	557	0.082980637252330805	5
1739	558	0.0824013739824294988	5
1740	559	0.0817178919911385054	5
1741	560	0.0809939973056316015	5
1742	561	0.0802361957728863068	5
1743	562	0.0793919868767262032	5
1744	563	0.0785002574324607932	5
1745	564	0.0775391951203345947	5
1746	565	0.0765661284327506936	5
1747	566	0.0755358245223761021	5
1748	567	0.0744393378496169961	5
1749	568	0.0733363419771193986	5
1750	569	0.072223456576466602	5
1751	570	0.0710315145552157939	5
1752	571	0.0698282122612000067	5
1753	572	0.0686695303767920068	5
1754	573	0.0675829701125622018	5
1755	574	0.0664895202964543991	5
1756	575	0.065510988608002696	5
1757	576	0.0647556841373443937	5
1758	577	0.0641270797699689948	5
1759	578	0.0634790975600481033	5
1760	579	0.0629054535180330054	5
1761	580	0.062419443204999002	5
1762	581	0.0618297878652811009	5
1763	582	0.0612244132906199015	5
1764	583	0.0607847139239311024	5
1765	584	0.0604032248258591017	5
1766	585	0.0600336574018002014	5
1767	586	0.0597913153469563016	5
1768	587	0.0594787284731864985	5
1769	588	0.059052750840783097	5
1770	589	0.0585798986256123033	5
1771	590	0.0581221163272857971	5
1772	591	0.0579821158200502021	5
1773	592	0.0578878354281186988	5
1774	593	0.0577730417251587011	5
1775	594	0.0577629160135985018	5
1776	595	0.0577621258795261022	5
1777	596	0.0577259726822376001	5
1778	597	0.057610798999667201	5
1779	598	0.0574540857225656967	5
1780	599	0.0572707787156105028	5
1781	600	0.0570811729878187013	5
1782	601	0.0568785607814788971	5
1783	602	0.0567849066108465014	5
1784	603	0.0566949110478163001	5
1785	604	0.0564385499805211965	5
1786	605	0.0562131877988577	5
1787	606	0.0560208246111870006	5
1788	607	0.0557550825178622991	5
1789	608	0.0553649000823498022	5
1790	609	0.054923744872212403	5
1791	610	0.054558030143380197	5
1792	611	0.0542965173721313976	5
1793	612	0.0540684230625628967	5
1794	613	0.0538273211568593979	5
1795	614	0.0535836406052112996	5
1796	615	0.0534824192523956007	5
1797	616	0.0533648461103439012	5
1798	617	0.0531240921467543009	5
1799	618	0.0529318496584891968	5
1800	619	0.0527653586119413015	5
1801	620	0.0527074258774518981	5
1802	621	0.052617820724844902	5
1803	622	0.0524772703647614011	5
1804	623	0.0523390360176562985	5
1805	624	0.0522239845246076986	5
1806	625	0.0520314365625381969	5
1807	626	0.0518576163798570022	5
1808	627	0.0517578162252902985	5
1809	628	0.0517114263027906029	5
1810	629	0.051694231480360002	5
1811	630	0.0517333604395390015	5
1812	631	0.0518485359847545985	5
1813	632	0.051953439787030202	5
1814	633	0.0517844278365372987	5
1815	634	0.0516001641750336026	5
1816	635	0.0514372862875462009	5
1817	636	0.0513086896389723032	5
1818	637	0.0512424089014529988	5
1819	638	0.0512162208557129031	5
1820	639	0.0511302307248115984	5
1821	640	0.0509381290525197997	5
1822	641	0.0508742257952689986	5
1823	642	0.0508591193705797001	5
1824	643	0.0506859913468361012	5
1825	644	0.0505011904984713003	5
1826	645	0.0503173630684614001	5
1827	646	0.0501064855605364012	5
1828	647	0.0500606689602135987	5
1829	648	0.0501168552786111984	5
1830	649	0.0500259414315223985	5
1831	650	0.0499206095933914032	5
1832	651	0.0499307591468095974	5
1833	652	0.0498793676495552021	5
1834	653	0.0497790329158305997	5
1835	654	0.0497974991798401032	5
1836	655	0.0497688233852386988	5
1837	656	0.0496724024415015966	5
1838	657	0.049595320224761999	5
1839	658	0.0495150890201329977	5
1840	659	0.0494749672710895969	5
1841	660	0.0495101436972618034	5
1842	661	0.0495849292725325033	5
1843	662	0.049508959800004998	5
1844	663	0.0494477793574333024	5
1845	664	0.0495225779712199984	5
1846	665	0.0495889849960804013	5
1847	666	0.049661801382899301	5
1848	667	0.0497762400656939011	5
1849	668	0.0499345328658818977	5
1850	669	0.0501357667148112987	5
1851	670	0.0503892231732607013	5
1852	671	0.0506351772695780022	5
1853	672	0.0508075166493653987	5
1854	673	0.051029301807284301	5
1855	674	0.0513126287609338982	5
1856	675	0.0515805367380380991	5
1857	676	0.051908674463629699	5
1858	677	0.0523503456264734005	5
1859	678	0.052912133559584601	5
1860	679	0.0534535091370344009	5
1861	680	0.0538740947842598003	5
1862	681	0.0542789332568646005	5
1863	682	0.0547470878809691031	5
1864	683	0.0552124723792076014	5
1865	684	0.0557221516966820013	5
1866	685	0.0561784885823726973	5
1867	686	0.0568869389593601033	5
1868	687	0.0577890485525131004	5
1869	688	0.0582246731966733974	5
1870	689	0.058356188982725099	5
1871	690	0.0583677534013987004	5
1872	691	0.0585974827408790033	5
1873	692	0.0592634208500385021	5
1874	693	0.0605406641960144001	5
1875	694	0.0620544914156199029	5
1876	695	0.0639095023274422053	5
1877	696	0.0663847025483847025	5
1878	697	0.0695218522101641034	5
1879	698	0.0734145060181617959	5
1880	699	0.0781013302505016022	5
1881	700	0.0835336327552795022	5
1882	701	0.089582639187574406	5
1883	702	0.0962478972971440028	5
1884	703	0.103379047662019996	5
1885	704	0.110875859856606002	5
1886	705	0.118847893178463004	5
1887	706	0.127361632883548986	5
1888	707	0.136141031235456011	5
1889	708	0.145224010199308001	5
1890	709	0.154783844947814997	5
1891	710	0.164594112336636011	5
1892	711	0.174587722122668987	5
1893	712	0.184781467914580999	5
1894	713	0.195287258923054013	5
1895	714	0.206075446307659005	5
1896	715	0.217001003026962014	5
1897	716	0.228279173374175998	5
1898	717	0.2401461571455	5
1899	718	0.252866497635841014	5
1900	719	0.265472677350043984	5
1901	720	0.277521871030330991	5
1902	721	0.288032700121402974	5
1903	722	0.298101212084293021	5
1904	723	0.308458021283149975	5
1905	724	0.319978016614913985	5
1906	725	0.330942812561988986	5
1907	726	0.341293913125991999	5
1908	727	0.351088571548462014	5
1909	728	0.360596695542335977	5
1910	729	0.369866776466370006	5
1911	730	0.378597205877303999	5
1912	731	0.386720693111419989	5
1913	732	0.394070941209793024	5
1914	733	0.400792717933655007	5
1915	734	0.407180339097977018	5
1916	735	0.413650572299956998	5
1917	736	0.419833096861839006	5
1918	737	0.425599911808968001	5
1919	738	0.431031125783919999	5
1920	739	0.435938814282416987	5
1921	740	0.440348017215729026	5
1922	741	0.44456225037574798	5
1923	742	0.448358944058417974	5
1924	743	0.451660475134850026	5
1925	744	0.454846104979514998	5
1926	745	0.457861733436584983	5
1927	746	0.460533425211905989	5
1928	747	0.462952575087548002	5
1929	748	0.465135875344275973	5
1930	749	0.467089891433715987	5
1931	750	0.468899360299110002	5
1932	751	0.470487627387047025	5
1933	752	0.471826440095902022	5
1934	753	0.472978833317756975	5
1935	754	0.474120455980301003	5
1936	755	0.475101575255393982	5
1937	756	0.475945109128951993	5
1938	757	0.476994732022285983	5
1939	758	0.478628388047218023	5
1940	759	0.481836667656898976	5
1941	760	0.485532614588737976	5
1942	761	0.488034915924071999	5
1943	762	0.48696340918540898	5
1944	763	0.485288012027740023	5
1945	764	0.484238910675048984	5
1946	765	0.482874017953873025	5
1947	766	0.48170120418071799	5
1948	767	0.481026834249495994	5
1949	768	0.480715671181679016	5
1950	769	0.480696570873261009	5
1951	770	0.480893814563751021	5
1952	771	0.481090596318245023	5
1953	772	0.481110304594039973	5
1954	773	0.480980280041695019	5
1955	774	0.480818152427673007	5
1956	775	0.480760294198990024	5
1957	776	0.480753344297409013	5
1958	777	0.480813744664192022	5
1959	778	0.480865034461022023	5
1960	779	0.480920588970184026	5
1961	780	0.481055545806884977	5
1962	781	0.481016555428505022	5
1963	782	0.480983343720435996	5
1964	783	0.481274053454398998	5
1965	784	0.481436455249786011	5
1966	785	0.481419634819030995	5
1967	786	0.481519761681557024	5
1968	787	0.481591874361038008	5
1969	788	0.481535258889198015	5
1970	789	0.481614145636559021	5
1971	790	0.481769776344299017	5
1972	791	0.481710514426230973	5
1973	792	0.48172987997531902	5
1974	793	0.481855875253677013	5
1975	794	0.482102641463280024	5
1976	795	0.482333076000214023	5
1977	796	0.482413849234581027	5
1978	797	0.482401829957961992	5
1979	798	0.482372927665709994	5
1980	799	0.482441309094429005	5
1981	800	0.482536229491233981	5
1982	801	0.482588070631026989	5
1983	802	0.482509696483612027	5
1984	803	0.482465285062790017	5
1985	804	0.482451081275939997	5
1986	805	0.482456916570662975	5
1987	806	0.482504054903984014	5
1988	807	0.482610765099524996	5
1989	808	0.482755324244499018	5
1990	809	0.482934105396271018	5
1991	810	0.483237802982329989	5
1992	811	0.483661398291587996	5
1993	812	0.484081330895423989	5
1994	813	0.484419730305671981	5
1995	814	0.484660339355468983	5
1996	815	0.484830516576767012	5
1997	816	0.484927153587341009	5
1998	817	0.484905678033829002	5
1999	818	0.484807822108269004	5
2000	819	0.484719258546829013	5
2001	820	0.484722074866294983	5
2002	821	0.484904950857163008	5
2003	822	0.485053101181984025	5
2004	823	0.484829872846603005	5
2005	824	0.484725686907768016	5
2006	825	0.484819042682648027	5
2007	826	0.48471794724464401	5
2008	827	0.484615969657897994	5
2009	828	0.484643980860709978	5
2010	829	0.484660890698432989	5
2011	830	0.484745573997498014	5
2012	831	0.484915453195571999	5
2013	832	0.485021752119064009	5
2014	833	0.484991848468780018	5
2015	834	0.484823915362358027	5
2016	835	0.484727501869201993	5
2017	836	0.484800931811332991	5
2018	837	0.484765547513962003	5
2019	838	0.484751912951470021	5
2020	839	0.485000857710838007	5
2021	840	0.485104748606682012	5
2022	841	0.485058766603470026	5
2023	842	0.485165914893150974	5
2024	843	0.485249969363213018	5
2025	844	0.48523826897144301	5
2026	845	0.48518137633800501	5
2027	846	0.485230994224548018	5
2028	847	0.485539913177490012	5
2029	848	0.485861402750014992	5
2030	849	0.48605686724185998	5
2031	850	0.486021539568900984	5
2032	851	0.486022883653640991	5
2033	852	0.486065056920051974	5
2034	853	0.485992676019669001	5
2035	854	0.48597506582736999	5
2036	855	0.48624234795570398	5
2037	856	0.48651626110076901	5
2038	857	0.486701202392578025	5
2039	858	0.486704972386360013	5
2040	859	0.486730226874351024	5
2041	860	0.486877638101578025	5
2042	861	0.486972397565841986	5
2043	862	0.487021318078040999	5
2044	863	0.487107816338539001	5
2045	864	0.486957159638405024	5
2046	865	0.486813130974769026	5
2047	866	0.487087118625641025	5
2048	867	0.487292519211769026	5
2049	868	0.487185460329056019	5
2050	869	0.487216222286224021	5
2051	870	0.487468609213829007	5
2052	871	0.487552377581595975	5
2053	872	0.487661701440811002	5
2054	873	0.487787136435508972	5
2055	874	0.487866541743277982	5
2056	875	0.487858375906943975	5
2057	876	0.487858527898788974	5
2058	877	0.487927874922752025	5
2059	878	0.487972536683082991	5
2060	879	0.487954282760619973	5
2061	880	0.488083416223526023	5
2062	881	0.488103082776070007	5
2063	882	0.488166692852973982	5
2064	883	0.488200291991233992	5
2065	884	0.48815336227417	5
2066	885	0.488179305195808988	5
2067	886	0.488246196508408004	5
2068	887	0.488358575105666992	5
2069	888	0.48851044178009001	5
2070	889	0.488755360245704984	5
2071	890	0.488953799009323009	5
2072	891	0.489079007506371011	5
2073	892	0.489149826765060025	5
2074	893	0.489247208833693992	5
2075	894	0.48938990831375101	5
2076	895	0.489544564485550004	5
2077	896	0.489712423086166027	5
2078	897	0.489511257410049028	5
2079	898	0.489098516106604986	5
2080	899	0.488870176672935974	5
2081	900	0.489101710915565979	5
2082	901	0.489227038621901977	5
2083	902	0.489419889450072976	5
2084	903	0.489767548441887024	5
2085	904	0.489685922861099021	5
2086	905	0.489766076207161005	5
2087	906	0.490087425708771007	5
2088	907	0.489712879061699025	5
2089	908	0.489258179068565002	5
2090	909	0.489854145050048984	5
2091	910	0.489912354946136996	5
2092	911	0.489358776807785001	5
2093	912	0.489151880145072993	5
2094	913	0.489241546392440996	5
2095	914	0.489294520020485013	5
2096	915	0.489220643043517978	5
2097	916	0.48925738632679	5
2098	917	0.489656159281731007	5
2099	918	0.489701938629151012	5
2100	919	0.489402723312377996	5
2101	920	0.48924903571605699	5
2102	921	0.489161431789398027	5
2103	922	0.489108666777611001	5
2104	923	0.489414045214652982	5
2105	924	0.489535602927207991	5
2106	925	0.489280337095260975	5
2107	926	0.489181831479073015	5
2108	927	0.489205825328827004	5
2109	928	0.489168941974640004	5
2110	929	0.489400514960289024	5
2111	930	0.490240418910980025	5
2112	931	0.490152257680892989	5
2113	932	0.489021447300911016	5
2114	933	0.487925067543983015	5
2115	934	0.487846001982689015	5
2116	935	0.488418439030647022	5
2117	936	0.488277989625930997	5
2118	937	0.488361868262290999	5
2119	938	0.487148481607437012	5
2120	939	0.485036185383797003	5
2121	940	0.484787520766257973	5
2122	941	0.48586618304252599	5
2123	942	0.485527053475379999	5
2124	943	0.485348656773566978	5
2125	944	0.485653913021088013	5
2126	945	0.484695020318031022	5
2127	946	0.483775761723517994	5
2128	947	0.485022926330565984	5
2129	948	0.483917137980460987	5
2130	949	0.482020536065101979	5
2131	950	0.48234804272651699	5
2132	951	0.481678214669228	5
2133	952	0.480413457751273976	5
2134	953	0.479368311166762984	5
2135	954	0.478641551733016979	5
2136	955	0.478712490200996987	5
2137	956	0.478205314278603022	5
2138	957	0.477313676476478987	5
2139	958	0.476263490319252025	5
2140	959	0.475278842449187999	5
2141	960	0.475299325585364973	5
2142	961	0.47537582218647001	5
2143	962	0.475585350394248985	5
2144	963	0.475492537021637018	5
2145	964	0.474791473150252996	5
2146	965	0.473892396688461004	5
2147	966	0.473020410537719982	5
2148	967	0.473729801177979026	5
2149	968	0.474290242791175987	5
2150	969	0.473124828934670005	5
2151	970	0.472720956802367975	5
2152	971	0.473227018117905018	5
2153	972	0.473228532075882002	5
2154	973	0.472867035865783991	5
2155	974	0.472504445910453985	5
2156	975	0.473084232211113009	5
2157	976	0.472919487953186002	5
2158	977	0.472011795639992016	5
2159	978	0.471952885389328003	5
2160	979	0.472409304976462996	5
2161	980	0.472223260998726024	5
2162	981	0.472220796346664973	5
2163	982	0.472411891818046992	5
2164	983	0.472354340553284013	5
2165	984	0.472409766912460005	5
2166	985	0.472763893008232017	5
2167	986	0.473378416895866017	5
2168	987	0.473330542445183022	5
2169	988	0.473482739925384977	5
2170	989	0.47324947416782398	5
2171	990	0.473112627863884028	5
2172	991	0.473670744895935025	5
2173	992	0.472777327895165023	5
2174	993	0.472300145030021989	5
2175	994	0.473480960726738009	5
2176	995	0.473803791403769992	5
2177	996	0.473391690850258007	5
2178	997	0.47410424053668998	5
2179	998	0.474611327052116005	5
2180	999	0.47487247884273498	5
2181	1000	0.474524602293967979	5
2182	1001	0.474908322095871027	5
2183	1002	0.474897146224975975	5
2184	1003	0.473469635844231018	5
2185	1004	0.472455507516860995	5
2186	1005	0.473739528656005993	5
2187	1006	0.475446745753287991	5
2188	1007	0.476679062843323009	5
2189	1008	0.476713067293166981	5
2190	1009	0.47575151920318598	5
2191	1010	0.474525725841521973	5
2192	1011	0.475469475984574008	5
2193	1012	0.476651754975318975	5
2194	1013	0.476782929897308028	5
2195	1014	0.477216607332228981	5
2196	1015	0.476885798573494002	5
2197	1016	0.476346987485885998	5
2198	1017	0.476549601554871027	5
2199	1018	0.477364954352379023	5
2200	1019	0.478552636504173012	5
2201	1020	0.478800320625304976	5
2202	1021	0.478291964530945024	5
2203	1022	0.476993578672408991	5
2204	1023	0.476112887263297979	5
2205	1024	0.476910966634750022	5
2206	1025	0.478263139724732	5
2207	1026	0.477995353937148981	5
2208	1027	0.476204845309257996	5
2209	1028	0.475970190763474021	5
2210	1029	0.47654685080051401	5
2211	1030	0.476876768469809975	5
2212	1031	0.477771598100661998	5
2213	1032	0.478834542632103011	5
2214	1033	0.478998726606368996	5
2215	1034	0.478725835680960998	5
2216	1035	0.477393510937691001	5
2217	1036	0.474148085713386014	5
2218	1037	0.472803595662116993	5
2219	1038	0.474417647719382995	5
2220	1039	0.475650075078010992	5
2221	1040	0.477047023177146978	5
2222	1041	0.478648027777672014	5
2223	1042	0.477857008576393016	5
2224	1043	0.474484595656395003	5
2225	1044	0.474157014489174022	5
2226	1045	0.475855040550231978	5
2227	1046	0.477015605568886003	5
2228	1047	0.473626095056534024	5
2229	1048	0.471881651878357011	5
2230	1049	0.474080160260200001	5
2231	1050	0.474883559346199025	5
2232	1051	0.473532998561858987	5
2233	1052	0.469628188014030001	5
2234	1053	0.469581186771392989	5
2235	1054	0.470988065004348977	5
2236	1055	0.473283594846724998	5
2237	1056	0.472013995051384028	5
2238	1057	0.470226761698723017	5
2239	1058	0.46834847033023802	5
2240	1059	0.46876533031463602	5
2241	1060	0.467721545696259011	5
2242	1061	0.464575996994971985	5
2243	1062	0.460387152433395008	5
2244	1063	0.456940674781799006	5
2245	1064	0.459414619207382025	5
2246	1065	0.465955635905266008	5
2247	1066	0.471215143799782021	5
2248	1067	0.464680993556976007	5
2249	1068	0.461482900381088024	5
2250	1069	0.46846289038658101	5
2251	1070	0.465190351009369008	5
2252	1071	0.459801626205444014	5
2253	1072	0.453168353438377025	5
2254	1073	0.454475748538970992	5
2255	1074	0.456478780508041027	5
2256	1075	0.455442103743552984	5
2257	326	0.068451992794871297	4
2258	327	0.068068271875381503	4
2259	328	0.0678235467523335966	4
2260	329	0.0674135252833367032	4
2261	330	0.0676817137748002978	4
2262	331	0.0679463170468806998	4
2263	332	0.0681434754282236044	4
2264	333	0.0673907060176134054	4
2265	334	0.0668651565909385986	4
2266	335	0.0665781982243061066	4
2267	336	0.0674125317484139952	4
2268	337	0.0678804095834493054	4
2269	338	0.0664323233067988933	4
2270	339	0.0657550498843192943	4
2271	340	0.0663365010172129038	4
2272	341	0.0659715492278337007	4
2273	342	0.0655176173895597042	4
2274	343	0.0656116750091313983	4
2275	344	0.0660067755728959982	4
2276	345	0.0661860499531030932	4
2277	346	0.065620997920632404	4
2278	347	0.0646905370056628931	4
2279	348	0.0644134785979986052	4
2280	349	0.0652466718107461985	4
2281	350	0.0658256854861974938	4
2282	351	0.0645389888435601994	4
2283	352	0.0645189289003610944	4
2284	353	0.0656101770699023951	4
2285	354	0.064961908385157599	4
2286	355	0.0642117843031883018	4
2287	356	0.0647029083222151052	4
2288	357	0.0647673353552817993	4
2289	358	0.0646692246198654064	4
2290	359	0.0645839244127272977	4
2291	360	0.0648726288229227038	4
2292	361	0.0652030702680349017	4
2293	362	0.064875448867678695	4
2294	363	0.0645594950765371045	4
2295	364	0.0642282329499720972	4
2296	365	0.063958605378866204	4
2297	366	0.0638268854469060953	4
2298	367	0.0631953276693821009	4
2299	368	0.0633510597050190027	4
2300	369	0.0647540852427482966	4
2301	370	0.0646750785410404039	4
2302	371	0.0634978208690882007	4
2303	372	0.0629791367799043961	4
2304	373	0.0630356967449187955	4
2305	374	0.0633612882345915007	4
2306	375	0.0635168198496103009	4
2307	376	0.0634630613029003032	4
2308	377	0.0628663398325443046	4
2309	378	0.0627431970089673968	4
2310	379	0.0632495645433664017	4
2311	380	0.0637428004294633976	4
2312	381	0.0638218451291323041	4
2313	382	0.0632149994373322061	4
2314	383	0.0631735086441039956	4
2315	384	0.0633157219737768007	4
2316	385	0.0632872074842452947	4
2317	386	0.0630900859832763949	4
2318	387	0.0626616064459084993	4
2319	388	0.0628725852817297037	4
2320	389	0.0629600364714860972	4
2321	390	0.0626513227820395951	4
2322	391	0.0622883286327124003	4
2323	392	0.0620714813470839982	4
2324	393	0.0624185018241405973	4
2325	394	0.062686032056808505	4
2326	395	0.0627075437456369039	4
2327	396	0.062593351677060094	4
2328	397	0.0624842189252377	4
2329	398	0.0624972581863402973	4
2330	399	0.0624627687036991008	4
2331	400	0.0623494833707810003	4
2332	401	0.0623341020196676032	4
2333	402	0.0623418886214495011	4
2334	403	0.0622539486736058967	4
2335	404	0.0623599823564291028	4
2336	405	0.0624878622591496027	4
2337	406	0.0624401375651360002	4
2338	407	0.0623926073312760024	4
2339	408	0.0624835539609193968	4
2340	409	0.0625816173851490021	4
2341	410	0.0626531522721052031	4
2342	411	0.0628121789544821008	4
2343	412	0.0627934910356997972	4
2344	413	0.0627290535718203063	4
2345	414	0.0629889879375696016	4
2346	415	0.0632930692285298968	4
2347	416	0.0633508760482072997	4
2348	417	0.063552965968847297	4
2349	418	0.0637912720441817932	4
2350	419	0.0638385135680436949	4
2351	420	0.0640268642455339015	4
2352	421	0.0643543887883425009	4
2353	422	0.0645628515630960048	4
2354	423	0.0647418934851884953	4
2355	424	0.064942595362663294	4
2356	425	0.0650902666151523951	4
2357	426	0.0652099888771772024	4
2358	427	0.0653513822704553937	4
2359	428	0.0655864477157593051	4
2360	429	0.0658915869891644052	4
2361	430	0.0661763828247785985	4
2362	431	0.0663897000253201031	4
2363	432	0.0666481174528598952	4
2364	433	0.0669601812958718068	4
2365	434	0.067260071262717297	4
2366	435	0.0673785556107758948	4
2367	436	0.0674384120851754948	4
2368	437	0.0676443170756101941	4
2369	438	0.0677825178951025065	4
2370	439	0.0678853377699852045	4
2371	440	0.0681270640343427936	4
2372	441	0.0683223973959684039	4
2373	442	0.0684828769415617017	4
2374	443	0.0687886413186789003	4
2375	444	0.0690064661204815022	4
2376	445	0.0690030857920646973	4
2377	446	0.0690997939556837054	4
2378	447	0.069304141774773606	4
2379	448	0.0693626377731561966	4
2380	449	0.0694571007043122968	4
2381	450	0.0696218799799681043	4
2382	451	0.0697756454348563981	4
2383	452	0.0698769457638264008	4
2384	453	0.0699816215783358037	4
2385	454	0.0700663421303033968	4
2386	455	0.0701634258031845065	4
2387	456	0.0702974457293748939	4
2388	457	0.0703882612287998061	4
2389	458	0.0703946128487586975	4
2390	459	0.0704356662929058047	4
2391	460	0.0705265887081622939	4
2392	461	0.0705506734549998932	4
2393	462	0.0705816641449927978	4
2394	463	0.070691917091608103	4
2395	464	0.0706253033131361008	4
2396	465	0.0704535115510225018	4
2397	466	0.0707305759191513006	4
2398	467	0.0708748113363980997	4
2399	468	0.0707391344010829953	4
2400	469	0.0707618668675422946	4
2401	470	0.0708944346755742971	4
2402	471	0.0708998352289200023	4
2403	472	0.0708849459886550931	4
2404	473	0.0709039259701966934	4
2405	474	0.0708699610084295023	4
2406	475	0.0708108149468898995	4
2407	476	0.0707864087074994985	4
2408	477	0.0708473481237888031	4
2409	478	0.0709277048707008945	4
2410	479	0.0709457274526358039	4
2411	480	0.0708952117711305063	4
2412	481	0.0708056040108203943	4
2413	482	0.0706993043422698975	4
2414	483	0.0706953544169663939	4
2415	484	0.0708357099443674004	4
2416	485	0.070896561816334705	4
2417	486	0.0708741795271635028	4
2418	487	0.0708967979997397052	4
2419	488	0.070877460390329397	4
2420	489	0.0708677135407925068	4
2421	490	0.0709586668759584066	4
2422	491	0.071060444787144697	4
2423	492	0.0710030462592839973	4
2424	493	0.0710135504603385953	4
2425	494	0.0711148008704186041	4
2426	495	0.0712245058268309028	4
2427	496	0.0713420476764441008	4
2428	497	0.0713928211480378966	4
2429	498	0.0715095899999142054	4
2430	499	0.0716653350740670964	4
2431	500	0.0717157024890184014	4
2432	501	0.0718414422124624003	4
2433	502	0.0721382249146699933	4
2434	503	0.0723896149545908002	4
2435	504	0.0726532388478517005	4
2436	505	0.0729715209454298019	4
2437	506	0.0733307667076587982	4
2438	507	0.0737614594399929047	4
2439	508	0.074303723126649901	4
2440	509	0.0749482635408639936	4
2441	510	0.0755136672407388937	4
2442	511	0.0761913809925317959	4
2443	512	0.0770143337547778972	4
2444	513	0.0778754081577062995	4
2445	514	0.078904279321432097	4
2446	515	0.0801435887813568004	4
2447	516	0.0815913263708353959	4
2448	517	0.0831672210246324067	4
2449	518	0.0846978642046452013	4
2450	519	0.0864055398851632933	4
2451	520	0.0882686559110879981	4
2452	521	0.0900844376534224028	4
2453	522	0.0918735358864068985	4
2454	523	0.0937507420778275063	4
2455	524	0.095592832565307706	4
2456	525	0.0974643625319003976	4
2457	526	0.0995195560157298986	4
2458	527	0.101504755020142001	4
2459	528	0.103283394128084002	4
2460	529	0.104862384498118993	4
2461	530	0.106285394728184004	4
2462	531	0.107697635143994996	4
2463	532	0.109063399583101003	4
2464	533	0.110336857289075996	4
2465	534	0.111397393792868005	4
2466	535	0.112254457920790002	4
2467	536	0.112963777035474999	4
2468	537	0.113616847991943007	4
2469	538	0.114249746501445995	4
2470	539	0.114868273586035005	4
2471	540	0.115358459204434993	4
2472	541	0.115755422413349005	4
2473	542	0.116108438372612005	4
2474	543	0.116466414183377998	4
2475	544	0.116852442175150006	4
2476	545	0.117212305963039998	4
2477	546	0.117557006329297997	4
2478	547	0.118054912984370999	4
2479	548	0.118498333543539003	4
2480	549	0.118704285472631996	4
2481	550	0.11896955743432	4
2482	551	0.119235776364802995	4
2483	552	0.119320231676102007	4
2484	553	0.119305628538131994	4
2485	554	0.119232358038424996	4
2486	555	0.118946706503629998	4
2487	556	0.118517106026410995	4
2488	557	0.118031876534224006	4
2489	558	0.117404319345950997	4
2490	559	0.116595274209975994	4
2491	560	0.115629310160875007	4
2492	561	0.114564368873835004	4
2493	562	0.113418626785278004	4
2494	563	0.112294231355189997	4
2495	564	0.111152870208025001	4
2496	565	0.109887441992760002	4
2497	566	0.108453792333603	4
2498	567	0.106896106153726994	4
2499	568	0.105300704389810998	4
2500	569	0.103675420582293995	4
2501	570	0.102162314206362001	4
2502	571	0.100659510493278995	4
2503	572	0.0991322278976441013	4
2504	573	0.0976991124451161014	4
2505	574	0.0963315889239310941	4
2506	575	0.0950257286429404963	4
2507	576	0.0938646897673607039	4
2508	577	0.0928009387105703937	4
2509	578	0.0917253695428370958	4
2510	579	0.0908506985753775032	4
2511	580	0.0901431746780873011	4
2512	581	0.0893876180052756986	4
2513	582	0.0886772308498620931	4
2514	583	0.088070694357156698	4
2515	584	0.0874643702059983985	4
2516	585	0.0868792563676833995	4
2517	586	0.0864857271313668047	4
2518	587	0.0860860068351031044	4
2519	588	0.0855834454298018965	4
2520	589	0.0852348428219557031	4
2521	590	0.0849157128483056939	4
2522	591	0.0843884892761706945	4
2523	592	0.0839518569409846982	4
2524	593	0.0836750987917184996	4
2525	594	0.0834816351532935985	4
2526	595	0.0832544375210999971	4
2527	596	0.0829709228128193976	4
2528	597	0.0826292663812636941	4
2529	598	0.0822980079799889991	4
2530	599	0.0821606088429689962	4
2531	600	0.0820346925407648003	4
2532	601	0.0818423461169004995	4
2533	602	0.0816828005015849956	4
2534	603	0.0814583111554384037	4
2535	604	0.0811600517481566031	4
2536	605	0.0808007281273604022	4
2537	606	0.080463226139545499	4
2538	607	0.0802330020815134048	4
2539	608	0.0799630761146545022	4
2540	609	0.0796225916594267003	4
2541	610	0.0792382847517728028	4
2542	611	0.0788273938000201985	4
2543	612	0.0783812139183282935	4
2544	613	0.0779988158494234057	4
2545	614	0.0777127694338559938	4
2546	615	0.0773253161460160932	4
2547	616	0.0769281975924969025	4
2548	617	0.0765784468501806009	4
2549	618	0.0762595292180777012	4
2550	619	0.0759924337267876004	4
2551	620	0.0758242361247539048	4
2552	621	0.0756383746862411971	4
2553	622	0.0753728654235601009	4
2554	623	0.0750487696379422975	4
2555	624	0.0747761208564042962	4
2556	625	0.0746878832578659058	4
2557	626	0.0746243774890899964	4
2558	627	0.0745879951864480972	4
2559	628	0.0745893813669682021	4
2560	629	0.0745098430663346939	4
2561	630	0.0743478603661059945	4
2562	631	0.0742808192968369002	4
2563	632	0.0742723323404789054	4
2564	633	0.0740968495607376015	4
2565	634	0.0739418815821409059	4
2566	635	0.0738296117633581023	4
2567	636	0.0735790405422449001	4
2568	637	0.0732760552316903957	4
2569	638	0.0730399858206509933	4
2570	639	0.072744416072964696	4
2571	640	0.0724056929349898987	4
2572	641	0.0720800418406725013	4
2573	642	0.0717454019933938952	4
2574	643	0.071466714516282101	4
2575	644	0.0712023127824067958	4
2576	645	0.0709431186318398022	4
2577	646	0.0706633295863867	4
2578	647	0.0704725362360477947	4
2579	648	0.0703501008450985038	4
2580	649	0.0701214902102947069	4
2581	650	0.0699326977133750971	4
2582	651	0.0698596533387899066	4
2583	652	0.0698230572044850062	4
2584	653	0.0698331624269486056	4
2585	654	0.0697557698935269976	4
2586	655	0.0697467844933270936	4
2587	656	0.069895524531602804	4
2588	657	0.0698189128190278979	4
2589	658	0.069671538844704603	4
2590	659	0.0695435289293527964	4
2591	660	0.0694801617413759037	4
2592	661	0.0694757167249917956	4
2593	662	0.0694794956594706054	4
2594	663	0.0695552859455347006	4
2595	664	0.0697164613753556983	4
2596	665	0.0697129026055336026	4
2597	666	0.0696713835000992016	4
2598	667	0.0697822533547877988	4
2599	668	0.0700114190578460943	4
2600	669	0.0703140646219252985	4
2601	670	0.0705326244235039007	4
2602	671	0.0707648247480393011	4
2603	672	0.0710556745529175027	4
2604	673	0.0714390337467193937	4
2605	674	0.0718727093189955035	4
2606	675	0.0722827207297087049	4
2607	676	0.0727202188223600027	4
2608	677	0.0732447218149901019	4
2609	678	0.0737422257661819042	4
2610	679	0.0742738030850886993	4
2611	680	0.0749062020331621031	4
2612	681	0.0755694959312677023	4
2613	682	0.076208554953336699	4
2614	683	0.0768606804311274955	4
2615	684	0.0774710975587367956	4
2616	685	0.0781214814633131027	4
2617	686	0.0791200183331965956	4
2618	687	0.0803340017795562966	4
2619	688	0.0809726573526858978	4
2620	689	0.0814358316361905032	4
2621	690	0.0819420989602804017	4
2622	691	0.0827619511634111044	4
2623	692	0.0841701205819845033	4
2624	693	0.0863446108996868966	4
2625	694	0.0893115807324647987	4
2626	695	0.0931174363940955041	4
2627	696	0.0977088116109370991	4
2628	697	0.103053187578917002	4
2629	698	0.109339108318091002	4
2630	699	0.11676200479269	4
2631	700	0.125289374589920011	4
2632	701	0.134521736949682003	4
2633	702	0.144520106911659008	4
2634	703	0.155043608695269008	4
2635	704	0.165864360332488997	4
2636	705	0.177133354544639993	4
2637	706	0.188992497324943998	4
2638	707	0.201142124831676011	4
2639	708	0.213595087826252	4
2640	709	0.226505398750305009	4
2641	710	0.239735314249992004	4
2642	711	0.253270378708839006	4
2643	712	0.267355798184871984	4
2644	713	0.281772489845752983	4
2645	714	0.296350523829459978	4
2646	715	0.311020126938819974	4
2647	716	0.326186767220496998	4
2648	717	0.342012721300125	4
2649	718	0.359097433090210028	4
2650	719	0.376408547163008977	4
2651	720	0.393138676881789995	4
2652	721	0.407736378908157004	4
2653	722	0.421478071808815025	4
2654	723	0.435709530115127974	4
2655	724	0.451649096608161993	4
2656	725	0.467089170217514005	4
2657	726	0.481647408008574973	4
2658	727	0.495291456580161993	4
2659	728	0.508664485812187039	4
2660	729	0.521709331870078952	4
2661	730	0.534022709727287048	4
2662	731	0.545661497116089023	4
2663	732	0.556401470303536039	4
2664	733	0.566394585371016945	4
2665	734	0.575824034214019953	4
2666	735	0.585199797153473034	4
2667	736	0.594159764051438	4
2668	737	0.602577072381973	4
2669	738	0.610479342937469971	4
2670	739	0.617767894268036022	4
2671	740	0.624481070041655983	4
2672	741	0.630720311403275002	4
2673	742	0.636338222026824996	4
2674	743	0.641353940963744984	4
2675	744	0.646100986003875999	4
2676	745	0.650507760047912997	4
2677	746	0.654287570714951028	4
2678	747	0.657642167806624989	4
2679	748	0.660683983564376964	4
2680	749	0.663503009080886996	4
2681	750	0.666121667623519986	4
2682	751	0.668430143594741955	4
2683	752	0.670299005508423029	4
2684	753	0.671858382225037021	4
2685	754	0.673437738418579013	4
2686	755	0.67481904625892597	4
2687	756	0.676025754213332997	4
2688	757	0.677612841129302979	4
2689	758	0.679776340723038053	4
2690	759	0.683358216285706033	4
2691	760	0.688899403810500965	4
2692	761	0.694730544090271018	4
2693	762	0.693775576353072965	4
2694	763	0.690900617837906017	4
2695	764	0.688741314411163041	4
2696	765	0.686437517404556052	4
2697	766	0.684538960456848034	4
2698	767	0.68326141238212601	4
2699	768	0.682492583990097046	4
2700	769	0.682137513160706033	4
2701	770	0.682097262144088945	4
2702	771	0.682105451822280995	4
2703	772	0.68204430341720601	4
2704	773	0.682060271501541027	4
2705	774	0.682120180130004972	4
2706	775	0.682067203521729004	4
2707	776	0.682028955221176014	4
2708	777	0.682059448957442971	4
2709	778	0.682070499658585017	4
2710	779	0.682131624221801958	4
2711	780	0.682293891906737948	4
2712	781	0.682279312610626021	4
2713	782	0.682163649797440041	4
2714	783	0.682260632514953946	4
2715	784	0.682349556684494041	4
2716	785	0.682404398918151966	4
2717	786	0.682566630840302002	4
2718	787	0.682705557346343972	4
2719	788	0.682835841178893954	4
2720	789	0.682916390895843994	4
2721	790	0.682937484979629028	4
2722	791	0.683073604106902987	4
2723	792	0.683192360401154053	4
2724	793	0.68328343033790595	4
2725	794	0.683355081081389981	4
2726	795	0.683425951004028032	4
2727	796	0.683503013849258045	4
2728	797	0.683664572238921964	4
2729	798	0.683854311704635953	4
2730	799	0.683957326412200972	4
2731	800	0.684060460329055986	4
2732	801	0.684134852886200018	4
2733	802	0.684054446220398016	4
2734	803	0.684016287326812966	4
2735	804	0.684102922677993996	4
2736	805	0.684306347370147949	4
2737	806	0.684544372558594016	4
2738	807	0.684717917442322022	4
2739	808	0.684808951616286965	4
2740	809	0.684842061996460028	4
2741	810	0.685008674860001054	4
2742	811	0.685287958383560047	4
2743	812	0.685513395071030041	4
2744	813	0.685469436645507968	4
2745	814	0.685258692502974998	4
2746	815	0.684929823875427002	4
2747	816	0.684793794155120983	4
2748	817	0.685012561082840032	4
2749	818	0.685270690917968994	4
2750	819	0.685593634843825961	4
2751	820	0.685998409986495972	4
2752	821	0.686011296510696034	4
2753	822	0.68576782941818204	4
2754	823	0.685703092813491955	4
2755	824	0.685994940996169977	4
2756	825	0.686481088399886974	4
2757	826	0.686572629213332952	4
2758	827	0.686486440896987959	4
2759	828	0.686448144912719993	4
2760	829	0.686467528343201017	4
2761	830	0.686513406038284035	4
2762	831	0.686598253250122026	4
2763	832	0.686505311727524048	4
2764	833	0.686367768049240046	4
2765	834	0.686773407459258967	4
2766	835	0.687162417173385998	4
2767	836	0.687273925542830955	4
2768	837	0.687530022859573986	4
2769	838	0.687778061628341986	4
2770	839	0.68791148066520702	4
2771	840	0.687920236587524991	4
2772	841	0.687904477119446023	4
2773	842	0.688161647319794012	4
2774	843	0.688402169942855968	4
2775	844	0.688512015342713002	4
2776	845	0.688719916343689009	4
2777	846	0.688971674442291016	4
2778	847	0.689294749498366999	4
2779	848	0.689334118366241033	4
2780	849	0.689185786247253018	4
2781	850	0.689541935920714999	4
2782	851	0.689928412437438965	4
2783	852	0.689970684051513961	4
2784	853	0.689653283357619995	4
2785	854	0.689433771371841031	4
2786	855	0.689735561609267966	4
2787	856	0.690239763259888006	4
2788	857	0.690719240903853948	4
2789	858	0.690885925292968994	4
2790	859	0.691076153516769986	4
2791	860	0.691252934932709007	4
2792	861	0.691508406400681008	4
2793	862	0.691849482059479048	4
2794	863	0.691999530792235973	4
2795	864	0.691847044229508046	4
2796	865	0.691524434089661022	4
2797	866	0.691354447603225952	4
2798	867	0.691428589820861972	4
2799	868	0.691770225763321034	4
2800	869	0.692041385173797985	4
2801	870	0.692090380191803023	4
2802	871	0.691920197010040039	4
2803	872	0.691776371002197021	4
2804	873	0.691801589727402022	4
2805	874	0.692181724309920976	4
2806	875	0.692329800128936967	4
2807	876	0.69218847155570995	4
2808	877	0.692175304889679044	4
2809	878	0.692148262262344049	4
2810	879	0.692089188098907959	4
2811	880	0.692278200387954956	4
2812	881	0.692550104856491044	4
2813	882	0.692240798473357954	4
2814	883	0.692199558019637951	4
2815	884	0.692457467317580955	4
2816	885	0.692264884710312001	4
2817	886	0.692249709367751986	4
2818	887	0.692421710491180953	4
2819	888	0.692270964384078979	4
2820	889	0.692271536588669023	4
2821	890	0.692734682559966952	4
2822	891	0.692879271507263028	4
2823	892	0.692625010013580011	4
2824	893	0.692326009273529053	4
2825	894	0.692036563158034967	4
2826	895	0.691906607151031028	4
2827	896	0.691547542810439952	4
2828	897	0.691082459688187045	4
2829	898	0.690571409463881958	4
2830	899	0.69043702483177205	4
2831	900	0.690434777736663952	4
2832	901	0.690561735630035978	4
2833	902	0.691239291429520009	4
2834	903	0.692174619436264016	4
2835	904	0.692189723253250011	4
2836	905	0.691850966215133978	4
2837	906	0.691692006587982022	4
2838	907	0.691261816024779985	4
2839	908	0.69057447314262399	4
2840	909	0.690083038806915017	4
2841	910	0.690085011720656949	4
2842	911	0.690252166986464966	4
2843	912	0.690318059921265048	4
2844	913	0.690105098485947033	4
2845	914	0.689463001489639016	4
2846	915	0.688765233755112005	4
2847	916	0.688660824298858998	4
2848	917	0.689146095514297952	4
2849	918	0.689624947309493974	4
2850	919	0.689855843782425038	4
2851	920	0.689630520343781028	4
2852	921	0.689416033029556008	4
2853	922	0.689230227470397971	4
2854	923	0.688968884944916038	4
2855	924	0.688792425394059049	4
2856	925	0.688659417629241988	4
2857	926	0.688373631238937023	4
2858	927	0.687709814310074052	4
2859	928	0.686535090208053034	4
2860	929	0.686232084035872991	4
2861	930	0.686133044958115001	4
2862	931	0.682461822032928045	4
2863	932	0.679293012619018977	4
2864	933	0.678134638071060003	4
2865	934	0.676302117109298995	4
2866	935	0.675128585100174039	4
2867	936	0.674831205606461038	4
2868	937	0.674749273061752031	4
2869	938	0.674860978126526012	4
2870	939	0.674641460180282038	4
2871	940	0.673725384473801037	4
2872	941	0.671947175264358987	4
2873	942	0.670129466056823997	4
2874	943	0.668834757804871005	4
2875	944	0.668625694513320989	4
2876	945	0.666864854097365956	4
2877	946	0.664368367195129039	4
2878	947	0.663271123170853016	4
2879	948	0.66170890927314796	4
2880	949	0.659734034538268954	4
2881	950	0.657521057128905961	4
2882	951	0.656690120697022039	4
2883	952	0.65647572278976396	4
2884	953	0.653919047117233032	4
2885	954	0.650975894927979026	4
2886	955	0.649373346567153953	4
2887	956	0.648433995246887052	4
2888	957	0.64727814197540301	4
2889	958	0.645291978120803966	4
2890	959	0.644880980253220049	4
2891	960	0.645001292228698953	4
2892	961	0.64288455247878995	4
2893	962	0.641507333517075007	4
2894	963	0.641357320547104037	4
2895	964	0.64196511507034304	4
2896	965	0.641360288858414007	4
2897	966	0.639655882120133024	4
2898	967	0.640185976028441961	4
2899	968	0.641172695159912043	4
2900	969	0.640511912107468051	4
2901	970	0.639902734756469971	4
2902	971	0.639634883403778054	4
2903	972	0.639809590578078957	4
2904	973	0.639628803730010964	4
2905	974	0.639462965726852017	4
2906	975	0.639240878820419023	4
2907	976	0.638577306270599054	4
2908	977	0.637955951690673984	4
2909	978	0.638496011495589988	4
2910	979	0.639207786321639992	4
2911	980	0.640424042940139993	4
2912	981	0.641851359605788985	4
2913	982	0.64246292710304298	4
2914	983	0.641707235574721979	4
2915	984	0.641453278064727961	4
2916	985	0.642225056886672974	4
2917	986	0.642516940832137951	4
2918	987	0.643024015426636009	4
2919	988	0.644437092542648027	4
2920	989	0.64492555856704703	4
2921	990	0.644749611616133977	4
2922	991	0.645884495973586992	4
2923	992	0.646224635839461992	4
2924	993	0.646238267421722967	4
2925	994	0.647900545597076993	4
2926	995	0.648604905605316029	4
2927	996	0.648370981216430997	4
2928	997	0.64843648076057403	4
2929	998	0.649002808332442949	4
2930	999	0.65004298686981199	4
2931	1000	0.650360083580016957	4
2932	1001	0.650317025184631969	4
2933	1002	0.650863254070282005	4
2934	1003	0.652345591783524026	4
2935	1004	0.653428989648819014	4
2936	1005	0.653339850902556973	4
2937	1006	0.654700547456741	4
2938	1007	0.657222509384155051	4
2939	1008	0.655954092741012018	4
2940	1009	0.655335658788681052	4
2941	1010	0.656919384002686013	4
2942	1011	0.657485163211823043	4
2943	1012	0.658394372463225963	4
2944	1013	0.659959459304810037	4
2945	1014	0.659957331418991044	4
2946	1015	0.660242706537247037	4
2947	1016	0.661538940668106035	4
2948	1017	0.661684560775756991	4
2949	1018	0.661934095621109053	4
2950	1019	0.662245064973830955	4
2951	1020	0.662980252504348999	4
2952	1021	0.66435495615005502	4
2953	1022	0.664827877283096003	4
2954	1023	0.665252107381820945	4
2955	1024	0.665487515926361017	4
2956	1025	0.665420299768447965	4
2957	1026	0.664628571271896007	4
2958	1027	0.663371783494948963	4
2959	1028	0.663462769985199041	4
2960	1029	0.664338368177413985	4
2961	1030	0.665094447135925027	4
2962	1031	0.665410137176513983	4
2963	1032	0.665085160732268976	4
2964	1033	0.665300828218460039	4
2965	1034	0.667444705963134988	4
2966	1035	0.670315980911254994	4
2967	1036	0.671270132064819003	4
2968	1037	0.670270645618438965	4
2969	1038	0.668594217300415017	4
2970	1039	0.671013909578322965	4
2971	1040	0.672849601507186978	4
2972	1041	0.669495272636413952	4
2973	1042	0.666662091016769986	4
2974	1043	0.665598332881928045	4
2975	1044	0.668531650304794001	4
2976	1045	0.670208913087845026	4
2977	1046	0.668894618749618974	4
2978	1047	0.669331252574920987	4
2979	1048	0.668965929746628052	4
2980	1049	0.667509615421295055	4
2981	1050	0.668285703659058039	4
2982	1051	0.668509763479232966	4
2983	1052	0.668817126750945956	4
2984	1053	0.667579716444016036	4
2985	1054	0.666110467910767001	4
2986	1055	0.664384859800339012	4
2987	1056	0.66330385804176295	4
2988	1057	0.663388389348984031	4
2989	1058	0.664878368377684992	4
2990	1059	0.666131651401519975	4
2991	1060	0.666086703538893987	4
2992	1061	0.661891585588454956	4
2993	1062	0.662554472684860007	4
2994	1063	0.666747409105300948	4
2995	1064	0.664089047908782981	4
2996	1065	0.659672373533249035	4
2997	1066	0.655515807867049993	4
2998	1067	0.655739635229111051	4
2999	1068	0.657092666625976984	4
3000	1069	0.654662567377090054	4
3001	1070	0.654943412542343006	4
3002	1071	0.651827049255371049	4
3003	1072	0.642398387193680032	4
3004	1073	0.644710034132004006	4
3005	1074	0.649175351858139016	4
3006	1075	0.652398025989533004	4
3007	326	0.0916724130511284013	3
3008	327	0.0894590429961684003	3
3009	328	0.0856799930334092991	3
3010	329	0.0844566039741038027	3
3011	330	0.0888460613787176029	3
3012	331	0.0859253220260146056	3
3013	332	0.0792226549237969035	3
3014	333	0.0836456939578054948	3
3015	334	0.0876809619367121956	3
3016	335	0.0864553097635509943	3
3017	336	0.0830550771206618049	3
3018	337	0.0795379132032395936	3
3019	338	0.0780033171176910956	3
3020	339	0.080058760941028595	3
3021	340	0.084421178326010704	3
3022	341	0.0852522701025006935	3
3023	342	0.086368070915341294	3
3024	343	0.0899331774562595054	3
3025	344	0.0876977015286681955	3
3026	345	0.082807099446654403	3
3027	346	0.0852392315864561045	3
3028	347	0.0842326469719409943	3
3029	348	0.0784225203096864942	3
3030	349	0.0802900176495311979	3
3031	350	0.084244685247540696	3
3032	351	0.0832693018019198955	3
3033	352	0.0816486794501544016	3
3034	353	0.0810455773025750975	3
3035	354	0.0841674916446208954	3
3036	355	0.0850876811891795037	3
3037	356	0.0808372087776659948	3
3038	357	0.0788767691701648954	3
3039	358	0.0801765117794276949	3
3040	359	0.0847504287958146946	3
3041	360	0.0864729993045331019	3
3042	361	0.0842041447758676981	3
3043	362	0.0836933031678197964	3
3044	363	0.0836448408663271054	3
3045	364	0.0826009754091500958	3
3046	365	0.0820958875119686959	3
3047	366	0.0825332123786208932	3
3048	367	0.0847238339483736003	3
3049	368	0.0843539964407682002	3
3050	369	0.080299654975533305	3
3051	370	0.0819776840507984994	3
3052	371	0.0860640555620193065	3
3053	372	0.0854321923106910047	3
3054	373	0.0848070867359637937	3
3055	374	0.0852262862026690049	3
3056	375	0.085736975073814406	3
3057	376	0.0847816187888384004	3
3058	377	0.0815755482763054068	3
3059	378	0.0837947875261306069	3
3060	379	0.0887212380766869008	3
3061	380	0.0864162426441908021	3
3062	381	0.0839904639869926001	3
3063	382	0.083862729370594094	3
3064	383	0.0823200345039369064	3
3065	384	0.0812018122524021946	3
3066	385	0.0832195710390804949	3
3067	386	0.0843608062714337054	3
3068	387	0.0839810241013766029	3
3069	388	0.083374038338661402	3
3070	389	0.0827789101749658029	3
3071	390	0.0824703685939312953	3
3072	391	0.0836665797978641962	3
3073	392	0.0847901161760090949	3
3074	393	0.0821623131632803066	3
3075	394	0.0810227412730458035	3
3076	395	0.0824240483343603031	3
3077	396	0.0829005781561139021	3
3078	397	0.0831532049924135069	3
3079	398	0.0840050987899300938	3
3080	399	0.0848830565810202997	3
3081	400	0.085314689204096697	3
3082	401	0.0842336900532243971	3
3083	402	0.0834367610514162028	3
3084	403	0.0837664958089592016	3
3085	404	0.0842867475003003935	3
3086	405	0.084687687456607999	3
3087	406	0.0846436694264409983	3
3088	407	0.0846505891531707938	3
3089	408	0.0847949292510746938	3
3090	409	0.0846832245588303029	3
3091	410	0.0847217366099359964	3
3092	411	0.0852559525519610006	3
3093	412	0.0851710252463815931	3
3094	413	0.0847619250416754011	3
3095	414	0.0855193361639977057	3
3096	415	0.0858495458960533003	3
3097	416	0.0851505771279335993	3
3098	417	0.0850295163691043021	3
3099	418	0.0853759851306677975	3
3100	419	0.0857418738305568973	3
3101	420	0.0856647621840238016	3
3102	421	0.0851491671055553956	3
3103	422	0.084843385964632298	3
3104	423	0.0849661678075789989	3
3105	424	0.0856386944651602033	3
3106	425	0.0858633462339637998	3
3107	426	0.085795197635889095	3
3108	427	0.0866143777966499051	3
3109	428	0.0867500230669975975	3
3110	429	0.0857253465801477016	3
3111	430	0.085696281865239296	3
3112	431	0.0861844718456269004	3
3113	432	0.0861053094267845986	3
3114	433	0.0860353708267209	3
3115	434	0.0860709547996521968	3
3116	435	0.0860362239181998029	3
3117	436	0.0860766395926473998	3
3118	437	0.0862804204225540022	3
3119	438	0.0862183310091498012	3
3120	439	0.0860348902642724955	3
3121	440	0.086252279579639296	3
3122	441	0.0866145975887775005	3
3123	442	0.0869814231991764969	3
3124	443	0.0872633419930933935	3
3125	444	0.0872553624212741991	3
3126	445	0.0867003910243512033	3
3127	446	0.0863675363361837006	3
3128	447	0.0863783247768878937	3
3129	448	0.0862620286643507039	3
3130	449	0.086231119930744296	3
3131	450	0.0864520072937011025	3
3132	451	0.0869542136788369058	3
3133	452	0.0875187627971170945	3
3134	453	0.0876869335770606023	3
3135	454	0.0875701084733006979	3
3136	455	0.0872889533638954995	3
3137	456	0.0873113274574278952	3
3138	457	0.0875311978161333049	3
3139	458	0.0877555496990680001	3
3140	459	0.0877222903072833043	3
3141	460	0.0874787084758280042	3
3142	461	0.0874808095395565033	3
3143	462	0.0874589756131173013	3
3144	463	0.0872534029185774024	3
3145	464	0.0872103571891786056	3
3146	465	0.0872906409204007028	3
3147	466	0.0872638262808323045	3
3148	467	0.0874010100960733033	3
3149	468	0.087730538100004099	3
3150	469	0.0879473201930525	3
3151	470	0.0878878310322761952	3
3152	471	0.087411463260650496	3
3153	472	0.0874082185328003969	3
3154	473	0.0878928974270820063	3
3155	474	0.0882257968187332986	3
3156	475	0.0882689952850344017	3
3157	476	0.0879846066236494029	3
3158	477	0.0879275724291800065	3
3159	478	0.0880737602710724016	3
3160	479	0.0882346518337728952	3
3161	480	0.0881571732461450958	3
3162	481	0.0878145508468150954	3
3163	482	0.0875225812196734065	3
3164	483	0.0875779911875726041	3
3165	484	0.0882636979222299056	3
3166	485	0.0888269357383252994	3
3167	486	0.0889961570501325988	3
3168	487	0.088548429310321794	3
3169	488	0.0883015654981137987	3
3170	489	0.0886954739689829047	3
3171	490	0.089069671928882696	3
3172	491	0.0892443545162674989	3
3173	492	0.0890439674258231978	3
3174	493	0.0890184119343758046	3
3175	494	0.0893257707357406061	3
3176	495	0.0897246859967710947	3
3177	496	0.0902403295040131032	3
3178	497	0.0908814258873461983	3
3179	498	0.0908601060509684061	3
3180	499	0.0904685147106645965	3
3181	500	0.0910225510597229975	3
3182	501	0.0917432270944120026	3
3183	502	0.092254821211099805	3
3184	503	0.0929823778569699999	3
3185	504	0.0937018543481828031	3
3186	505	0.0939452908933163938	3
3187	506	0.0947200469672679068	3
3188	507	0.0960716083645820063	3
3189	508	0.0967972613871096038	3
3190	509	0.0976327396929266011	3
3191	510	0.0992483571171759033	3
3192	511	0.100836060941218997	3
3193	512	0.102371357381343994	3
3194	513	0.104324828833341002	3
3195	514	0.106009501963854003	3
3196	515	0.107226807624102	3
3197	516	0.109053000807761993	3
3198	517	0.111501328647136994	3
3199	518	0.114369489252567	3
3200	519	0.116982784122228997	3
3201	520	0.119244985282421001	3
3202	521	0.121910512447356997	3
3203	522	0.124669216573237998	3
3204	523	0.127305839210749006	3
3205	524	0.130083926022052987	3
3206	525	0.132896788418292999	3
3207	526	0.135390080511570005	3
3208	527	0.137901246547699002	3
3209	528	0.140490770339965987	3
3210	529	0.142810784280300002	3
3211	530	0.144764229655266002	3
3212	531	0.146317534148693001	3
3213	532	0.148090414702891998	3
3214	533	0.150056742131709997	3
3215	534	0.15136972069740301	3
3216	535	0.152458928525448012	3
3217	536	0.153670966625214012	3
3218	537	0.154849484562874007	3
3219	538	0.155925437808035999	3
3220	539	0.156719543039798986	3
3221	540	0.157600037753582001	3
3222	541	0.158640459179878013	3
3223	542	0.159308299422263988	3
3224	543	0.159884274005889004	3
3225	544	0.160792835056781991	3
3226	545	0.161577433347701999	3
3227	546	0.162161864340304995	3
3228	547	0.162674926221371002	3
3229	548	0.163259945809840989	3
3230	549	0.163994893431664013	3
3231	550	0.164587199687958013	3
3232	551	0.165045201778412004	3
3233	552	0.165544681251048986	3
3234	553	0.165779359638691004	3
3235	554	0.165663681924342998	3
3236	555	0.16542285680770899	3
3237	556	0.165067039430141005	3
3238	557	0.164581708610057997	3
3239	558	0.163784757256507013	3
3240	559	0.162645362317561992	3
3241	560	0.161212459206582004	3
3242	561	0.159755639731884003	3
3243	562	0.158433660864829989	3
3244	563	0.156989194452762992	3
3245	564	0.155403472483158001	3
3246	565	0.153728246688842995	3
3247	566	0.151913203299046007	3
3248	567	0.149981975555420005	3
3249	568	0.148108057677746013	3
3250	569	0.146135106682777988	3
3251	570	0.143898911774158006	3
3252	571	0.141836352646350999	3
3253	572	0.140001326799393006	3
3254	573	0.138189241290092996	3
3255	574	0.13638106733560601	3
3256	575	0.134547386318445011	3
3257	576	0.132727205753326	3
3258	577	0.131109755486250007	3
3259	578	0.130024407058953989	3
3260	579	0.128645647317171014	3
3261	580	0.126952666789294011	3
3262	581	0.126091945916414011	3
3263	582	0.125375345349311995	3
3264	583	0.124151133000850997	3
3265	584	0.123142048716544994	3
3266	585	0.122479129582642995	3
3267	586	0.122197251766921006	3
3268	587	0.121666800230742	3
3269	588	0.120613783597946	3
3270	589	0.119658220559358	3
3271	590	0.118977185338735997	3
3272	591	0.118724007159471998	3
3273	592	0.118605479598046001	3
3274	593	0.118403617292642996	3
3275	594	0.117741905152798004	3
3276	595	0.117083039134740996	3
3277	596	0.116772003471851002	3
3278	597	0.116143293678760001	3
3279	598	0.115452110767365002	3
3280	599	0.115599438548088004	3
3281	600	0.11579217389226	3
3282	601	0.115628410130739004	3
3283	602	0.114751894026993997	3
3284	603	0.113758016377687995	3
3285	604	0.113406404852867002	3
3286	605	0.113432928919791995	3
3287	606	0.113426443189381998	3
3288	607	0.112237360328435995	3
3289	608	0.111034829169512003	3
3290	609	0.110609207302331994	3
3291	610	0.110482614487409994	3
3292	611	0.110233664512634	3
3293	612	0.109145563095808001	3
3294	613	0.108284756541251997	3
3295	614	0.107924431562423997	3
3296	615	0.107459917664528004	3
3297	616	0.106864292174578004	3
3298	617	0.106158148497342994	3
3299	618	0.105445511639118	3
3300	619	0.104885548353194996	3
3301	620	0.104854099452496005	3
3302	621	0.104673869907856001	3
3303	622	0.104064349085092003	3
3304	623	0.103733323514461004	3
3305	624	0.103534344583749993	3
3306	625	0.103091288357973002	3
3307	626	0.102896306663752005	3
3308	627	0.102952100336551999	3
3309	628	0.102663036435842001	3
3310	629	0.102445881813764003	3
3311	630	0.102628272026777004	3
3312	631	0.102746259421110001	3
3313	632	0.102676823735236997	3
3314	633	0.102339118719100994	3
3315	634	0.101972259581089006	3
3316	635	0.101669803261756994	3
3317	636	0.101333543658257003	3
3318	637	0.100935988128184995	3
3319	638	0.10045275092125	3
3320	639	0.099896240979432796	3
3321	640	0.0992803201079368036	3
3322	641	0.0986378975212574977	3
3323	642	0.0980324558913709954	3
3324	643	0.0974854528903963957	3
3325	644	0.0967890918254851046	3
3326	645	0.0959788486361503046	3
3327	646	0.0951604954898357946	3
3328	647	0.0945064686238767937	3
3329	648	0.0941081196069716019	3
3330	649	0.0939996801316739933	3
3331	650	0.0939065627753736948	3
3332	651	0.0935769602656365967	3
3333	652	0.0932792052626607998	3
3334	653	0.0930942334234716035	3
3335	654	0.093136820942163398	3
3336	655	0.0930337011814118958	3
3337	656	0.0925966612994671007	3
3338	657	0.0920602567493918056	3
3339	658	0.0914961732923986054	3
3340	659	0.0909088961780071952	3
3341	660	0.0906836055219174958	3
3342	661	0.0906962454318998995	3
3343	662	0.0902288481593129937	3
3344	663	0.0897255241870879017	3
3345	664	0.0894717574119566067	3
3346	665	0.0890525616705416939	3
3347	666	0.0886267125606535061	3
3348	667	0.0886416845023630939	3
3349	668	0.0885573178529737021	3
3350	669	0.0883041433990000985	3
3351	670	0.0884729288518426998	3
3352	671	0.0887475945055483939	3
3353	672	0.0887836068868636946	3
3354	673	0.0888108760118485052	3
3355	674	0.0888875313103200948	3
3356	675	0.0889966599643232936	3
3357	676	0.0893807820975777989	3
3358	677	0.0900566354393956964	3
3359	678	0.0903521664440632005	3
3360	679	0.0904981009662153002	3
3361	680	0.090909738093614495	3
3362	681	0.0916653983294963975	3
3363	682	0.092654481530189306	3
3364	683	0.093331664800643796	3
3365	684	0.0942851491272450049	3
3366	685	0.0958709865808484996	3
3367	686	0.0971422158181669965	3
3368	687	0.0983354523777962009	3
3369	688	0.100078281015158005	3
3370	689	0.102150000631808999	3
3371	690	0.104505553841591006	3
3372	691	0.107286851853132997	3
3373	692	0.110743977129459006	3
3374	693	0.115191090852022004	3
3375	694	0.12102734670043	3
3376	695	0.127892173826693989	3
3377	696	0.134560674428940014	3
3378	697	0.142463400959969011	3
3379	698	0.152287013828754009	3
3380	699	0.162843495607375988	3
3381	700	0.174322009086608998	3
3382	701	0.186747126281261	3
3383	702	0.199383497238159013	3
3384	703	0.211913675069807989	3
3385	704	0.224643938243388991	3
3386	705	0.237731948494911	3
3387	706	0.251351580023766008	3
3388	707	0.264878764748574025	3
3389	708	0.278266280889510997	3
3390	709	0.291625887155532004	3
3391	710	0.305076926946640015	3
3392	711	0.31870244443416601	3
3393	712	0.332625553011894004	3
3394	713	0.346615865826606973	3
3395	714	0.360452160239218999	3
3396	715	0.37417691946029602	3
3397	716	0.388183549046516974	3
3398	717	0.402794390916825007	3
3399	718	0.418303042650223	3
3400	719	0.433656111359596974	3
3401	720	0.448792770504951977	3
3402	721	0.461921632289886974	3
3403	722	0.473637133836745994	3
3404	723	0.485796228051184997	3
3405	724	0.49943175911903398	3
3406	725	0.51241800189018305	3
3407	726	0.524649918079375999	3
3408	727	0.536105692386626975	3
3409	728	0.547125697135925959	3
3410	729	0.557779878377913985	3
3411	730	0.567788958549499956	3
3412	731	0.577173531055449995	3
3413	732	0.585856527090071966	3
3414	733	0.594296485185622947	3
3415	734	0.602119117975234985	3
3416	735	0.609448194503783958	3
3417	736	0.616765201091767024	3
3418	737	0.623727351427078025	3
3419	738	0.629685759544373003	3
3420	739	0.635368436574936024	3
3421	740	0.640857100486756037	3
3422	741	0.645560562610625999	3
3423	742	0.650003552436827947	3
3424	743	0.654517412185668945	3
3425	744	0.658365607261658048	3
3426	745	0.661639571189881037	3
3427	746	0.664682090282439964	3
3428	747	0.667464733123779963	3
3429	748	0.670076459646224976	3
3430	749	0.672814965248108021	3
3431	750	0.675291895866395042	3
3432	751	0.677113920450211015	3
3433	752	0.678607702255249023	3
3434	753	0.680007815361022949	3
3435	754	0.681780815124511053	3
3436	755	0.683109223842620961	3
3437	756	0.683818757534026989	3
3438	757	0.684732437133788951	3
3439	758	0.686295926570893	3
3440	759	0.689525336027145053	3
3441	760	0.693667531013488992	3
3442	761	0.697110891342162975	3
3443	762	0.696364521980284978	3
3444	763	0.694877684116363969	3
3445	764	0.694224148988723977	3
3446	765	0.693128168582916038	3
3447	766	0.69185316562652599	3
3448	767	0.690599352121353038	3
3449	768	0.689715981483460028	3
3450	769	0.689250230789185014	3
3451	770	0.689223438501358032	3
3452	771	0.689426928758620994	3
3453	772	0.689720004796981034	3
3454	773	0.690078586339951006	3
3455	774	0.690259456634522039	3
3456	775	0.689934641122817993	3
3457	776	0.68985110521316495	3
3458	777	0.690034925937653032	3
3459	778	0.689786404371262041	3
3460	779	0.689660936594008955	3
3461	780	0.689949125051497969	3
3462	781	0.690069526433944036	3
3463	782	0.690028816461562999	3
3464	783	0.689952433109283003	3
3465	784	0.690154045820235984	3
3466	785	0.690621882677078025	3
3467	786	0.690965056419373003	3
3468	787	0.691111594438552967	3
3469	788	0.69102364778518699	3
3470	789	0.690869390964507946	3
3471	790	0.690728008747101052	3
3472	791	0.690684080123901034	3
3473	792	0.690558493137358953	3
3474	793	0.690376311540602972	3
3475	794	0.690447211265563965	3
3476	795	0.690562158823012973	3
3477	796	0.690570265054702981	3
3478	797	0.690606594085693026	3
3479	798	0.690698325634003019	3
3480	799	0.690857052803039995	3
3481	800	0.691099375486374012	3
3482	801	0.691378563642502053	3
3483	802	0.69152176380157504	3
3484	803	0.691567569971085039	3
3485	804	0.691562026739121039	3
3486	805	0.691631942987442017	3
3487	806	0.691625654697417991	3
3488	807	0.691173017024994008	3
3489	808	0.691176712512969971	3
3490	809	0.691794484853744951	3
3491	810	0.692340552806854026	3
3492	811	0.692764520645142046	3
3493	812	0.693041384220123957	3
3494	813	0.693267881870269997	3
3495	814	0.693533003330230047	3
3496	815	0.693974584341048972	3
3497	816	0.694396734237671009	3
3498	817	0.694609522819519043	3
3499	818	0.694292575120925015	3
3500	819	0.693780660629273016	3
3501	820	0.693387210369109996	3
3502	821	0.693494260311126043	3
3503	822	0.693766772747039018	3
3504	823	0.693465828895568959	3
3505	824	0.692947983741760032	3
3506	825	0.692620784044265969	3
3507	826	0.693493336439133023	3
3508	827	0.69431674480438299	3
3509	828	0.69423228502273604	3
3510	829	0.694246917963027954	3
3511	830	0.694557517766952959	3
3512	831	0.695261925458908969	3
3513	832	0.695476651191711981	3
3514	833	0.695046871900559027	3
3515	834	0.694385856389999945	3
3516	835	0.694283276796341053	3
3517	836	0.695032000541687012	3
3518	837	0.694610893726347989	3
3519	838	0.693846255540848	3
3520	839	0.694463014602662021	3
3521	840	0.694958537817002009	3
3522	841	0.694962084293366034	3
3523	842	0.694689750671387052	3
3524	843	0.694373995065688976	3
3525	844	0.694201767444610041	3
3526	845	0.694219619035721047	3
3527	846	0.694543242454529031	3
3528	847	0.69533216953277599	3
3529	848	0.695140510797501054	3
3530	849	0.694232553243637973	3
3531	850	0.694918960332871039	3
3532	851	0.695615679025650024	3
3533	852	0.695488512516021951	3
3534	853	0.695178180932997991	3
3535	854	0.695105254650115967	3
3536	855	0.695686966180801947	3
3537	856	0.695368289947509988	3
3538	857	0.694481998682021984	3
3539	858	0.694924771785735973	3
3540	859	0.695501476526260043	3
3541	860	0.695773571729660034	3
3542	861	0.696336179971694946	3
3543	862	0.696643322706222978	3
3544	863	0.696115434169768954	3
3545	864	0.69604453444480896	3
3546	865	0.696337401866912953	3
3547	866	0.695865541696549017	3
3548	867	0.695914953947068038	3
3549	868	0.696689575910569014	3
3550	869	0.696778029203415028	3
3551	870	0.69672021269798301	3
3552	871	0.696994304656981978	3
3553	872	0.696688801050186046	3
3554	873	0.696109801530838013	3
3555	874	0.696183353662490956	3
3556	875	0.696341216564179022	3
3557	876	0.69635188579559304	3
3558	877	0.695912361145019975	3
3559	878	0.695997655391693004	3
3560	879	0.697086095809936968	3
3561	880	0.696904182434082031	3
3562	881	0.696207404136658048	3
3563	882	0.696715831756592019	3
3564	883	0.696983844041825007	3
3565	884	0.69698706269264199	3
3566	885	0.69807961583137601	3
3567	886	0.698501676321028997	3
3568	887	0.697624236345291027	3
3569	888	0.698039710521697998	3
3570	889	0.698899596929549949	3
3571	890	0.698846161365509033	3
3572	891	0.698762595653534047	3
3573	892	0.698728263378143977	3
3574	893	0.698197811841963945	3
3575	894	0.699069112539292048	3
3576	895	0.701501250267029031	3
3577	896	0.701397091150284036	3
3578	897	0.69983667135238703	3
3579	898	0.698453933000565019	3
3580	899	0.698863685131072998	3
3581	900	0.700059324502944946	3
3582	901	0.699247986078262995	3
3583	902	0.699106454849243053	3
3584	903	0.700085192918777022	3
3585	904	0.700890839099884033	3
3586	905	0.701535642147064986	3
3587	906	0.701862841844559049	3
3588	907	0.700911849737167025	3
3589	908	0.69915279746055603	3
3590	909	0.697482436895371039	3
3591	910	0.698664546012877974	3
3592	911	0.701760470867156982	3
3593	912	0.701526254415512973	3
3594	913	0.70075336098670904	3
3595	914	0.700525581836700995	3
3596	915	0.700204104185104037	3
3597	916	0.699147373437882025	3
3598	917	0.697196543216705988	3
3599	918	0.699374914169311968	3
3600	919	0.702996432781220038	3
3601	920	0.70124539732933	3
3602	921	0.699800074100495051	3
3603	922	0.700260221958159956	3
3604	923	0.702301144599914995	3
3605	924	0.703318178653717041	3
3606	925	0.701973199844361018	3
3607	926	0.702532351016998957	3
3608	927	0.704001098871230968	3
3609	928	0.703984469175338967	3
3610	929	0.703152507543562955	3
3611	930	0.701838850975035955	3
3612	931	0.700718849897384977	3
3613	932	0.701205044984817949	3
3614	933	0.703305572271348045	3
3615	934	0.70171245932579096	3
3616	935	0.69825896620750405	3
3617	936	0.695712685585021973	3
3618	937	0.696231245994567982	3
3619	938	0.700007140636443981	3
3620	939	0.706664174795150979	3
3621	940	0.703519254922867043	3
3622	941	0.693448126316071001	3
3623	942	0.693794876337051947	3
3624	943	0.697785168886185025	3
3625	944	0.701075494289397971	3
3626	945	0.696273088455199973	3
3627	946	0.691519021987915039	3
3628	947	0.695148617029189952	3
3629	948	0.697567462921143022	3
3630	949	0.698098510503769032	3
3631	950	0.699064940214156993	3
3632	951	0.700240731239318959	3
3633	952	0.699864804744721014	3
3634	953	0.690904408693314043	3
3635	954	0.687834531068801991	3
3636	955	0.695872455835342962	3
3637	956	0.695703268051147017	3
3638	957	0.692086011171341053	3
3639	958	0.69190108776092496	3
3640	959	0.690108001232146995	3
3641	960	0.688105016946793047	3
3642	961	0.690297186374664973	3
3643	962	0.691881269216538031	3
3644	963	0.692089140415191983	3
3645	964	0.694079995155334029	3
3646	965	0.693216353654861006	3
3647	966	0.688552081584929976	3
3648	967	0.690829515457153986	3
3649	968	0.692921698093414973	3
3650	969	0.686566770076752042	3
3651	970	0.684784024953842052	3
3652	971	0.688025414943694957	3
3653	972	0.692200064659118985	3
3654	973	0.693281739950180054	3
3655	974	0.69102013111114502	3
3656	975	0.690826058387757014	3
3657	976	0.690317660570143987	3
3658	977	0.688606381416321023	3
3659	978	0.689705103635787964	3
3660	979	0.690873205661774015	3
3661	980	0.688779652118682972	3
3662	981	0.687308847904205988	3
3663	982	0.687156677246093972	3
3664	983	0.688219398260117021	3
3665	984	0.688424825668334961	3
3666	985	0.687317043542862049	3
3667	986	0.685224801301955955	3
3668	987	0.686947256326674971	3
3669	988	0.693318665027617964	3
3670	989	0.693365842103959018	3
3671	990	0.690092802047730047	3
3672	991	0.687783598899841975	3
3673	992	0.686205118894577026	3
3674	993	0.68660616874694802	3
3675	994	0.69120496511459395	3
3676	995	0.690396130084991011	3
3677	996	0.684770852327347024	3
3678	997	0.686522930860519964	3
3679	998	0.688874512910843007	3
3680	999	0.688746035099029985	3
3681	1000	0.686897426843642966	3
3682	1001	0.68751060962677002	3
3683	1002	0.693215787410736972	3
3684	1003	0.693102270364761019	3
3685	1004	0.690084993839264027	3
3686	1005	0.691300988197325994	3
3687	1006	0.689517021179198997	3
3688	1007	0.685029238462449008	3
3689	1008	0.683462977409363015	3
3690	1009	0.68277102708816495	3
3691	1010	0.683106690645218007	3
3692	1011	0.688253700733185036	3
3693	1012	0.690927296876907016	3
3694	1013	0.687133848667144997	3
3695	1014	0.686756998300552035	3
3696	1015	0.688606142997741033	3
3697	1016	0.68994390964508101	3
3698	1017	0.691050350666046032	3
3699	1018	0.690806597471236961	3
3700	1019	0.68718662858009405	3
3701	1020	0.684726119041442982	3
3702	1021	0.685029596090316995	3
3703	1022	0.688430011272430975	3
3704	1023	0.690745800733565951	3
3705	1024	0.691120862960815985	3
3706	1025	0.69720810651779197	3
3707	1026	0.698014408349990956	3
3708	1027	0.685886442661285956	3
3709	1028	0.677771329879760964	3
3710	1029	0.674665182828902976	3
3711	1030	0.674849122762680054	3
3712	1031	0.679393321275710949	3
3713	1032	0.683099597692489957	3
3714	1033	0.672346800565720049	3
3715	1034	0.680840611457824041	3
3716	1035	0.704393625259399969	3
3717	1036	0.685340493917464988	3
3718	1037	0.668086707592011053	3
3719	1038	0.673958569765091053	3
3720	1039	0.688483417034148948	3
3721	1040	0.700413197278976996	3
3722	1041	0.699507117271424006	3
3723	1042	0.687452942132950051	3
3724	1043	0.675774127244949008	3
3725	1044	0.678859472274780051	3
3726	1045	0.678557634353637029	3
3727	1046	0.675521820783615001	3
3728	1047	0.680936157703398992	3
3729	1048	0.679198890924453957	3
3730	1049	0.669310718774795976	3
3731	1050	0.665489464998245017	3
3732	1051	0.670899957418441995	3
3733	1052	0.682964771986007024	3
3734	1053	0.677167236804962047	3
3735	1054	0.668805807828902976	3
3736	1055	0.671186864376068004	3
3737	1056	0.676657974720000999	3
3738	1057	0.676402807235717995	3
3739	1058	0.662586301565169955	3
3740	1059	0.666888564825057983	3
3741	1060	0.678096890449524037	3
3742	1061	0.662503123283386008	3
3743	1062	0.66957759857177801	3
3744	1063	0.691211074590682983	3
3745	1064	0.653504818677902	3
3746	1065	0.651301890611649004	3
3747	1066	0.698373675346374956	3
3748	1067	0.682283967733383956	3
3749	1068	0.65209430456161499	3
3750	1069	0.639001578092574962	3
3751	1070	0.659338057041167991	3
3752	1071	0.680343717336653997	3
3753	1072	0.669861346483230036	3
3754	1073	0.677352219820023027	3
3755	1074	0.685062915086745994	3
3756	1075	0.659795612096787054	3
\.


--
-- TOC entry 2260 (class 0 OID 0)
-- Dependencies: 195
-- Name: libreria_firma_espectral_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('libreria_firma_espectral_id_seq', 7, true);


--
-- TOC entry 2222 (class 0 OID 16922)
-- Dependencies: 198
-- Data for Name: libreria_indice_vegetacion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY libreria_indice_vegetacion (id, "NDVI", "RDVi", "MSR", "MSAVI", "MCARI", "TVI", "MCARI1", "MTVI1", "MCARI2", "MTVI2", "Especie_id") FROM stdin;
1	0.773021877869000007	0.682527235999999982	2.29464118400000006	0.729076834000000007	0.165315944999999992	38.2537088500000024	0.986520346000000048	0.986520346000000048	0.77750710199999995	0.77750710199999995	3
2	0.813057856000000023	0.706281548999999953	2.65939494700000001	0.765797980999999961	0.0950209620000000005	37.6728199100000012	0.959041699000000025	0.959041699000000025	0.785103171000000044	0.785103171000000044	4
3	0.810895790999999977	0.591968064000000016	2.63711728799999978	0.66460002600000001	0.054955933999999998	26.4376149699999985	0.674044952000000031	0.674044952000000031	0.668470247000000017	0.668470247000000017	5
4	0.815147428434792043	0.56915003197519598	2.6812568085128401	0.636787111270355033	0.164574221499175999	25.5732107588222988	0.66855599373579	0.668555993735790999	0.691745586730920015	0.691745586730920015	2
5	0.876858390262840959	0.639649529109212023	3.53380065160727019	0.743388884356556034	0.226773870861974008	29.8228665702044999	0.786050278432666949	0.786050278432666949	0.82929358966322797	0.82929358966322797	1
\.


--
-- TOC entry 2261 (class 0 OID 0)
-- Dependencies: 197
-- Name: libreria_indice_vegetacion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('libreria_indice_vegetacion_id_seq', 5, true);


--
-- TOC entry 2224 (class 0 OID 16930)
-- Dependencies: 200
-- Data for Name: libreria_parametros_curva_luz; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY libreria_parametros_curva_luz (id, "Amax", "K", "Rd", "PCL", "Especie_id") FROM stdin;
1	2.79749999999999988	7.51609999999999978	-0.688500000000000001	2.45369999999999999	1
2	2.93619999999999992	4.21520000000000028	-0.243600000000000011	0.381299999999999972	4
3	2.65899999999999981	6.0743999999999998	-0.888100000000000001	3.04630000000000001	3
4	2.79979999999999984	108.799999999999997	-0.181999999999999995	7.56419999999999959	5
5	2.39400000000000013	20.9029999999999987	-0.699100000000000055	8.62190000000000012	2
\.


--
-- TOC entry 2262 (class 0 OID 0)
-- Dependencies: 199
-- Name: libreria_parametros_curva_luz_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('libreria_parametros_curva_luz_id_seq', 5, true);


--
-- TOC entry 2022 (class 2606 OID 16765)
-- Name: auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- TOC entry 2028 (class 2606 OID 16775)
-- Name: auth_group_permissions_group_id_permission_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_key UNIQUE (group_id, permission_id);


--
-- TOC entry 2030 (class 2606 OID 16773)
-- Name: auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- TOC entry 2024 (class 2606 OID 16763)
-- Name: auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- TOC entry 2017 (class 2606 OID 16755)
-- Name: auth_permission_content_type_id_codename_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_key UNIQUE (content_type_id, codename);


--
-- TOC entry 2019 (class 2606 OID 16753)
-- Name: auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- TOC entry 2039 (class 2606 OID 16793)
-- Name: auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- TOC entry 2041 (class 2606 OID 16795)
-- Name: auth_user_groups_user_id_group_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_key UNIQUE (user_id, group_id);


--
-- TOC entry 2032 (class 2606 OID 16783)
-- Name: auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- TOC entry 2045 (class 2606 OID 16803)
-- Name: auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- TOC entry 2047 (class 2606 OID 16805)
-- Name: auth_user_user_permissions_user_id_permission_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_key UNIQUE (user_id, permission_id);


--
-- TOC entry 2035 (class 2606 OID 16785)
-- Name: auth_user_username_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- TOC entry 2051 (class 2606 OID 16861)
-- Name: django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- TOC entry 2012 (class 2606 OID 16745)
-- Name: django_content_type_app_label_3ec8c61c_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_app_label_3ec8c61c_uniq UNIQUE (app_label, model);


--
-- TOC entry 2014 (class 2606 OID 16743)
-- Name: django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- TOC entry 2010 (class 2606 OID 16735)
-- Name: django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- TOC entry 2054 (class 2606 OID 16881)
-- Name: django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- TOC entry 2058 (class 2606 OID 16895)
-- Name: libreria_clasificacion_cientifica_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY libreria_clasificacion_cientifica
    ADD CONSTRAINT libreria_clasificacion_cientifica_pkey PRIMARY KEY (id);


--
-- TOC entry 2061 (class 2606 OID 16903)
-- Name: libreria_cordenada_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY libreria_cordenada
    ADD CONSTRAINT libreria_cordenada_pkey PRIMARY KEY (id);


--
-- TOC entry 2063 (class 2606 OID 16911)
-- Name: libreria_especie_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY libreria_especie
    ADD CONSTRAINT libreria_especie_pkey PRIMARY KEY (id);


--
-- TOC entry 2066 (class 2606 OID 16919)
-- Name: libreria_firma_espectral_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY libreria_firma_espectral
    ADD CONSTRAINT libreria_firma_espectral_pkey PRIMARY KEY (id);


--
-- TOC entry 2069 (class 2606 OID 16927)
-- Name: libreria_indice_vegetacion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY libreria_indice_vegetacion
    ADD CONSTRAINT libreria_indice_vegetacion_pkey PRIMARY KEY (id);


--
-- TOC entry 2072 (class 2606 OID 16935)
-- Name: libreria_parametros_curva_luz_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY libreria_parametros_curva_luz
    ADD CONSTRAINT libreria_parametros_curva_luz_pkey PRIMARY KEY (id);


--
-- TOC entry 2020 (class 1259 OID 16812)
-- Name: auth_group_name_331666e8_like; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX auth_group_name_331666e8_like ON auth_group USING btree (name varchar_pattern_ops);


--
-- TOC entry 2025 (class 1259 OID 16823)
-- Name: auth_group_permissions_0e939a4f; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX auth_group_permissions_0e939a4f ON auth_group_permissions USING btree (group_id);


--
-- TOC entry 2026 (class 1259 OID 16824)
-- Name: auth_group_permissions_8373b171; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX auth_group_permissions_8373b171 ON auth_group_permissions USING btree (permission_id);


--
-- TOC entry 2015 (class 1259 OID 16811)
-- Name: auth_permission_417f1b1c; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX auth_permission_417f1b1c ON auth_permission USING btree (content_type_id);


--
-- TOC entry 2036 (class 1259 OID 16837)
-- Name: auth_user_groups_0e939a4f; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX auth_user_groups_0e939a4f ON auth_user_groups USING btree (group_id);


--
-- TOC entry 2037 (class 1259 OID 16836)
-- Name: auth_user_groups_e8701ad4; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX auth_user_groups_e8701ad4 ON auth_user_groups USING btree (user_id);


--
-- TOC entry 2042 (class 1259 OID 16849)
-- Name: auth_user_user_permissions_8373b171; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX auth_user_user_permissions_8373b171 ON auth_user_user_permissions USING btree (permission_id);


--
-- TOC entry 2043 (class 1259 OID 16848)
-- Name: auth_user_user_permissions_e8701ad4; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX auth_user_user_permissions_e8701ad4 ON auth_user_user_permissions USING btree (user_id);


--
-- TOC entry 2033 (class 1259 OID 16825)
-- Name: auth_user_username_94b8aae_like; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX auth_user_username_94b8aae_like ON auth_user USING btree (username varchar_pattern_ops);


--
-- TOC entry 2048 (class 1259 OID 16872)
-- Name: django_admin_log_417f1b1c; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX django_admin_log_417f1b1c ON django_admin_log USING btree (content_type_id);


--
-- TOC entry 2049 (class 1259 OID 16873)
-- Name: django_admin_log_e8701ad4; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX django_admin_log_e8701ad4 ON django_admin_log USING btree (user_id);


--
-- TOC entry 2052 (class 1259 OID 16882)
-- Name: django_session_de54fa62; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX django_session_de54fa62 ON django_session USING btree (expire_date);


--
-- TOC entry 2055 (class 1259 OID 16883)
-- Name: django_session_session_key_630ca218_like; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX django_session_session_key_630ca218_like ON django_session USING btree (session_key varchar_pattern_ops);


--
-- TOC entry 2056 (class 1259 OID 16960)
-- Name: libreria_clasificacion_cientifica_a2d40ddd; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX libreria_clasificacion_cientifica_a2d40ddd ON libreria_clasificacion_cientifica USING btree ("Especie_id");


--
-- TOC entry 2059 (class 1259 OID 16954)
-- Name: libreria_cordenada_a2d40ddd; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX libreria_cordenada_a2d40ddd ON libreria_cordenada USING btree ("Especie_id");


--
-- TOC entry 2064 (class 1259 OID 16941)
-- Name: libreria_firma_espectral_a2d40ddd; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX libreria_firma_espectral_a2d40ddd ON libreria_firma_espectral USING btree ("Especie_id");


--
-- TOC entry 2067 (class 1259 OID 16947)
-- Name: libreria_indice_vegetacion_a2d40ddd; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX libreria_indice_vegetacion_a2d40ddd ON libreria_indice_vegetacion USING btree ("Especie_id");


--
-- TOC entry 2070 (class 1259 OID 16953)
-- Name: libreria_parametros_curva_luz_a2d40ddd; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX libreria_parametros_curva_luz_a2d40ddd ON libreria_parametros_curva_luz USING btree ("Especie_id");


--
-- TOC entry 2075 (class 2606 OID 16818)
-- Name: auth_group_permiss_permission_id_23962d04_fk_auth_permission_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permiss_permission_id_23962d04_fk_auth_permission_id FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2074 (class 2606 OID 16813)
-- Name: auth_group_permissions_group_id_58c48ba9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_58c48ba9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2073 (class 2606 OID 16806)
-- Name: auth_permiss_content_type_id_51277a81_fk_django_content_type_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permiss_content_type_id_51277a81_fk_django_content_type_id FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2077 (class 2606 OID 16831)
-- Name: auth_user_groups_group_id_30a071c9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_30a071c9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2076 (class 2606 OID 16826)
-- Name: auth_user_groups_user_id_24702650_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_24702650_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2079 (class 2606 OID 16843)
-- Name: auth_user_user_per_permission_id_3d7071f0_fk_auth_permission_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_per_permission_id_3d7071f0_fk_auth_permission_id FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2078 (class 2606 OID 16838)
-- Name: auth_user_user_permissions_user_id_7cd7acb6_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_7cd7acb6_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2080 (class 2606 OID 16862)
-- Name: django_admin_content_type_id_5151027a_fk_django_content_type_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_content_type_id_5151027a_fk_django_content_type_id FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2081 (class 2606 OID 16867)
-- Name: django_admin_log_user_id_1c5f563_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_1c5f563_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2082 (class 2606 OID 16961)
-- Name: libreria_clasificaci_Especie_id_75570659_fk_libreria_especie_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY libreria_clasificacion_cientifica
    ADD CONSTRAINT "libreria_clasificaci_Especie_id_75570659_fk_libreria_especie_id" FOREIGN KEY ("Especie_id") REFERENCES libreria_especie(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2083 (class 2606 OID 16955)
-- Name: libreria_cordenada_Especie_id_d005419_fk_libreria_especie_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY libreria_cordenada
    ADD CONSTRAINT "libreria_cordenada_Especie_id_d005419_fk_libreria_especie_id" FOREIGN KEY ("Especie_id") REFERENCES libreria_especie(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2084 (class 2606 OID 16936)
-- Name: libreria_firma_espec_Especie_id_5759573b_fk_libreria_especie_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY libreria_firma_espectral
    ADD CONSTRAINT "libreria_firma_espec_Especie_id_5759573b_fk_libreria_especie_id" FOREIGN KEY ("Especie_id") REFERENCES libreria_especie(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2085 (class 2606 OID 16942)
-- Name: libreria_indice_vege_Especie_id_2bce9558_fk_libreria_especie_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY libreria_indice_vegetacion
    ADD CONSTRAINT "libreria_indice_vege_Especie_id_2bce9558_fk_libreria_especie_id" FOREIGN KEY ("Especie_id") REFERENCES libreria_especie(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2086 (class 2606 OID 16948)
-- Name: libreria_parametros__Especie_id_3701dd03_fk_libreria_especie_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY libreria_parametros_curva_luz
    ADD CONSTRAINT "libreria_parametros__Especie_id_3701dd03_fk_libreria_especie_id" FOREIGN KEY ("Especie_id") REFERENCES libreria_especie(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2231 (class 0 OID 0)
-- Dependencies: 5
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2015-04-14 18:34:23 COT

--
-- PostgreSQL database dump complete
--

